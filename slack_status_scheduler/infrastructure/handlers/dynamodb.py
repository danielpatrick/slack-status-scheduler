"""Parse dynamodb stream item and publish to SNS topic."""

import json

import boto3

from .base import BaseHandler


class DynamodbStreamToSnsHandler(BaseHandler):
    """Publish dynamodb stream event to SNS."""

    TOPIC_ARN = None
    sns_client = boto3.client("sns")

    def execute(self):
        """
        Send record to SNS topic defined at self.TOPIC_ARN.

        Use self.create_message method to customise the format and contents of
        the message sent to SNS.

        Use self.create_message_attributes method to customise the message
        attributes.

        Use self.filter method to determine which records should be sent
        (eg filter based on one of INSERT/MODIFY/REMOVE event types).
        """
        for record in self.event["Records"]:
            if self.filter(record):
                message = self.create_message(record)
                message_attributes = self.create_message_attributes(record)
                self.send_to_sns(message, message_attributes)
            else:
                self.logger.info(
                    "Skipping filtered record",
                    {"record": record},
                )

    def send_to_sns(self, message, message_attributes=None):
        """Send message to SNS defined at self.TOPIC_ARN."""
        kwargs = {
            "TopicArn": self.TOPIC_ARN,
            "Message": message,
        }

        if message_attributes is not None:
            kwargs["MessageAttributes"] = message_attributes

        self.logger.info(
            "Sending to SNS",
            kwargs,
        )

        self.sns_client.publish(**kwargs)

    def filter(self, record):
        """
        Override to customise which messages are sent to SNS.

        Default behaviour is to send all messages.

        Should return a boolean, where True indicates the message should be
        sent.
        """
        return True

    def create_message(self, record):
        """Create SNS message body from dynamodb stream record."""
        return json.dumps(record["dynamodb"])

    def create_message_attributes(self, record):
        """Create SNS message attributes from dynamodb stream record."""
        return {
            "eventName": {
                "StringValue": record["eventName"],
                "DataType": "String",
            },
        }
