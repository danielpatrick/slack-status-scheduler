"""Exception classes corresponding to HTTP responses."""


class HttpErrorBase(Exception):
    """Abstract base class for HTTP error scenarios."""

    status_code = None

    def __init__(self, message, body=None):
        """
        Create an exception that contains HTTP response information.

        HttpError should not be instantiated directly, instead its subclasses
        should be used.

        Parameters:
            message: str
                Exception message (for internal use)

            body: Optional[str]
                Intended body of the HTTP response after this error is raised
        """
        super().__init__(message)

        self.body = body if body is not None else self.default_body


class HttpBadRequestError(HttpErrorBase):
    """400 Bad request."""

    status_code = 400
    default_body = "Bad request"


class HttpForbiddenError(HttpErrorBase):
    """403 Forbidden."""

    status_code = 403
    default_body = "Forbidden"


class HttpInternalServerError(HttpErrorBase):
    """500 Internal server error."""

    status_code = 500
    default_body = "Internal server error"
