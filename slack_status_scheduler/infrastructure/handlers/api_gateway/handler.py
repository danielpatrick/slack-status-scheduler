"""Handler that responds to API Gateway V2 events."""

from base64 import b64decode

import chardet
from requests.structures import CaseInsensitiveDict
from requests.utils import get_encoding_from_headers

from infrastructure.handlers.base import BaseHandler

from .exceptions import HttpErrorBase


class ApiGatewayV2Handler(BaseHandler):
    """Handle API Gateway V2 (HttpApi) events."""

    FAIL_ON_ERROR = False

    def parse_headers(self):
        """Parse headers from event."""
        headers = self.event.get("headers", {})
        multi_value_headers = self.event.get("multiValueHeaders", {})

        return CaseInsensitiveDict({**headers, **multi_value_headers})

    def parse_body(self):
        """
        Parse headers from event, setting body attribute on handler.

        Decodes from base64 and bytes if required.
        """
        body = self.event.get("body")

        if self.event.get("isBase64Encoded", False):
            body = b64decode(body)

        if isinstance(body, bytes):
            encoding = get_encoding_from_headers(self.headers)

            if encoding is None:
                encoding = chardet.detect(body).get("encoding")

            if encoding is None:
                encoding = "utf-8"

            body = body.decode(encoding)

        return body

    def parse_event(self):
        """Parse event, setting instance body and headers attributes."""
        super().parse_event()
        self.headers = self.parse_headers()
        self.body = self.parse_body()

    def loggable_event(self):
        """Return a modified version of the event object."""
        return {
            **self.event,
            "body": self.body,
        }

    def create_response(
        self,
        *,
        status_code=200,
        body=None,
        headers=None,
        multi_value_headers=None,
        is_base64=False
    ):
        """Create HTTP response."""
        response = {"statusCode": status_code}

        if body is not None:
            response["body"] = body

        if headers is not None:
            response["headers"] = headers

        if multi_value_headers is not None:
            response["multiValueHeaders"] = multi_value_headers

        if is_base64:
            response["isBase64Encoded"] = True

        self.logger.info("Returning response", response)

        return response

    def execute(self):
        """
        Handle the API Gateway event as required.

        The response will be used as the lambda response.
        """
        return self.create_response()

    def handle_exception(self, exception):
        """Log information about the exception and create HTTP response."""
        super().handle_exception(exception)

        if isinstance(exception, HttpErrorBase):
            body = exception.body
            status_code = exception.status_code
        else:
            body = "Internal server error"
            status_code = 500

        return self.create_response(status_code=status_code, body=body)
