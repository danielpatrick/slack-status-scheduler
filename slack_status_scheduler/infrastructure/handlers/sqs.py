"""Process SQS message(s)."""

import json

from .base import BaseHandler


class SqsMessageHandler(BaseHandler):
    """Process SQS message(s)."""

    def parse_event(self):
        """Parse record from event."""
        self.record = None
        self.record_body = None

        records = self.event.get("Records", [])
        num_records = len(records)

        if num_records > 1:
            error_msg = (
                "Event must contain zero or one records; "
                f"found {num_records} records"
            )

            raise ValueError(error_msg)

        if num_records == 1:
            self.record = records[0]
            self.record_body = json.loads(self.record["body"])

    def loggable_event(self):
        """Return a modified version of the event object."""
        if self.record is None:
            return self.event

        return {
            **self.event,
            "Records": [
                {
                    **self.record,
                    "body": self.record_body,
                }
            ],
        }
