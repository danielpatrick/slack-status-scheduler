"""Logging library (wraps python logging module)."""

import json
import logging
import re
import sys
import traceback
from collections.abc import Mapping

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class Logger:
    """Logger class for uniform logging format across all lambdas."""

    redaction_keys = set()

    def __init__(self, service, source, additional_redaction_keys=None):
        """
        Construct an instance of Logger.

        One logger should be used throughout the lambda invocation to enable
        reuse of state.
        """
        self.service = service
        self.source = source

        if additional_redaction_keys is not None:
            self.redaction_keys.update(additional_redaction_keys)

    def _create_log(self, message, data=None, traceback=None):
        log = {
            "service": self.service,
            "source": self.source,
            "message": message,
        }

        if data is not None:
            log["data"] = self._redact_and_format(data)

        if traceback is not None:
            log["traceback"] = traceback
        elif sys.exc_info()[0] is not None:
            log["traceback"] = self._format_exception()

        return json.dumps(log)

    def _format_exception(self, exception=None):
        if exception is None:
            return traceback.format_exc()

        return "".join(
            traceback.format_exception(
                type(exception),
                exception,
                exception.__traceback__,
            )
        )

    def _redact_number(self, data, int_or_float):
        redacted = re.sub(r"\d", "N", str(data))

        return f"Redacted {int_or_float}: {redacted}"

    def _redact(self, data):
        if isinstance(data, str):
            return f"Redacted str: {len(data)} chars"
        if isinstance(data, int):
            return self._redact_number(data, "int")
        if isinstance(data, float):
            return self._redact_number(data, "float")

        return f"Redacted {type(data).__name__}"

    def _redact_and_format(self, data, keys=None):
        if keys is None:
            keys = set()

        if isinstance(data, list):
            return [self._redact_and_format(item, keys) for item in data]
        if isinstance(data, Mapping):
            return {
                k: self._redact_and_format(v, keys | {k})
                for k, v in data.items()
            }

        if keys & self.redaction_keys:
            return self._redact(data)
        elif not isinstance(data, (str, int, float)):
            return str(data)
        else:
            return data

    def debug(self, message, data=None):
        """Debug log."""
        logging.debug(self._create_log(message, data))

    def info(self, message, data=None):
        """Info log."""
        logging.info(self._create_log(message, data))

    def warning(self, message, data=None, *, exception=None):
        """Warning log."""
        if exception is None:
            traceback = None
        else:
            traceback = self._format_exception(exception)

        logging.warning(self._create_log(message, data, traceback))

    def error(self, message, data=None, *, exception=None):
        """Error log."""
        if exception is None:
            traceback = None
        else:
            traceback = self._format_exception(exception)

        logging.error(self._create_log(message, data, traceback))
