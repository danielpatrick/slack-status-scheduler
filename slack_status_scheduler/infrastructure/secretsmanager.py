"""Util for fetching values from secretsmanager."""
import json

import boto3

client = boto3.client("secretsmanager")


def get_secret_values(secret_id, *keys):
    """
    Fetch list of values from given secret.

    If only multiple values requested, returns list of strings,
    otherwise returns a string.
    """
    response = client.get_secret_value(SecretId=secret_id)

    key_values = json.loads(response["SecretString"])
    values = []

    for key in keys:
        values.append(key_values.get(key))

    return values[0] if len(keys) == 1 else values
