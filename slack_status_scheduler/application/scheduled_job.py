"""Retrieve and update status change jobs on a schedule."""

import os

import boto3
import pendulum

from domain.config import ONE_OFF_SCHEDULE_NEXT_TIME
from domain.schedules import ScheduleRule
from infrastructure.handlers.base import BaseHandler


class ScheduledJobHandler(BaseHandler):
    """Update dynamodb items based on scheduled event."""

    OFFSET_MINUTES = 15
    TABLE_NAME = os.environ["SCHEDULE_TABLE_NAME"]
    MAX_ITEMS = 100

    dynamodb_client = boto3.client("dynamodb")

    def execute(self):
        """
        Update dynamodb items.

        Fetches dynamodb items for the current hour and updates their
        "NextEventBeginHour" attribute.
        """
        event_time = self.event["time"]
        time = pendulum.parse(event_time)

        mins = time.minute

        if mins == (60 - self.OFFSET_MINUTES):
            time = time.add(minutes=self.OFFSET_MINUTES)

        if time.minute != 0:
            raise Exception(f"Unexpected invocation at {mins} past the hour")

        timestamp = time.format("YYYY-MM-DDTHH")

        for item in self.get_items_for_timestamp(timestamp):
            schedule = ScheduleRule(
                next_time=timestamp,
                timezone=item["Timezone"]["S"],
                duration=int(item["Duration"]["N"]),
                days=[int(d) for d in item.get("Days", {}).get("NS", [])],
            )

            if schedule.days:
                next_time = next(schedule).next_time
            else:
                next_time = ONE_OFF_SCHEDULE_NEXT_TIME

            team_id = item["TeamId"]
            user_id_schedule_id = item["UserIdScheduleId"]

            dynamodb_kwargs = {
                "TableName": self.TABLE_NAME,
                "Key": {
                    "TeamId": team_id,
                    "UserIdScheduleId": user_id_schedule_id,
                },
                "UpdateExpression": (
                    "SET NextEventBeginHour = :next_time REMOVE CreatedBy"
                ),
                "ConditionExpression": "NextEventBeginHour = :prev_time",
                "ExpressionAttributeValues": {
                    ":next_time": {"S": next_time},
                    ":prev_time": {"S": timestamp},
                },
            }

            self.logger.info("Updating schedule begin hour", dynamodb_kwargs)

            self.dynamodb_client.update_item(**dynamodb_kwargs)

    def get_items_for_timestamp(self, timestamp):
        """Yield items from database (generator function)."""
        last_evaluated_key = None

        while True:
            kwargs = {
                "TableName": self.TABLE_NAME,
                "IndexName": "NextEventIndex",
                "Select": "ALL_PROJECTED_ATTRIBUTES",
                "Limit": self.MAX_ITEMS,
                "KeyConditionExpression": "NextEventBeginHour = :timestamp",
                "ExpressionAttributeValues": {":timestamp": {"S": timestamp}},
            }

            if last_evaluated_key is not None:
                kwargs["ExclusiveStartKey"] = last_evaluated_key

            response = self.dynamodb_client.query(**kwargs)

            self.logger.info("Response from dynamodb", response)

            for item in response.get("Items", []):
                yield item

            last_evaluated_key = response.get("LastEvaluatedKey")

            if last_evaluated_key is None:
                break


handler = ScheduledJobHandler.handler
