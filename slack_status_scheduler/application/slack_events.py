"""Handle Slack Events API events."""

import json
import os

import boto3

from domain.slack.handlers import EventsHandler
from infrastructure.secretsmanager import get_secret_values


class SlackEventsHandler(EventsHandler):
    """Handle Slack Events API events."""

    SLACK_SIGNING_SECRET = get_secret_values(
        "SlackAppCredentials", "SigningSecret"
    )

    USER_HOME_TAB_QUEUE_URL = os.environ["USER_HOME_TAB_QUEUE_URL"]
    ACCESS_REVOKED_QUEUE_URL = os.environ["ACCESS_REVOKED_QUEUE_URL"]

    sqs_client = boto3.client("sqs")

    def app_home_opened(self, body):
        """Handle app_home_opened Events API event."""
        tab = body["event"]["tab"]

        if tab != "home":
            self.logger.info(
                "Ignoring event because not home tab",
                {"tab": tab},
            )

            return

        if "view" in body["event"]:
            self.logger.info(
                "Ignoring event because user already has a home tab view"
            )

            return

        team_id = body["team_id"]
        user_id = body["event"]["user"]

        message_body = {
            "Keys": {
                "TeamId": {"S": team_id},
                "EntityId": {"S": user_id},
            }
        }

        kwargs = {
            "QueueUrl": self.USER_HOME_TAB_QUEUE_URL,
            "MessageBody": json.dumps(message_body),
        }
        self.logger.info("Sending SQS message", kwargs)

        self.sqs_client.send_message(**kwargs)

    def tokens_revoked(self, body):
        """Handle tokens_revoked Events API event."""
        team_id = body["team_id"]
        bot_user_ids = body["event"].get("tokens", {}).get("bot", [])

        entity_ids = body["event"].get("tokens", {}).get("oauth", [])

        if bot_user_ids:
            entity_ids.append(team_id)

        message_body = {
            "team_id": team_id,
            "entity_ids": entity_ids,
        }

        kwargs = {
            "QueueUrl": self.ACCESS_REVOKED_QUEUE_URL,
            "MessageBody": json.dumps(message_body),
        }
        self.logger.info("Sending SQS message", kwargs)

        self.sqs_client.send_message(**kwargs)


handler = SlackEventsHandler.handler
