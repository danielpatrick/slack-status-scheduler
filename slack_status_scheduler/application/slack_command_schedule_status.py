"""Handle Slack /schedule-status slash command events."""

import json
import os
import re

from domain.config import MAX_EMOJIS_PER_SCHEDULE, MAX_SCHEDULES_PER_USER
from domain.exceptions import MaxSchedulesError
from domain.slack.emojis import EMOJI_SET
from domain.slack.exceptions import NotAuthenticatedError
from domain.slack.forms import schedule_status_form, send_form
from domain.slack.handlers import SlashCommandHandler
from infrastructure.secretsmanager import get_secret_values

# Store these so we can ignore them in user input (a temporary measure until
# there is an obvious way to validate emoji + modifier combinations)
SKIN_TONE_EMOJI_MODIFIERS = {
    ":skin-tone-2:",
    ":skin-tone-3:",
    ":skin-tone-4:",
    ":skin-tone-5:",
    ":skin-tone-6:",
}


class ScheduleStatusCommandHandler(SlashCommandHandler):
    """Handle Slack /schedule-status slash command events."""

    SLACK_SIGNING_SECRET = get_secret_values(
        "SlackAppCredentials", "SigningSecret"
    )
    EMOJI_SET = EMOJI_SET - SKIN_TONE_EMOJI_MODIFIERS
    EMOJI_REGEX = re.compile(":[a-z0-9-_'+]+:")
    COMMAND_NAME = "/schedule-status"
    AUTH_URL = os.environ["AUTH_URL"]

    def _parse_emojis(self, emojis_string):
        emojis = []

        for match in self.EMOJI_REGEX.finditer(emojis_string.lower()):
            if len(emojis) >= MAX_EMOJIS_PER_SCHEDULE:
                break

            if match[0] in self.EMOJI_SET:
                emojis.append(match[0])

        return emojis

    def execute(self):
        """Respond to /schedule-status slash command."""
        emojis = self._parse_emojis(self.body.get("text", ""))

        try:
            send_form(
                schedule_status_form,
                user_id=self.body["user_id"],
                team_id=self.body["team_id"],
                trigger_id=self.body["trigger_id"],
                status_emojis=emojis,
            )
        except NotAuthenticatedError:
            message = (
                f"You must <{self.AUTH_URL}|authorize the app> "
                "before you can perform this action."
            )
            body = {
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": message,
                        },
                    }
                ]
            }

            return self.create_response(
                body=json.dumps(body),
                headers={"Content-Type": "application/json"},
            )

        except MaxSchedulesError:
            message = (
                "Sorry, you can only create up to "
                f"{MAX_SCHEDULES_PER_USER} status schedules.\n\n"
                "Please delete one or more schedules to create another."
            )
            body = {
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": message,
                        },
                    }
                ]
            }

            return self.create_response(
                body=json.dumps(body),
                headers={"Content-Type": "application/json"},
            )

        return self.create_response()


handler = ScheduleStatusCommandHandler.handler
