"""Delete scheduled status."""

import json
import os

import boto3
import pendulum

from domain.db.schedules import delete_schedule
from infrastructure.handlers.sqs import SqsMessageHandler


class DeleteStatusScheduleHandler(SqsMessageHandler):
    """Delete scheduled status from dynamodb."""

    sqs_client = boto3.client("sqs")

    DELETE_SCHEDULE_QUEUE_URL = os.environ["DELETE_SCHEDULE_QUEUE_URL"]

    FIFTEEN_MINUTES_IN_SECONDS = 60 * 15

    def execute(self):
        """Delete scheduled status from dynamodb."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        image = self.record_body["OldImage"]

        old_next_time = image["NextEventBeginHour"]["S"]

        start_time = pendulum.parse(old_next_time).int_timestamp
        delay = start_time - pendulum.now(tz="UTC").int_timestamp

        if delay > 0:
            kwargs = {
                "QueueUrl": self.DELETE_SCHEDULE_QUEUE_URL,
                "MessageBody": json.dumps(self.record_body),
                "DelaySeconds": min(delay, self.FIFTEEN_MINUTES_IN_SECONDS),
            }

            self.sqs_client.send_message(**kwargs)

            return

        kwargs = {
            "team_id": image["TeamId"]["S"],
            "user_id": image["UserId"]["S"],
            "schedule_id": image["ScheduleId"]["S"],
        }

        self.logger.info("Deleting schedule", kwargs)

        delete_schedule(**kwargs)


handler = DeleteStatusScheduleHandler.handler
