"""Delete user schedules."""

from domain.db.schedules import delete_schedule, get_user_schedule_ids
from infrastructure.handlers.sqs import SqsMessageHandler


class DeleteUserSchedulesHandler(SqsMessageHandler):
    """Delete schedules for deactivated users."""

    def execute(self):
        """Delete schedules."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        user_id = self.record_body["Keys"]["EntityId"]["S"]
        team_id = self.record_body["Keys"]["TeamId"]["S"]

        schedules = get_user_schedule_ids(team_id=team_id, user_id=user_id)

        for schedule_id in schedules:
            self.logger.info(
                "Deleting schedule",
                {
                    "team": team_id,
                    "user": user_id,
                    "schedule_id": schedule_id,
                },
            )

            delete_schedule(
                team_id=team_id,
                user_id=user_id,
                schedule_id=schedule_id,
                update_user=False,
            )


handler = DeleteUserSchedulesHandler.handler
