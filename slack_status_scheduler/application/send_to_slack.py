"""Parse SQS and send status update to Slack."""

from slack_sdk import WebClient

from domain.db.users import UserInfo, get_user_info
from infrastructure.handlers.sqs import SqsMessageHandler


class SendStatusToSlackHandler(SqsMessageHandler):
    """Send status update from SQS message to Slack."""

    def execute(self):
        """Send status update to Slack."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        start_time = self.record_body["start_time"]  # noqa
        end_time = self.record_body["end_time"]
        status_text = self.record_body["status_text"]
        status_emoji = self.record_body["status_emoji"]
        overwrite = self.record_body["overwrite"]
        team_id = self.record_body["team_id"]
        user_id = self.record_body["user_id"]

        token = get_user_info(team_id, user_id, UserInfo.USER_TOKEN)

        if token is None:
            raise Exception("Unable to retreive Slack user token")

        payload = {
            "profile": {
                "status_text": status_text,
                "status_emoji": status_emoji,
                "status_expiration": end_time,
            }
        }

        client = WebClient(token=token)

        if overwrite is False and self.already_has_status(client, start_time):
            self.logger.info(
                "Not setting status because user already has one set"
            )

            return

        self.logger.info("Setting user status", payload)

        client.users_profile_set(**payload)

    def already_has_status(self, client, start_time):
        """
        Check if user already has a status set that will overlap.

        Returns True if a status is set and if that status will end after
        start_time. Returns False otherwise.
        """
        r = client.users_profile_get()

        if r["profile"]["status_text"] == "":
            return False

        if r["profile"]["status_expiration"] == 0:
            return True

        if r["profile"]["status_expiration"] > start_time:
            return True

        return False


handler = SendStatusToSlackHandler.handler
