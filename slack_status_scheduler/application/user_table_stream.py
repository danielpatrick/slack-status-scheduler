"""Publish all user table dynamodb stream items to SNS topic."""

import os

from infrastructure.handlers.dynamodb import DynamodbStreamToSnsHandler


class UserTableStreamHandler(DynamodbStreamToSnsHandler):
    """Publish stream records to SNS topic."""

    TOPIC_ARN = os.environ["USER_STREAM_TOPIC_ARN"]

    def filter(self, record):
        """
        Only send user records.

        EntityId is equal to TeamId for team records but not user records.
        """
        team_id = record["dynamodb"]["Keys"]["TeamId"]
        entity_id = record["dynamodb"]["Keys"]["EntityId"]

        return team_id != entity_id


handler = UserTableStreamHandler.handler
