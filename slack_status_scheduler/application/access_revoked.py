"""Handle access revocation events."""

from domain.db.users import delete_user_or_team
from infrastructure.handlers.sqs import SqsMessageHandler


class AccessRevokedHandler(SqsMessageHandler):
    """Delete Users or Teams who revoked app tokens."""

    def execute(self):
        """Delete users and/or teams."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        team_id = self.record_body["team_id"]

        for entity_id in self.record_body["entity_ids"]:
            self.logger.info(
                "Deleting user/team",
                {"team_id": team_id, "entity_id": entity_id},
            )

            delete_user_or_team(team_id=team_id, entity_id=entity_id)


handler = AccessRevokedHandler.handler
