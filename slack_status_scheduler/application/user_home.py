"""Create or update user home tab after sign up or other trigger."""

from domain.slack.home_tab import update_home_tab
from infrastructure.handlers.sqs import SqsMessageHandler


class UserHomeUpdateHandler(SqsMessageHandler):
    """Update user home tab in Slack."""

    def execute(self):
        """Create or update Slack user's home tab."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        if "EntityId" in self.record_body["Keys"]:
            user_id = self.record_body["Keys"]["EntityId"]["S"]
        else:
            if "OldImage" in self.record_body:
                user_id = self.record_body["OldImage"]["UserId"]["S"]
            else:
                user_id = self.record_body["NewImage"]["UserId"]["S"]

        team_id = self.record_body["Keys"]["TeamId"]["S"]

        update_home_tab(user_id=user_id, team_id=team_id)


handler = UserHomeUpdateHandler.handler
