"""Parse SQS message and convert to Slack status update payload."""

import json
import os
import random

import boto3
import pendulum

from domain.schedules.status_schedule_rule import StatusScheduleRule
from infrastructure.handlers.sqs import SqsMessageHandler


class CreateStatusMessageHandler(SqsMessageHandler):
    """Validate timing of status update and prepare message for Slack."""

    FIFTEEN_MINUTES_IN_SECONDS = 60 * 15
    QUEUE_URL = os.environ["SEND_TO_SLACK_QUEUE_URL"]

    sqs_client = boto3.client("sqs")

    def execute(self):
        """Create Slack status update payload and send to next SQS."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        event_type = self.record["messageAttributes"]["eventName"][
            "stringValue"
        ]

        if event_type == "INSERT":
            image = self.record_body["NewImage"]
        elif event_type == "MODIFY":
            image = self.record_body["OldImage"]
        else:
            self.logger.info(f"Ignoring event type {event_type}")

            return

        rule = StatusScheduleRule(
            image["NextEventBeginHour"]["S"],
            image["Timezone"]["S"],
            int(image["Duration"]["N"]),
            [int(d) for d in image.get("Days", {}).get("NS", [])],
            image["StatusText"]["S"],
            image.get("StatusEmojis", {}).get("SS", []),
            image["Overwrite"]["BOOL"],
        )

        start_time = rule.next_datetime.int_timestamp
        end_time = rule.calculate_end_time().int_timestamp

        if rule.status_emojis:
            emoji = random.choice(rule.status_emojis)  # nosec
        else:
            emoji = ""

        message = json.dumps(
            {
                "start_time": start_time,
                "end_time": end_time,
                "status_text": rule.status_text,
                "status_emoji": emoji,
                "overwrite": rule.overwrite,
                "user_id": image["UserId"]["S"],
                "team_id": image["TeamId"]["S"],
            }
        )

        self.send_status_update_to_sqs(
            message=message,
            start_time=start_time,
            end_time=end_time,
        )

    def send_status_update_to_sqs(self, message, start_time, end_time):
        """Send status update to SQS queue."""
        kwargs = {
            "QueueUrl": self.QUEUE_URL,
            "MessageBody": message,
        }

        if (pendulum.now(tz="UTC").int_timestamp + 30) > end_time:
            self.logger.warning(
                "Not setting status because end time has "
                "elapsed or is about to",
                {"end_time": str(pendulum.from_timestamp(end_time))},
            )

            return

        delay = start_time - pendulum.now(tz="UTC").int_timestamp

        if 0 < delay <= self.FIFTEEN_MINUTES_IN_SECONDS:
            kwargs["DelaySeconds"] = int(delay)
        elif delay > self.FIFTEEN_MINUTES_IN_SECONDS:
            start_dt = pendulum.from_timestamp(start_time)

            self.logger.warning(
                "It's too early to send this status message",
                {"start_time": str(start_dt)},
            )

            return

        self.logger.info("Sending SQS message", kwargs)

        self.sqs_client.send_message(**kwargs)


handler = CreateStatusMessageHandler.handler
