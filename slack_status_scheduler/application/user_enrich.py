"""Enrich user data after user sign up (currently only updates timezone)."""

from slack_sdk import WebClient

from domain.db.users import UserInfo, get_user_info, set_user_timezone
from infrastructure.handlers.sqs import SqsMessageHandler


class UserEnrichHandler(SqsMessageHandler):
    """Update user timezone following user create/update."""

    def execute(self):
        """Update user data."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        user_id = self.record_body["Keys"]["EntityId"]["S"]
        team_id = self.record_body["Keys"]["TeamId"]["S"]

        token = get_user_info(team_id, user_id, UserInfo.USER_TOKEN)

        if token is None:
            raise Exception("Unable to retreive Slack user token")

        client = WebClient(token=token)

        response = client.users_info(user=user_id)

        timezone = response["user"]["tz"]

        kwargs = {"user_id": user_id, "team_id": team_id, "timezone": timezone}

        self.logger.info("Updating user timezone", kwargs)

        set_user_timezone(**kwargs)


handler = UserEnrichHandler.handler
