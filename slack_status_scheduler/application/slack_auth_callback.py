"""Callback for Slack to redirect to."""

import os
from urllib.parse import urlencode

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from ua_parser.user_agent_parser import ParseOS

from domain.db.users import add_user
from infrastructure.handlers.api_gateway import ApiGatewayV2Handler
from infrastructure.secretsmanager import get_secret_values


class SlackAuthCallbackHandler(ApiGatewayV2Handler):
    """Slack callback redirected to after user accepts scopes on Slack site."""

    SUCCESS_URL = os.environ["SUCCESS_URL"]
    FAILURE_URL = os.environ["FAILURE_URL"]

    SLACK_CLIENT_ID, SLACK_CLIENT_SECRET = get_secret_values(
        "SlackAppCredentials", "ClientId", "ClientSecret"
    )

    SUPPORTS_SLACK_SCHEME = {
        "Android",
        "iOS",
        "Mac OS X",
        "Windows",
    }

    LOG_REDACTION_KEYS = {"code"}

    def execute(self):
        """Obtain auth0 token from Slack and redirect user."""
        request_domain = self.event["requestContext"]["domainName"]
        request_path = self.event["requestContext"]["http"]["path"]
        redirect_uri = f"https://{request_domain}{request_path}"

        code = self.event["queryStringParameters"]["code"]

        client = WebClient()

        location = self.FAILURE_URL  # default to failure

        try:
            response = client.oauth_v2_access(
                client_id=self.SLACK_CLIENT_ID,
                client_secret=self.SLACK_CLIENT_SECRET,
                code=code,
                redirect_uri=redirect_uri,
            )

            app_id = response["app_id"]
            team_id = response["team"]["id"]

            add_user(
                user_id=response["authed_user"]["id"],
                user_scope=response["authed_user"]["scope"],
                user_token=response["authed_user"]["access_token"],
                team_id=team_id,
                team_name=response["team"]["name"],
                bot_id=response["bot_user_id"],
                bot_scope=response["scope"],
                bot_token=response["access_token"],
            )

            os_info = ParseOS(self.event["headers"].get("user-agent", ""))

            if os_info["family"] in self.SUPPORTS_SLACK_SCHEME:
                params = urlencode({"team_id": team_id, "app_id": app_id})
                location = f"{self.SUCCESS_URL}?{params}"
            else:
                location = self.SUCCESS_URL
        except SlackApiError as e:
            self.logger.error(
                "Slack API call failed with error:",
                {"response": e.response},
                exception=e,
            )
        except Exception as e:
            self.logger.error(
                "Error encountered in oauth callback - redirecting to failure",
                exception=e,
            )

        return self.create_response(
            status_code=302,
            headers={"Location": location},
        )


handler = SlackAuthCallbackHandler.handler
