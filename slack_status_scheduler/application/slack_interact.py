"""Handle Slack interaction requests."""

import json

from domain.config import MAX_SCHEDULES_PER_USER
from domain.db.schedules import (
    DatePastError,
    InvalidEndDateError,
    InvalidEndTimeError,
    TimePastError,
    create_daily_schedule,
    create_one_off_schedule,
    delete_schedule,
)
from domain.exceptions import MaxSchedulesError
from domain.slack.handlers import InteractHandler
from infrastructure.handlers.api_gateway import HttpBadRequestError
from infrastructure.secretsmanager import get_secret_values


class SlackInteractionHandler(InteractHandler):
    """Handle Slack interaction events."""

    SLACK_SIGNING_SECRET = get_secret_values(
        "SlackAppCredentials", "SigningSecret"
    )

    def shortcut(self, *, team_id, user_id):
        """Handle Slack global shortcut events."""
        callback_id = self.body.get("callback_id")

        raise HttpBadRequestError(
            f"Unexpected shortcut with callback_id '{callback_id}'"
        )

    def view_submission(self, *, team_id, user_id):
        """Handle Slack view_submission events."""
        callback_id = self.body.get("view", {}).get("callback_id")

        if callback_id == "daily-status-submit":
            return self.view_submission_daily_status_submit(
                team_id=team_id, user_id=user_id
            )
        if callback_id == "schedule-status-submit":
            return self.view_submission_schedule_status_submit(
                team_id=team_id, user_id=user_id
            )
        else:
            raise HttpBadRequestError(
                f"Unexpected view.callback_id '{callback_id}'"
            )

    def view_submission_daily_status_submit(self, *, team_id, user_id):
        """Validate and save status schedule."""
        state = self.body["view"]["state"]["values"]

        status_text = state["status-text"]["input-text"]["value"]
        start_time = int(
            state["start-time"]["select-start-time"]["selected_option"][
                "value"
            ]
        )
        duration = int(
            state["duration"]["select-duration"]["selected_option"]["value"]
        )
        overwrite = (
            len(state["overwrite"]["toggle-overwrite"]["selected_options"]) > 0
        )
        days = [
            int(option["value"])
            for option in state["days-of-week"]["select-days"][
                "selected_options"
            ]
        ]

        if not days:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "days-of-week": "You must select at least one day.",
                    },
                }
            )

        private_metadata = json.loads(self.body["view"]["private_metadata"])

        timezone = private_metadata["timezone"]

        status_emojis = private_metadata.get("emojis", [])

        try:
            create_daily_schedule(
                team_id=team_id,
                user_id=user_id,
                status_text=status_text,
                status_emojis=status_emojis,
                timezone=timezone,
                hour=start_time,
                duration=duration,
                overwrite=overwrite,
                days=days,
            )
        except MaxSchedulesError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "status-text": (
                            "You cannot create more than "
                            f"{MAX_SCHEDULES_PER_USER} schedules."
                        )
                    },
                }
            )

    def view_submission_schedule_status_submit(self, *, team_id, user_id):
        """Validate and save status schedule."""
        state = self.body["view"]["state"]["values"]

        status_text = state["status-text"]["input-text"]["value"]
        start_date_str = state["start-date"]["select-start-date"][
            "selected_date"
        ]
        start_time = int(
            state["start-time"]["select-start-time"]["selected_option"][
                "value"
            ]
        )
        end_date_str = state["end-date"]["select-end-date"]["selected_date"]
        end_time = int(
            state["end-time"]["select-end-time"]["selected_option"]["value"]
        )
        overwrite = (
            len(state["overwrite"]["toggle-overwrite"]["selected_options"]) > 0
        )

        private_metadata = json.loads(self.body["view"]["private_metadata"])

        timezone = private_metadata["timezone"]

        status_emojis = private_metadata.get("emojis", [])

        try:
            create_one_off_schedule(
                team_id=team_id,
                user_id=user_id,
                status_text=status_text,
                status_emojis=status_emojis,
                timezone=timezone,
                start_date_str=start_date_str,
                start_hour=start_time,
                end_date_str=end_date_str,
                end_hour=end_time,
                overwrite=overwrite,
            )
        except MaxSchedulesError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "status-text": (
                            "You cannot create more than "
                            f"{MAX_SCHEDULES_PER_USER} schedules."
                        )
                    },
                }
            )
        except DatePastError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "start-date": (
                            "Start date and time must be at least "
                            "one minute in the future."
                        )
                    },
                }
            )
        except TimePastError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "start-time": (
                            "Start date and time must be at least "
                            "one minute in the future."
                        )
                    },
                }
            )
        except InvalidEndDateError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "end-date": "End date cannot be before start date."
                    },
                }
            )
        except InvalidEndTimeError:
            return json.dumps(
                {
                    "response_action": "errors",
                    "errors": {
                        "end-time": (
                            "End date/time must be at least one "
                            "hour after start date/time."
                        )
                    },
                }
            )

    def block_actions(self, *, team_id, user_id):
        """Handle Slack block_actions events."""
        view_type = self.body.get("view", {}).get("type")

        if view_type == "home":
            self.block_actions_home(team_id=team_id, user_id=user_id)
        elif view_type == "modal":
            self.block_actions_modal(team_id=team_id, user_id=user_id)
        else:
            raise HttpBadRequestError(f"Unexpected view.type '{view_type}'")

    def block_actions_home(self, *, team_id, user_id):
        """Process user actions from the home tab."""
        for action in self.body["actions"]:
            if action["value"] == "delete_schedule":
                delete_schedule(
                    team_id=team_id,
                    user_id=user_id,
                    schedule_id=action["block_id"],
                )

    def block_actions_modal(self, *, team_id, user_id):
        """Process user actions from modals."""
        self.logger.info("Ignoring block_actions type 'modal'")


handler = SlackInteractionHandler.handler
