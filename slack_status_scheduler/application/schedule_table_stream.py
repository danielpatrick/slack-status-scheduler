"""Publish all schedule table dynamodb stream items to SNS topic."""

import os

from infrastructure.handlers.dynamodb import DynamodbStreamToSnsHandler


class ScheduleTableStreamHandler(DynamodbStreamToSnsHandler):
    """Publish stream records to SNS topic."""

    TOPIC_ARN = os.environ["SCHEDULE_STREAM_TOPIC_ARN"]

    def create_message_attributes(self, record):
        """Create SNS message attributes from dynamodb stream record."""
        attributes = super().create_message_attributes(record)

        image = record["dynamodb"].get("NewImage", {})

        if "NextEventBeginHour" in image:
            attributes["nextTime"] = {
                "StringValue": image["NextEventBeginHour"].get("S", "N/A"),
                "DataType": "String",
            }

        return attributes


handler = ScheduleTableStreamHandler.handler
