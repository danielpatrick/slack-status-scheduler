"""Post schedule creation job."""

from slack_sdk import WebClient

from domain.db.users import BotInfo, get_user_info
from infrastructure.handlers.sqs import SqsMessageHandler
from infrastructure.secretsmanager import get_secret_values


class ScheduleCreatedHandler(SqsMessageHandler):
    """Perform any tasks required after creation of schedule for user."""

    APP_ID = get_secret_values("SlackAppCredentials", "AppId")

    def execute(self):
        """Message user to confirm schedule creation."""
        if self.record is None:
            self.logger.info("No record in event; nothing to do")

            return

        image = self.record_body.get("NewImage")

        created_by = image.get("CreatedBy", {}).get("S")

        if created_by != "USER":
            self.logger.info(
                "Item not created by user - not sending message",
                {"created_by": created_by},
            )

            return

        user_id = image["UserId"]["S"]
        team_id = image["TeamId"]["S"]

        self.send_confirm_message(user_id=user_id, team_id=team_id)

    def send_confirm_message(self, *, user_id, team_id):
        """Send user a message to confirm creation of schedule."""
        token = get_user_info(team_id, user_id, BotInfo.BOT_TOKEN)

        if token is None:
            raise Exception("Unable to retreive Slack bot token")

        client = WebClient(token=token)

        home_url = f"slack://app?team={team_id}&id={self.APP_ID}&tab=home"

        kwargs = {
            "channel": user_id,
            "user": user_id,
            "text": (
                f"Scheduled status created. Click <{home_url}|"
                "here> to view and manage your scheduled statuses."
            ),
        }

        self.logger.info("Sending ephemeral message to user", kwargs)

        return client.chat_postEphemeral(**kwargs)


handler = ScheduleCreatedHandler.handler
