"""Exception classes shared by various parts of the code."""


class MaxSchedulesError(Exception):
    """Raised when a user tries to add more schedules than allowed."""

    pass
