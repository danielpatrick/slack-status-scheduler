"""Rule for when to set Slack status."""

from .exceptions import ValidationException
from .schedule_rule import ScheduleRule


class StatusScheduleRule(ScheduleRule):
    """Representation of rule for setting Slack statuses."""

    def __init__(
        self,
        next_time,
        timezone,
        duration,
        days,
        status_text,
        status_emojis,
        overwrite,
    ):
        """Create a rule for setting Slack statuses on a schedule."""
        super().__init__(
            next_time=next_time,
            timezone=timezone,
            duration=duration,
            days=days,
        )
        self.status_text = self._validated_status_text(status_text)
        self.status_emojis = self._validated_status_emojis(status_emojis[:])
        self.overwrite = self._validated_overwrite(overwrite)

    def __next__(self):
        """Get the next instance of this rule."""
        return self.__class__(
            next_time=self._incremented_next_time(),
            timezone=self.timezone,
            duration=self.duration,
            days=self.days,
            status_text=self.status_text,
            status_emojis=self.status_emojis,
            overwrite=self.overwrite,
        )

    def __eq__(self, other):
        """Check instance attributes for equality."""
        return (
            super().__eq__(other)
            and self.status_text == other.status_text
            and self.status_emojis == other.status_emojis
        )

    def _validated_status_text(self, status_text):
        if 0 < len(status_text) <= 100 and isinstance(status_text, str):
            return status_text

        raise ValidationException(
            "Status text must be a string with length 1-100 inclusive."
        )

    def _valid_emoji(self, emoji_string):
        return (
            2 < len(emoji_string) <= 100
            and emoji_string.startswith(":")
            and emoji_string.endswith(":")
        )

    def _validated_status_emojis(self, status_emojis):
        if isinstance(status_emojis, list) and all(
            self._valid_emoji(e) for e in status_emojis
        ):
            return status_emojis

        raise ValidationException(
            "Status emojis must be 3-100 characters inclusive and "
            "start and end with a colon"
        )

    def _validated_overwrite(self, overwrite):
        if isinstance(overwrite, bool):
            return overwrite

        raise ValidationException("Overwrite must be a boolean")
