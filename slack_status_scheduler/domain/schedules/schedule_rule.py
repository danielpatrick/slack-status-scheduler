"""Rule for a daily scheduled event."""

import re

import pendulum
from pendulum.tz.zoneinfo.exceptions import InvalidTimezone

from .exceptions import ValidationException

simple_date_regex = re.compile(r"\d{4}-\d{2}-\d{2}T\d{2}")


class ScheduleRule:
    """Generic daily schedule rule."""

    PENDULUM_NEXT_TIME_FORMAT = "YYYY-MM-DDTHH"

    def __init__(self, next_time, timezone, duration, days):
        """Create a rule for a schedule."""
        self.next_datetime = self._validated_next_time(next_time)
        self.timezone = self._validated_timezone(timezone)
        self.duration = self._validated_duration(duration)
        self.days = sorted(set(self._validated_days(days)))

    def calculate_end_time(self):
        """Calculate the status expiration time."""
        return (
            self.next_datetime.in_tz(self.timezone)
            .naive()
            .add(hours=self.duration)
            .in_tz(self.timezone)
            .in_tz("UTC")
        )

    @property
    def next_time(self):
        """Get next_time in YYYY-MM-DDTHH format."""
        return self.next_datetime.format(self.PENDULUM_NEXT_TIME_FORMAT)

    def __next__(self):
        """Get the next instance of this rule."""
        return self.__class__(
            next_time=self._incremented_next_time(),
            timezone=self.timezone,
            duration=self.duration,
            days=self.days,
        )

    def __eq__(self, other):
        """Check instance attributes for equality."""
        return (
            self.next_datetime == other.next_datetime
            and self.timezone == other.timezone
            and self.duration == other.duration
            and self.days == other.days
        )

    def _incremented_next_time(self):
        if not any(d in self.days for d in range(7)):
            raise Exception("I don't fancy an infinite loop, do you?")

        days_to_add = 1
        today = self.next_datetime.weekday()

        while (today + days_to_add) % 7 not in self.days:
            days_to_add += 1

        next_time = (
            self.next_datetime.in_tz(self.timezone)
            .add(days=days_to_add)
            .in_tz("UTC")
        )

        return next_time.format(self.PENDULUM_NEXT_TIME_FORMAT)

    def _convert_to_datetime(self, timestamp):
        return pendulum.from_format(timestamp, self.PENDULUM_NEXT_TIME_FORMAT)

    def _validated_next_time(self, next_time):
        try:
            dt = self._convert_to_datetime(next_time)

            if simple_date_regex.match(next_time):
                return dt
        except ValueError:
            pass

        raise ValidationException(
            f"Next time must be in the format {self.PENDULUM_NEXT_TIME_FORMAT}"
        )

    def _validated_timezone(self, timezone):
        try:
            pendulum.timezone(timezone)
            return timezone
        except InvalidTimezone:
            pass

        raise ValidationException(f"{timezone} is not a valid timezone")

    def _validated_duration(self, duration):
        if duration >= 1 and isinstance(duration, int):
            return duration

        raise ValidationException(
            "Duration must be an integer greater than or equal to 1"
        )

    def _validated_days(self, days):
        if all(day in range(7) for day in days):
            return days

        raise ValidationException(
            "Days must be a list of integers within the range 0-6 inclusive"
        )
