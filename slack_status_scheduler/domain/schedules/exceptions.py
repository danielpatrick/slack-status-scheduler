"""Exceptions used by schedule rule classes."""


class ValidationException(Exception):
    """Class for schedule argument validation failures."""

    pass
