"""Status schedule CRUD."""

import os
from uuid import uuid4

import boto3
import pendulum

from domain.config import MAX_SCHEDULES_PER_USER, ONE_OFF_SCHEDULE_NEXT_TIME
from domain.exceptions import MaxSchedulesError
from domain.schedules.status_schedule_rule import StatusScheduleRule

SCHEDULE_TABLE_NAME = os.environ["SCHEDULE_TABLE_NAME"]
USER_TABLE_NAME = os.environ["USER_TABLE_NAME"]

dynamodb_client = boto3.client("dynamodb")


class DatePastError(Exception):
    """Raised when start date is before today."""

    pass


class TimePastError(Exception):
    """Raised when start date/time is in the past."""

    pass


class InvalidEndDateError(Exception):
    """Raised when end date is before start date."""

    pass


class InvalidEndTimeError(Exception):
    """Raised when end date/date is before start date/time."""

    pass


def create_daily_schedule(
    *,
    team_id,
    user_id,
    status_text,
    status_emojis,
    timezone,
    hour,
    duration,
    overwrite,
    days,
):
    """Save a daily status schedule to the database."""
    schedule_id = str(uuid4())
    hour = int(hour)
    duration = int(duration)
    days = [int(d) for d in days]

    now = pendulum.now(tz=timezone)

    dt = now.at(hour)

    if now > dt:
        dt = dt.add(days=1)

    if not 0 < duration <= 24:
        raise Exception(
            "Daily status duration must be between 1 and 24 hours inclusive"
        )

    if not days:
        raise Exception("One or more days required for daily scheduled status")

    while dt.weekday() not in days:
        dt = dt.add(days=1)

    next_time = dt.in_tz("UTC").format("YYYY-MM-DDTHH")

    item = {
        "TeamId": {"S": team_id},
        "UserIdScheduleId": {"S": f"{user_id}#{schedule_id}"},
        "CreatedBy": {"S": "USER"},
        "Overwrite": {"BOOL": overwrite},
        "NextEventBeginHour": {"S": next_time},
        "StatusText": {"S": status_text},
        "UserId": {"S": user_id},
        "ScheduleId": {"S": schedule_id},
        "Timezone": {"S": timezone},
        "Hour": {"N": str(hour)},
        "Duration": {"N": str(duration)},
        "Days": {"NS": [str(d) for d in days]},
    }

    if status_emojis:
        item["StatusEmojis"] = {"SS": status_emojis}

    put_schedule = {
        "Put": {
            "TableName": SCHEDULE_TABLE_NAME,
            "Item": item,
        }
    }

    increment_user_schedule_count = {
        "Update": {
            "TableName": USER_TABLE_NAME,
            "Key": {"TeamId": {"S": team_id}, "EntityId": {"S": user_id}},
            "UpdateExpression": "SET ScheduleCount = ScheduleCount + :One",
            "ConditionExpression": "ScheduleCount < :MaxSchedules",
            "ExpressionAttributeValues": {
                ":One": {"N": "1"},
                ":MaxSchedules": {"N": str(MAX_SCHEDULES_PER_USER)},
            },
        }
    }

    try:
        dynamodb_client.transact_write_items(
            TransactItems=[put_schedule, increment_user_schedule_count]
        )
    except dynamodb_client.exceptions.TransactionCanceledException as e:
        if "ConditionalCheckFailed" in str(e):
            raise MaxSchedulesError(
                "User is trying to create more than the "
                "allowed number of schedules"
            )

        raise e


def create_one_off_schedule(
    *,
    team_id,
    user_id,
    status_text,
    status_emojis,
    timezone,
    start_date_str,
    start_hour,
    end_date_str,
    end_hour,
    overwrite,
):
    """Save a one-off status schedule to the database."""
    schedule_id = str(uuid4())
    start_hour = int(start_hour)
    start_date = pendulum.parse(start_date_str, tz=timezone)

    if pendulum.today(tz=timezone) > start_date:
        raise DatePastError("Scheduled status must be today or later")

    start_dt = start_date.at(start_hour)

    if pendulum.now(tz=timezone).add(minutes=1) > start_dt:
        raise TimePastError(
            "Scheduled status must be at least one minute in the future"
        )

    end_hour = int(end_hour)
    end_date = pendulum.parse(end_date_str, tz=timezone)

    if start_date > end_date:
        raise InvalidEndDateError("End date cannot be before start date")

    end_dt = end_date.at(end_hour)
    duration = (end_dt.naive() - start_dt.naive()).in_hours()

    if duration < 1:
        raise InvalidEndTimeError(
            "End date/time must be at least one hour after start date/time"
        )

    next_time = start_dt.in_tz("UTC").format("YYYY-MM-DDTHH")

    item = {
        "TeamId": {"S": team_id},
        "UserIdScheduleId": {"S": f"{user_id}#{schedule_id}"},
        "CreatedBy": {"S": "USER"},
        "Overwrite": {"BOOL": overwrite},
        "NextEventBeginHour": {"S": next_time},
        "StatusText": {"S": status_text},
        "UserId": {"S": user_id},
        "ScheduleId": {"S": schedule_id},
        "Timezone": {"S": timezone},
        "Hour": {"N": str(start_hour)},
        "Duration": {"N": str(duration)},
    }

    if status_emojis:
        item["StatusEmojis"] = {"SS": status_emojis}

    put_schedule = {
        "Put": {
            "TableName": SCHEDULE_TABLE_NAME,
            "Item": item,
        }
    }

    increment_user_schedule_count = {
        "Update": {
            "TableName": USER_TABLE_NAME,
            "Key": {"TeamId": {"S": team_id}, "EntityId": {"S": user_id}},
            "UpdateExpression": "SET ScheduleCount = ScheduleCount + :One",
            "ConditionExpression": "ScheduleCount < :MaxSchedules",
            "ExpressionAttributeValues": {
                ":One": {"N": "1"},
                ":MaxSchedules": {"N": str(MAX_SCHEDULES_PER_USER)},
            },
        }
    }

    try:
        dynamodb_client.transact_write_items(
            TransactItems=[put_schedule, increment_user_schedule_count]
        )
    except dynamodb_client.exceptions.TransactionCanceledException as e:
        if "ConditionalCheckFailed" in str(e):
            raise MaxSchedulesError(
                "User is trying to create more than the "
                "allowed number of schedules"
            )

        raise e


def delete_schedule(*, team_id, user_id, schedule_id, update_user=True):
    """Delete the status schedule from the database."""
    delete_schedule_kwargs = {
        "TableName": SCHEDULE_TABLE_NAME,
        "Key": {
            "TeamId": {"S": team_id},
            "UserIdScheduleId": {"S": f"{user_id}#{schedule_id}"},
        },
    }

    if not update_user:
        dynamodb_client.delete_item(**delete_schedule_kwargs)

        return

    update_user_kwargs = {
        "TableName": USER_TABLE_NAME,
        "Key": {"TeamId": {"S": team_id}, "EntityId": {"S": user_id}},
        "UpdateExpression": (
            "SET ScheduleCount = if_not_exists(ScheduleCount, :Zero) - :One"
        ),
        "ExpressionAttributeValues": {
            ":Zero": {"N": "0"},
            ":One": {"N": "1"},
        },
    }

    dynamodb_client.transact_write_items(
        TransactItems=[
            {"Delete": delete_schedule_kwargs},
            {"Update": update_user_kwargs},
        ]
    )


def get_user_schedule_ids(*, team_id, user_id):
    """Retreive the user's schedule ids from the database."""
    response = dynamodb_client.query(
        TableName=SCHEDULE_TABLE_NAME,
        Limit=10,
        KeyConditionExpression=(
            "TeamId = :TeamId AND begins_with(UserIdScheduleId, :UserId)"
        ),
        ExpressionAttributeValues={
            ":TeamId": {"S": team_id},
            ":UserId": {"S": user_id},
        },
        ProjectionExpression="ScheduleId",
    )

    return [item["ScheduleId"]["S"] for item in response.get("Items", [])]


def get_user_schedules(*, team_id, user_id):
    """Retreive the user's schedules from the database."""
    response = dynamodb_client.query(
        TableName=SCHEDULE_TABLE_NAME,
        Limit=10,
        KeyConditionExpression=(
            "TeamId = :TeamId AND begins_with(UserIdScheduleId, :UserId)"
        ),
        ExpressionAttributeValues={
            ":TeamId": {"S": team_id},
            ":UserId": {"S": user_id},
        },
    )

    return {
        item["ScheduleId"]["S"]: StatusScheduleRule(
            item["NextEventBeginHour"]["S"],
            item["Timezone"]["S"],
            int(item["Duration"]["N"]),
            [int(d) for d in item.get("Days", {}).get("NS", [])],
            item["StatusText"]["S"],
            item.get("StatusEmojis", {}).get("SS", []),
            item["Overwrite"]["BOOL"],
        )
        for item in response.get("Items", [])
        if item["NextEventBeginHour"]["S"] != ONE_OFF_SCHEDULE_NEXT_TIME
    }
