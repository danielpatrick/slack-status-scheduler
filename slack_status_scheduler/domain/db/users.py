"""User info CRUD."""

import os
from enum import Enum

import boto3
import pendulum

TABLE_NAME = os.environ["USER_TABLE_NAME"]

dynamodb_client = boto3.client("dynamodb")


class RecordNotFoundError(Exception):
    """Raised when an attempted lookup cannot find the record."""

    pass


class UserInfo(Enum):
    """Types of user info that can be fetched from the DB."""

    USER_TOKEN = "UserToken"  # nosec
    USER_TIMEZONE = "UserTimezone"
    NUM_SCHEDULES = "ScheduleCount"


class BotInfo(Enum):
    """Types of bot info that can be fetched from the DB."""

    BOT_TOKEN = "BotToken"  # nosec


def _unpack_dynamodb_value(dynamodb_item, key):
    if key not in dynamodb_item:
        return None

    [(k, v)] = dynamodb_item[key].items()

    # TODO: add branches for more types as we need them
    if k == "S":
        return str(v)
    elif k == "N":
        if "." in v:
            return float(v)
        else:
            return int(v)

    raise Exception(f"Unhandled dynamodb type received: {k}")


def get_user_info(team_id, user_id, *info_list):
    """
    Get user info of the type(s) specified.

    Returns None if the we do not hold that information for the specified user.

    Returns list if multiple requested, ordered as per the supplied args.
    """
    if not info_list:
        raise TypeError(
            "Must supply at least one type of user info to be retrieved"
        )

    non_key_projection = ", ".join(info.value for info in info_list)
    projection = f"TeamId, EntityId, {non_key_projection}"
    keys = []

    if any(isinstance(i, BotInfo) for i in info_list):
        keys.append({"TeamId": {"S": team_id}, "EntityId": {"S": team_id}})
    if any(isinstance(i, UserInfo) for i in info_list):
        keys.append({"TeamId": {"S": team_id}, "EntityId": {"S": user_id}})

    response = dynamodb_client.batch_get_item(
        RequestItems={
            TABLE_NAME: {
                "Keys": keys,
                "ProjectionExpression": projection,
            },
        },
    )

    items = response.get("Responses", {}).get(TABLE_NAME, [])

    if len(items) < len(keys):
        raise RecordNotFoundError(
            "At least one requested record does not exist"
        )

    team = {}
    user = {}

    for item in items:
        if item["EntityId"]["S"] == team_id:
            team = item
        else:
            user = item

    result = []

    for info in info_list:
        result.append(
            _unpack_dynamodb_value(
                team if isinstance(info, BotInfo) else user,
                info.value,
            )
        )

    if len(info_list) == 1:
        return result[0]

    return result


def _update_dict(team_id, entity_id, to_update, schedule_count=False):
    update_expression_key_values = ", ".join(
        f"#{k} = :{k}" for k, _ in to_update
    )
    update_expression = f"SET {update_expression_key_values}"
    attribute_names = {f"#{k}": k for k, _ in to_update}
    attribute_values = {f":{k}": {"S": v} for k, v in to_update}

    if schedule_count:
        update_expression += (
            ", #ScheduleCount = if_not_exists(#ScheduleCount, :Zero)"
        )
        attribute_names["#ScheduleCount"] = "ScheduleCount"
        attribute_values[":Zero"] = {"N": "0"}

    return {
        "Update": {
            "TableName": TABLE_NAME,
            "Key": {"TeamId": {"S": team_id}, "EntityId": {"S": entity_id}},
            "UpdateExpression": update_expression,
            "ExpressionAttributeNames": attribute_names,
            "ExpressionAttributeValues": attribute_values,
        }
    }


# TODO: error handling
def add_user(
    *,
    user_id,
    user_scope,
    user_token,
    team_id,
    team_name,
    bot_id,
    bot_scope,
    bot_token,
):
    """
    Store the user in the datbase.

    If user already exists matching fields will be overwritten.
    """
    update_time = str(pendulum.now().int_timestamp)

    team_to_update = [
        ("TeamName", team_name),
        ("BotId", bot_id),
        ("BotScope", bot_scope),
        ("BotToken", bot_token),
        ("UpdatedTime", update_time),
    ]

    user_to_update = [
        ("UserScope", user_scope),
        ("UserToken", user_token),
        ("UpdatedTime", update_time),
    ]

    dynamodb_client.transact_write_items(
        TransactItems=[
            _update_dict(team_id, team_id, team_to_update),
            _update_dict(team_id, user_id, user_to_update, True),
        ]
    )


# TODO: error handling
def set_user_timezone(*, user_id, team_id, timezone):
    """Set user timezone (assumes user exists)."""
    dynamodb_client.update_item(
        TableName=TABLE_NAME,
        Key={"TeamId": {"S": team_id}, "EntityId": {"S": user_id}},
        UpdateExpression="SET UserTimezone = :timezone",
        ExpressionAttributeValues={
            ":timezone": {"S": timezone},
        },
    )


# TODO: error handling
def delete_user_or_team(*, team_id, entity_id):
    """Delete user or team if it exists in the database."""
    dynamodb_client.delete_item(
        TableName=TABLE_NAME,
        Key={"TeamId": {"S": team_id}, "EntityId": {"S": entity_id}},
    )
