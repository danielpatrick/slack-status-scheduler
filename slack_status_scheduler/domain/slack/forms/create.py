"""Create a Slack modal form."""

import json


def create_metadata(timezone, emojis):
    """Generate metadata for Slack modal form."""
    metadata = {"timezone": timezone}

    if emojis:
        metadata["emojis"] = emojis

    return json.dumps(metadata)


def form_creator(*, title, callback_id, metadata, blocks):
    """Crete a Slack modal form."""
    return {
        "type": "modal",
        "title": {"type": "plain_text", "text": title},
        "submit": {"type": "plain_text", "text": "Create"},
        "close": {"type": "plain_text", "text": "Cancel"},
        "private_metadata": metadata,
        "callback_id": callback_id,
        "blocks": blocks,
    }
