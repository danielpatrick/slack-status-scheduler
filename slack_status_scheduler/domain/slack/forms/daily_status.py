"""Daily status schedule Slack modal."""

from .blocks import (
    days_of_week_block,
    duration_block,
    emojis_block,
    overwrite_block,
    status_text_block,
    time_block,
    timezone_block,
)
from .create import create_metadata, form_creator


def daily_status_form(timezone, emojis=None):
    """Form for daily scheduled status."""
    return form_creator(
        title="Schedule a daily status",
        callback_id="daily-status-submit",
        metadata=create_metadata(timezone, emojis),
        blocks=[
            status_text_block,
            emojis_block(emojis),
            overwrite_block,
            time_block("Start time", 12),
            timezone_block(timezone),
            duration_block,
            days_of_week_block,
        ],
    )
