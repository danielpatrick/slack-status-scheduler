"""Create and send a modal form to Slack."""

from slack_sdk import WebClient

from domain.config import MAX_SCHEDULES_PER_USER
from domain.db.users import (
    BotInfo,
    RecordNotFoundError,
    UserInfo,
    get_user_info,
)
from domain.exceptions import MaxSchedulesError
from domain.slack.exceptions import NotAuthenticatedError


def send_form(
    create_form, *, user_id, team_id, trigger_id, status_emojis=None
):
    """Create a modal in Slack for user to schedule a one-off status update."""
    try:
        token, timezone, num_schedules = get_user_info(
            team_id,
            user_id,
            BotInfo.BOT_TOKEN,
            UserInfo.USER_TIMEZONE,
            UserInfo.NUM_SCHEDULES,
        )
    except RecordNotFoundError:
        raise NotAuthenticatedError(
            "Cannot create schedules until user has authorized the app"
        )

    if num_schedules >= MAX_SCHEDULES_PER_USER:
        raise MaxSchedulesError(
            "User is trying to create more than the "
            "allowed number of schedules"
        )

    if token is None:
        raise Exception("Unable to retreive Slack bot token")

    if timezone is None:
        raise Exception("Unable to retreive Slack user's timezone")

    form = create_form(timezone, status_emojis)

    client = WebClient(token=token)

    return client.views_open(trigger_id=trigger_id, view=form)
