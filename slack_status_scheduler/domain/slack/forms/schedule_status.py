"""One-off status schedule Slack modal."""

import pendulum

from .blocks import (
    date_block,
    emojis_block,
    overwrite_block,
    status_text_block,
    time_block,
    timezone_block,
)
from .create import create_metadata, form_creator


def schedule_status_form(timezone, emojis=None):
    """Form for one-off scheduled status."""
    date_str = pendulum.now(tz=timezone).to_date_string()

    return form_creator(
        title="Schedule a status",
        callback_id="schedule-status-submit",
        metadata=create_metadata(timezone, emojis),
        blocks=[
            status_text_block,
            emojis_block(emojis),
            overwrite_block,
            date_block("Start date", date_str),
            time_block("Start time", 12),
            timezone_block(timezone),
            date_block("End date", date_str),
            time_block("End time", 13),
        ],
    )
