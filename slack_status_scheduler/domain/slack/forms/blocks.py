"""Block literals and block factories to be used in modal forms."""

from domain.config import DEFAULT_EMOJI

status_text_block = {
    "block_id": "status-text",
    "type": "input",
    "label": {"type": "plain_text", "text": "Status text"},
    "element": {
        "type": "plain_text_input",
        "action_id": "input-text",
        "min_length": 1,
        "max_length": 100,
        "placeholder": {
            "type": "plain_text",
            "text": "eg Out for lunch",
        },
    },
}


def emojis_block(emojis):
    """Section block for displaying emojis."""
    if emojis:
        emoji_text = "*Emojis*\n\n" + " ".join(emojis)
    else:
        emoji_text = (
            f"*Emojis* (none selected, using default)\n\n{DEFAULT_EMOJI}"
        )

    return {
        "block_id": "emojis",
        "type": "section",
        "text": {"type": "mrkdwn", "text": emoji_text},
    }


def timezone_block(timezone):
    """Context block for displaying timezone."""
    return {
        "block_id": "timezone",
        "type": "context",
        "elements": [
            {
                "type": "plain_text",
                "text": f"Your timezone is {timezone}",
            }
        ],
    }


def date_block(label, date_str):
    """Input block for date selection."""
    block_id = label.lower().replace(" ", "-")

    return {
        "block_id": block_id,
        "type": "input",
        "label": {"type": "plain_text", "text": label},
        "element": {
            "type": "datepicker",
            "action_id": f"select-{block_id}",
            "initial_date": date_str,
            "placeholder": {
                "type": "plain_text",
                "text": f"Select {label.lower()}",
            },
        },
    }


def _time_option(hour):
    return {
        "text": {
            "type": "plain_text",
            "text": f"{hour:02}:00",
        },
        "value": f"{hour:02}",
    }


def time_block(label, default_hour):
    """Input block for 24 hour time selection."""
    block_id = label.lower().replace(" ", "-")

    return {
        "block_id": block_id,
        "type": "input",
        "label": {"type": "plain_text", "text": label},
        "element": {
            "type": "static_select",
            "action_id": f"select-{block_id}",
            "placeholder": {
                "type": "plain_text",
                "text": f"Select {label.lower()}",
            },
            "initial_option": _time_option(default_hour),
            "options": [_time_option(n) for n in range(0, 24)],
        },
    }


def _duration_option(hours):
    return {
        "text": {
            "type": "plain_text",
            "text": f"{hours} hours",
        },
        "value": f"{hours}",
    }


duration_block = {
    "block_id": "duration",
    "type": "input",
    "label": {
        "type": "plain_text",
        "text": "Duration",
    },
    "element": {
        "type": "static_select",
        "action_id": "select-duration",
        "placeholder": {
            "type": "plain_text",
            "text": "Pick a duration in hours",
        },
        "initial_option": _duration_option(1),
        "options": [_duration_option(n) for n in range(1, 25)],
    },
}


def _overwrite_option():
    return {
        "text": {
            "type": "plain_text",
            "text": "Overwrite status if already set",
        },
        "value": "overwrite",
    }


overwrite_block = {
    "block_id": "overwrite",
    "type": "actions",
    "elements": [
        {
            "type": "checkboxes",
            "action_id": "toggle-overwrite",
            "initial_options": [_overwrite_option()],
            "options": [_overwrite_option()],
        },
    ],
}


def _day_of_week_option(n, day):
    return {
        "text": {"type": "plain_text", "text": day},
        "value": str(n),
    }


days_of_week_block = {
    "block_id": "days-of-week",
    "type": "input",
    "label": {"type": "plain_text", "text": "Repeat every"},
    "element": {
        "type": "checkboxes",
        "action_id": "select-days",
        "initial_options": [
            _day_of_week_option(n, d)
            for n, d in enumerate(
                (
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                )
            )
        ],
        "options": [
            _day_of_week_option(n, d)
            for n, d in enumerate(
                (
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday",
                )
            )
        ],
    },
}
