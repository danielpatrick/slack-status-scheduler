"""Exception classes relating to Slack."""


class NotAuthenticatedError(Exception):
    """Raised when a user has not authorised the app with Slack."""

    pass
