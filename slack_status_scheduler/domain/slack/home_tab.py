"""Status schedule Slack modal."""

import os

from slack_sdk import WebClient

from domain.config import MAX_SCHEDULES_PER_USER
from domain.db.schedules import get_user_schedules
from domain.db.users import (
    BotInfo,
    RecordNotFoundError,
    UserInfo,
    get_user_info,
)

AUTH_URL = os.environ["AUTH_URL"]
HOME_URL = os.environ["HOME_URL"]

day_list = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
]


def _format_hour(dt):
    hour_str = dt.format("LT")

    if hour_str == "12:00 PM":
        return "12:00 noon"
    elif hour_str == "12:00 AM":
        return "12:00 midnight"
    else:
        return hour_str.lower()


def _schedule_block(status_schedule_rule):
    status_emojis = " ".join(status_schedule_rule.status_emojis)

    if status_emojis:
        status_emojis = " " + status_emojis

    tz = status_schedule_rule.timezone

    start_date = status_schedule_rule.next_datetime.in_tz(tz)
    start_hour_str = _format_hour(start_date)

    days = [day_list[d] for d in status_schedule_rule.days]

    days_str = ""

    duration_str = (
        f" for {status_schedule_rule.duration} "
        f"hour{'' if status_schedule_rule.duration == 1 else 's'}"
    )

    if len(days) == 0:
        start_date_str = start_date.format("ddd D MMM YYYY")
        start_hour_str = f"{start_date_str} at {start_hour_str}"

        end_date = status_schedule_rule.calculate_end_time().in_tz(tz)
        end_date_str = end_date.format("ddd D MMM YYYY")
        end_hour_str = _format_hour(end_date)
        duration_str = f"\nTo {end_date_str} at {end_hour_str}"
    elif len(days) == 1:
        days_str = f"\nEvery {days[0]}"
    else:
        days_str = "\nEvery {} and {}".format(", ".join(days[:-1]), days[-1])

    overwrite_str = ""

    if status_schedule_rule.overwrite:
        overwrite_str = "\n_This status will overwrite any existing status_"

    return {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": (
                f"*{status_schedule_rule.status_text}*{status_emojis}"
                f"\n{start_hour_str} ({tz}){duration_str}"
                f"{days_str}{overwrite_str}"
            ),
        },
    }


def _schedule_actions_block(schedule_id, schedule):
    return {
        "type": "actions",
        "elements": [
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": "Delete schedule",
                },
                "style": "danger",
                "value": "delete_schedule",
            },
        ],
        "block_id": schedule_id,
    }


def _home_tab_modal(**schedules):
    remaining_schedules = MAX_SCHEDULES_PER_USER - len(schedules)

    if remaining_schedules < 1:
        instructions = (
            "You have reached the maximum number of scheduled statuses "
            f"({MAX_SCHEDULES_PER_USER}) and will have to delete one or more "
            "before creating another one."
        )
    else:
        status_str = "status" if remaining_schedules == 1 else "statuses"
        instructions = (
            f"You can add up to {remaining_schedules} more scheduled "
            f"{status_str}. Create a new one-off scheduled status using the "
            "`/schedule-status` command or a daily scheduled status using "
            "the `/daily-status` command."
        )

    instructions = (
        f"{instructions}\n\nNote that one-off schedules due to trigger within "
        "the next 15 minutes may not appear in this list."
    )

    modal = {
        "type": "home",
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": instructions,
                },
            },
        ],
    }

    if schedules:
        modal["blocks"].append(
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": "Your current schedules",
                },
            },
        )

    for schedule_id, schedule in schedules.items():
        modal["blocks"].append({"type": "divider"})
        modal["blocks"].append(_schedule_block(schedule))
        modal["blocks"].append(_schedule_actions_block(schedule_id, schedule))

    return modal


def _not_authorized_home_tab_modal():
    instructions = (
        "For information about the app and how to use it, see the "
        f"<{HOME_URL}|home page>.\n\n"
        f"To get started with the app, please <{AUTH_URL}|authorize> it."
    )

    modal = {
        "type": "home",
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": instructions,
                },
            },
        ],
    }

    return modal


def update_home_tab(*, user_id, team_id):
    """Create or update user's home tab in Slack."""
    bot_token = get_user_info(team_id, user_id, BotInfo.BOT_TOKEN)

    if bot_token is None:
        raise Exception("Unable to retreive Slack bot token")

    try:
        get_user_info(team_id, user_id, UserInfo.USER_TOKEN)
    except RecordNotFoundError:
        view = _not_authorized_home_tab_modal()
    else:
        schedules = get_user_schedules(team_id=team_id, user_id=user_id)

        view = _home_tab_modal(**schedules)

    client = WebClient(token=bot_token)

    return client.views_publish(user_id=user_id, view=view)
