"""Mixin for parsing API Gateway Slack payloads."""

import hashlib
import hmac
from urllib.parse import parse_qsl

import pendulum

from .exceptions import HmacVerificationError


class SlackParserMixin:
    """Provides methods for parsing and verifying Slack payloads."""

    SLACK_SIGNING_SECRET = None

    def verify_signature(self, body, headers):
        """Verify Slack signature."""
        signature = headers["x-slack-signature"]
        timestamp = headers["x-slack-request-timestamp"]

        expected_signature = (
            "v0="
            + hmac.new(
                self.SLACK_SIGNING_SECRET.encode("utf-8"),
                f"v0:{timestamp}:{body}".encode("utf-8"),
                hashlib.sha256,
            ).hexdigest()
        )

        if not hmac.compare_digest(expected_signature, signature):
            raise HmacVerificationError("Invalid HMAC signature")

        if abs(pendulum.now().int_timestamp - int(timestamp)) > 60:
            raise HmacVerificationError(
                "Rejecting HMAC signature because timestamp is more than 60 "
                "seconds in the past or in the future"
            )

    def parse_form_encoded(self, body):
        """Parse www-form-encoded body."""
        return dict(parse_qsl(body))
