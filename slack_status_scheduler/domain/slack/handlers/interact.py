"""API Gateway handler for Slack interact payloads."""

import json

from infrastructure.handlers.api_gateway import (
    ApiGatewayV2Handler,
    HttpBadRequestError,
    HttpForbiddenError,
)

from .exceptions import HmacVerificationError
from .mixin import SlackParserMixin


class InteractHandler(ApiGatewayV2Handler, SlackParserMixin):
    """Handle Slack interact payloads."""

    LOG_REDACTION_KEYS = {
        "x-slack-signature",
        "domain",
        "username",
        "name",
        "token",
    }

    INTERACTION_TYPES = {
        "shortcut",
        "message_action",
        "block_actions",
        "interactive_message",
        "view_submission",
        "view_closed",
    }

    def parse_body(self):
        """Parse event body and verify HMAC signature."""
        body = super().parse_body()

        try:
            self.verify_signature(body, self.headers)
        except HmacVerificationError as e:
            raise HttpForbiddenError("HMAC verification error") from e

        body = self.parse_form_encoded(body)

        return json.loads(body["payload"])

    def execute(self):
        """Dispatch interaction events to appropriate method."""
        user_id = self.body.get("user", {}).get("id")
        team_id = self.body.get("team", {}).get("id")

        interaction_type = self.body.get("type")

        if interaction_type not in self.INTERACTION_TYPES:
            self.logger.warning(
                "Unknown interaction type",
                {
                    "interaction_type": interaction_type,
                    "payload": self.body,
                },
            )

            raise HttpBadRequestError(
                f"Unexpected interaction type '{interaction_type}'"
            )

        method = getattr(self, interaction_type)
        body = method(team_id=team_id, user_id=user_id)

        if body is not None:
            return self.create_response(
                body=body, headers={"Content-Type": "application/json"}
            )

        return self.create_response()

    def _warn_unhandled(self, interaction_type):
        self.logger.warning(
            "Unhandled interaction type",
            {
                "interaction_type": interaction_type,
                "payload": self.body,
            },
        )

    def shortcut(self, *, team_id, user_id):
        """Handle shortcut interaction."""
        self._warn_unhandled("shortcut")

    def message_action(self, *, team_id, user_id):
        """Handle message_action interaction."""
        self._warn_unhandled("message_action")

    def block_actions(self, *, team_id, user_id):
        """Handle block_actions interaction."""
        self._warn_unhandled("block_actions")

    def interactive_message(self, *, team_id, user_id):
        """Handle interactive_message interaction."""
        self._warn_unhandled("interactive_message")

    def view_submission(self, *, team_id, user_id):
        """Handle view_submission interaction."""
        self._warn_unhandled("view_submission")

    def view_closed(self, *, team_id, user_id):
        """Handle view_closed interaction."""
        self._warn_unhandled("view_closed")
