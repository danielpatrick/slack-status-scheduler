"""API Gateway handler for Slack command payloads."""

from infrastructure.handlers.api_gateway import (
    ApiGatewayV2Handler,
    HttpBadRequestError,
    HttpForbiddenError,
)

from .exceptions import HmacVerificationError
from .mixin import SlackParserMixin


class SlashCommandHandler(ApiGatewayV2Handler, SlackParserMixin):
    """Handle Slack slash command payloads."""

    LOG_REDACTION_KEYS = {
        "x-slack-signature",
        "team_domain",
        "user_name",
        "token",
    }

    COMMAND_NAME = None

    def parse_body(self):
        """Parse event body and verify HMAC signature."""
        body = super().parse_body()

        try:
            self.verify_signature(body, self.headers)
        except HmacVerificationError as e:
            raise HttpForbiddenError("HMAC verification error") from e

        return self.parse_form_encoded(body)

    def check_command(self):
        """Confirm expected command."""
        command = self.body["command"]

        if command != self.COMMAND_NAME:
            raise HttpBadRequestError(f"Unexpected slash command '{command}'")

    def parse_event(self):
        """Perform additional check to confirm expected command."""
        super().parse_event()
        self.check_command()
