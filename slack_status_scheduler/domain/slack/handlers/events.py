"""API Gateway handler for Slack Events API events."""

import json

from infrastructure.handlers.api_gateway import (
    ApiGatewayV2Handler,
    HttpBadRequestError,
    HttpForbiddenError,
)

from .events_api import EVENT_TYPES
from .exceptions import HmacVerificationError
from .mixin import SlackParserMixin


class EventsHandler(ApiGatewayV2Handler, SlackParserMixin):
    """Handle Slack interact payloads."""

    LOG_REDACTION_KEYS = {
        "x-slack-signature",
        "token",
        "challenge",
    }

    def parse_body(self):
        """Parse event body and verify HMAC signature."""
        body = super().parse_body()

        try:
            self.verify_signature(body, self.headers)
        except HmacVerificationError as e:
            raise HttpForbiddenError("HMAC verification error") from e

        return json.loads(body)

    def execute(self):
        """Dispatch interaction events to appropriate method."""
        body_type = self.body.get("type")

        if body_type == "url_verification":
            response_body = self._url_verification(self.body)
        elif body_type == "event_callback":
            response_body = self._event_callback(self.body)
        else:
            self.logger.warning(
                "Unexpected body type",
                {
                    "body_type": body_type,
                    "payload": self.body,
                },
            )

            raise HttpBadRequestError(f"Unexpected body type '{body_type}'")

        if response_body is None:
            return self.create_response()

        return self.create_response(
            body=response_body,
            headers={"Content-Type": "application/json"},
        )

    def _event_callback(self, body):
        self.event_type = body["event"]["type"]

        if self.event_type not in EVENT_TYPES:
            self._bad_request_unknown_event(self.event_type)

        method_name = self.event_type.replace(".", "__")

        method = getattr(self, method_name, None)

        if method is None:
            self._warn_unhandled(self.event_type)
        else:
            return method(self.body)

    def _url_verification(self, body):
        """Handle Slack Events API url_verification."""
        challenge = body["challenge"]

        return json.dumps({"challenge": challenge})

    def _bad_request_unknown_event(self, event_type):
        self.logger.warning(
            "Unknown event type",
            {
                "event_type": event_type,
                "payload": self.body,
            },
        )

        raise HttpBadRequestError(f"Unexpected event type '{event_type}'")

    def _warn_unhandled(self, event_type):
        self.logger.warning(
            "Unhandled event type",
            {
                "event_type": event_type,
                "payload": self.body,
            },
        )
