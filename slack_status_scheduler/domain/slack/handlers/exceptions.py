"""API Gateway handler exceptions for Slack payloads."""


class HmacVerificationError(Exception):
    """Exception raised if unable to verify Slack payload using HMAC."""

    pass
