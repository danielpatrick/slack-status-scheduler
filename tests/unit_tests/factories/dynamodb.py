import os
import time
from uuid import uuid4


def schedule_dynamodb_image(**kwargs):
    team_id = kwargs.get("team_id", "team-id")
    user_id = kwargs.get("user_id", "user-id")
    schedule_id = kwargs.get("schedule_id", str(uuid4()))

    overwrite = kwargs.get("overwrite", True)
    created_by = kwargs.get("created_by", "USER")

    next_time = kwargs.get("next_time", "2021-05-16T12")

    status_text = kwargs.get("status_text", "Out for lunch")
    status_emojis = kwargs.get(
        "status_emojis", [":knife_fork_plate:", ":sandwich:"]
    )
    timezone = kwargs.get("timezone", "Europe/Paris")
    hour = str(kwargs.get("hour", 13))
    duration = str(kwargs.get("duration", 1))
    days = kwargs.get("days", [0, 1, 2, 3, 4, 5, 6])

    image = {
        "TeamId": {"S": team_id},
        "UserIdScheduleId": {"S": f"{user_id}#{schedule_id}"},
        "CreatedBy": {"S": created_by},
        "Overwrite": {"BOOL": overwrite},
        "NextEventBeginHour": {"S": next_time},
        "UserId": {"S": user_id},
        "ScheduleId": {"S": schedule_id},
        "StatusText": {"S": status_text},
        "StatusEmojis": {"SS": status_emojis},
        "Timezone": {"S": timezone},
        "Hour": {"N": hour},
        "Duration": {"N": duration},
    }

    if days is not None:
        image["Days"] = {"NS": [str(d) for d in days]}

    return image


def dynamodb_stream_record(
    keys,
    event_name,
    stream_view_type,
    old_image,
    new_image,
    region=None,
):
    if region is None:
        region = os.environ["AWS_DEFAULT_REGION"]

    record = {
        "eventID": str(uuid4()),
        "eventName": event_name,
        "eventVersion": "1.1",
        "eventSource": "aws:dynamodb",
        "awsRegion": region,
        "dynamodb": {
            "ApproximateCreationDateTime": str(int(time.time())),
            "Keys": keys,
            "SequenceNumber": "123",
            "SizeBytes": 123,
            "StreamViewType": stream_view_type,
        },
        "eventSourceArn": "stream-arn",
    }

    if event_name in ("REMOVE", "MODIFY") and stream_view_type in (
        "OLD_IMAGE",
        "NEW_AND_OLD_IMAGES",
    ):
        record["dynamodb"]["OldImage"] = old_image
    if event_name in ("INSERT", "MODIFY") and stream_view_type in (
        "NEW_AND_OLD_IMAGES",
        "NEW_IMAGE",
    ):
        record["dynamodb"]["NewImage"] = new_image

    return record


def schedule_stream_record(**kwargs):
    event_name = kwargs.pop("event_name", "MODIFY")
    stream_view_type = kwargs.pop("stream_view_type", "NEW_AND_OLD_IMAGES")

    if "old_next_time" in kwargs:
        next_time = kwargs.pop("old_next_time")
        old_image = schedule_dynamodb_image(next_time=next_time, **kwargs)
    else:
        old_image = schedule_dynamodb_image(**kwargs)

    if "new_next_time" in kwargs:
        next_time = kwargs.pop("new_next_time")
        new_image = schedule_dynamodb_image(next_time=next_time, **kwargs)
    else:
        new_image = schedule_dynamodb_image(**kwargs)

    keys = {
        "TeamId": new_image["TeamId"],
        "UserIdScheduleId": new_image["UserIdScheduleId"],
    }

    return dynamodb_stream_record(
        keys=keys,
        event_name=event_name,
        stream_view_type=stream_view_type,
        old_image=old_image,
        new_image=new_image,
    )


def user_dynamodb_image(**kwargs):
    team_id = kwargs.get("team_id", "team-id")
    user_id = kwargs.get("user_id", "user-id")
    user_scope = kwargs.get("user_scope", "user-scope")
    user_token = kwargs.get("user_token", "user-token")
    team_name = kwargs.get("team_name", "team-name")
    bot_scope = kwargs.get("bot_scope", "bot-scope")
    bot_token = kwargs.get("bot_token", "bot-token")
    schedule_count = kwargs.get("schedule_count", 0)

    return {
        "TeamId": {"S": team_id},
        "EntityId": {"S": user_id},
        "UserScope": {"S": user_scope},
        "UserToken": {"S": user_token},
        "TeamName": {"S": team_name},
        "BotScope": {"S": bot_scope},
        "BotToken": {"S": bot_token},
        "ScheduleCount": {"N": str(schedule_count)},
    }


def team_dynamodb_image(**kwargs):
    team_id = kwargs.get("team_id", "team-id")
    team_name = kwargs.get("team_name", "team-name")
    bot_scope = kwargs.get("bot_scope", "bot-scope")
    bot_token = kwargs.get("bot_token", "bot-token")

    return {
        "TeamId": {"S": team_id},
        "EntityId": {"S": team_id},
        "TeamName": {"S": team_name},
        "BotScope": {"S": bot_scope},
        "BotToken": {"S": bot_token},
    }


def team_user_stream_record(entity="USER", **kwargs):
    event_name = kwargs.pop("event_name", "INSERT")
    stream_view_type = kwargs.pop("stream_view_type", "NEW_AND_OLD_IMAGES")

    if entity == "USER":
        old_image = user_dynamodb_image(**kwargs)
        new_image = user_dynamodb_image(**kwargs)
    else:
        old_image = team_dynamodb_image(**kwargs)
        new_image = team_dynamodb_image(**kwargs)

    keys = {
        "TeamId": new_image["TeamId"],
        "EntityId": new_image["EntityId"],
    }

    return dynamodb_stream_record(
        keys=keys,
        event_name=event_name,
        stream_view_type=stream_view_type,
        old_image=old_image,
        new_image=new_image,
    )
