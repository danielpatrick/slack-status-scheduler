import os


def scheduled_event(dt=None):
    if dt is None:
        timestamp = "2021-05-15T10:45:00Z"
    else:
        timestamp = f"{dt.isoformat(timespec='seconds')}Z"

    return {
        "version": "0",
        "id": "9c3e36e9-75a2-a243-6c72-3b6e04b80e94",
        "detail-type": "Scheduled Event",
        "source": "aws.events",
        "account": "123456789012",
        "time": timestamp,
        "region": os.environ.get("AWS_DEFAULT_REGION"),
        "resources": [
            (
                "arn:aws:events:eu-west-1:123456789012:rule/"
                "stack-status-scheduler-add-dynamodb-JobSchedule-1XIUXVYNPAS4J"
            )
        ],
        "detail": {},
    }
