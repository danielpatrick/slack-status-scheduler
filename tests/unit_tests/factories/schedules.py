from domain.schedules import ScheduleRule, StatusScheduleRule


def rule(
    *,
    next_time=None,
    timezone=None,
    duration=None,
    days=None,
):
    return ScheduleRule(
        next_time="2021-05-16T11" if next_time is None else next_time,
        timezone="Europe/Paris" if timezone is None else timezone,
        duration=1 if duration is None else duration,
        days=[0, 2, 4, 6] if days is None else days,
    )


def status_rule(
    *,
    next_time=None,
    timezone=None,
    duration=None,
    days=None,
    status_text=None,
    status_emojis=None,
    overwrite=True,
):
    if status_emojis is None:
        status_emojis = [":knife_fork_plate:", ":sandwich:"]

    return StatusScheduleRule(
        next_time="2021-05-16T11" if next_time is None else next_time,
        timezone="Europe/Paris" if timezone is None else timezone,
        duration=1 if duration is None else duration,
        days=[0, 2, 4, 6] if days is None else days,
        status_text="Out for lunch" if status_text is None else status_text,
        status_emojis=status_emojis,
        overwrite=overwrite,
    )
