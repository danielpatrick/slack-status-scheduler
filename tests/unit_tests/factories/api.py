from base64 import b64encode


def api_event(method, path, params=None, **kwargs):
    base64 = kwargs.get("base64", False)
    body = kwargs.get("body", "")

    if base64:
        if isinstance(body, str):
            body = body.encode("utf-8")

        body = b64encode(body).decode("utf-8")

    event = {
        "headers": kwargs.get("headers", {}),
        "multiValueHeaders": kwargs.get("multi_value_headers", {}),
        "requestContext": {
            "domainName": "www.example.com",
            "http": {
                "method": method,
                "path": path,
            },
        },
        "body": body,
        "isBase64Encoded": base64,
    }

    if params is not None:
        event["queryStringParameters"] = params

    return event
