import hashlib
import hmac
import json
import os
from urllib.parse import urlencode

import pendulum

from .api import api_event

VALID_SECRET = os.environ["SLACK_VALID_SIGNING_SECRET"]
INVALID_SECRET = os.environ["SLACK_INVALID_SIGNING_SECRET"]


def slack_success_response(**kwargs):
    return {
        "ok": True,
        "access_token": kwargs.get("bot_access_token", "xoxb-0123"),
        "token_type": "bot",
        "scope": kwargs.get("bot_scope", "commands,incoming-webhook"),
        "bot_user_id": kwargs.get("bot_user_id", "U0KRQLJ9H"),
        "app_id": kwargs.get("app_id", "A0KRD7HC3"),
        "team": {
            "name": kwargs.get("team_name", "Slack Softball Team"),
            "id": kwargs.get("team_id", "T9TK3CUKW"),
        },
        "enterprise": {
            "name": "slack-sports",
            "id": "E12345678",
        },
        "authed_user": {
            "id": kwargs.get("user_id", "U1234"),
            "scope": kwargs.get("user_scope", "chat:write"),
            "access_token": kwargs.get("user_access_token", "xoxp-1234"),
            "token_type": "user",
        },
    }


def slack_api_event(body, path, **kwargs):
    base64 = kwargs.pop("base64", True)
    timestamp = str(
        kwargs.pop("slack_timestamp", pendulum.now().int_timestamp)
    )
    secret = kwargs.pop("slack_secret", VALID_SECRET)

    signature = (
        "v0="
        + hmac.new(
            secret.encode("utf-8"),
            f"v0:{timestamp}:{body}".encode("utf-8"),
            hashlib.sha256,
        ).hexdigest()
    )

    headers = {
        "x-slack-request-timestamp": timestamp,
        "x-slack-signature": signature,
    }

    return api_event(
        "POST", path, body=body, headers=headers, base64=base64, **kwargs
    )


def common_event(body, url, valid_signature, valid_timestamp, encode=True):
    if encode:
        body = urlencode(list(body.items()))

    secret = VALID_SECRET if valid_signature else INVALID_SECRET
    dt = pendulum.now()

    if not valid_timestamp:
        dt = dt.subtract(minutes=2)

    return slack_api_event(
        body,
        url,
        slack_secret=secret,
        slack_timestamp=str(dt.int_timestamp),
    )


def slash_event(payload, valid_signature=True, valid_timestamp=True):
    return common_event(
        payload,
        "/command/schedule-status",
        valid_signature,
        valid_timestamp,
    )


def interact_event(payload, valid_signature=True, valid_timestamp=True):
    return common_event(
        {"payload": json.dumps(payload, separators=(",", ":"))},
        "/interact",
        valid_signature,
        valid_timestamp,
    )


def events_api_event(payload, valid_signature=True, valid_timestamp=True):
    return common_event(
        json.dumps(payload, separators=(",", ":")),
        "/events",
        valid_signature,
        valid_timestamp,
        False,
    )


def static_select(name, text, value):
    return {
        name: {
            "type": "static_select",
            "selected_option": {
                "text": {"text": {"type": "plain_text", "text": text}},
                "value": value,
            },
        }
    }


def slack_schedule_view_submission_payload(**kwargs):
    def text_input(name, text):
        return {name: {"type": "plain_text_input", "value": text}}

    status_text = kwargs.get("status_text", "Brunch")
    start_time = kwargs.get("start_time", 10)
    emojis = kwargs.get("status_emojis")
    user_id = kwargs.get("user_id", "U1234")
    team_id = kwargs.get("team_id", "T1234")

    private_metadata = {
        "timezone": kwargs.get("timezone", "Europe/London"),
    }

    if emojis is not None:
        private_metadata["emojis"] = emojis

    submitted_view = {
        "type": "view_submission",
        "user": {"id": user_id, "team_id": team_id, "username": "user1"},
        "team": {"id": team_id, "domain": "some-team-name"},
        "view": {
            "id": "V1234",
            "type": "modal",
            "blocks": [],
            "private_metadata": json.dumps(private_metadata),
            "callback_id": kwargs.get("callback_id", "daily-status-submit"),
            "state": {
                "values": {
                    "status-text": text_input("input-text", status_text),
                    "start-time": static_select(
                        "select-start-time",
                        f"{start_time:02}:00",
                        f"{start_time:02}",
                    ),
                    "overwrite": {
                        "toggle-overwrite": {
                            "type": "checkboxes",
                            "selected_options": [],
                        }
                    },
                }
            },
        },
    }

    if kwargs.get("overwrite", True):
        submitted_view["view"]["state"]["values"]["overwrite"][
            "toggle-overwrite"
        ]["selected_options"].append(
            {
                "text": {
                    "type": "plain_text",
                    "text": "Overwrite existing status",
                },
                "value": "overwrite",
            }
        )

    return submitted_view


def slack_daily_schedule_view_submission_payload(**kwargs):
    duration = kwargs.get("duration", 1)

    days_of_week = {
        0: "Monday",
        1: "Tuesday",
        2: "Wednesday",
        3: "Thursday",
        4: "Friday",
        5: "Saturday",
        6: "Sunday",
    }

    def day_option(n):
        return {
            "text": {
                "type": "plain_text",
                "text": days_of_week[n],
            },
            "value": str(n),
        }

    def days_select(*days):
        return {
            "select-days": {
                "type": "checkboxes",
                "selected_options": [day_option(n) for n in days],
            }
        }

    days = kwargs.pop("days", [5, 6])

    if "callback_id" not in kwargs:
        kwargs["callback_id"] = "daily-status-submit"

    view = slack_schedule_view_submission_payload(**kwargs)

    view["view"]["state"]["values"]["duration"] = static_select(
        "select-duration", f"{duration} hours", str(duration)
    )
    view["view"]["state"]["values"]["days-of-week"] = days_select(*days)

    return view


def slack_one_off_schedule_view_submission_payload(**kwargs):
    start_date = kwargs.pop("start_date", "2021-07-30")
    end_date = kwargs.pop("end_date", "2021-07-31")
    end_time = kwargs.pop("end_time", 10)

    if "callback_id" not in kwargs:
        kwargs["callback_id"] = "schedule-status-submit"

    view = slack_schedule_view_submission_payload(**kwargs)

    view["view"]["state"]["values"]["start-date"] = {
        "select-start-date": {
            "type": "datepicker",
            "selected_date": start_date,
        }
    }

    view["view"]["state"]["values"]["end-date"] = {
        "select-end-date": {
            "type": "datepicker",
            "selected_date": end_date,
        }
    }

    view["view"]["state"]["values"]["end-time"] = static_select(
        "select-end-time",
        f"{end_time:02}:00",
        f"{end_time:02}",
    )

    return view
