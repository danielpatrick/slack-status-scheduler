import json
from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber

from application.schedule_table_stream import handler
from domain.config import ONE_OFF_SCHEDULE_NEXT_TIME
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def sns_stub():
    with Stubber(handler.__self__.sns_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


def test_handler_sends_insert_records_to_correct_topic(sns_stub, logger):
    record = schedule_stream_record(event_name="INSERT")

    expected_params = {
        "TopicArn": "test-schedule-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "INSERT", "DataType": "String"},
            "nextTime": {"StringValue": "2021-05-16T12", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "123"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_modify_records_to_correct_topic(sns_stub, logger):
    record = schedule_stream_record(event_name="MODIFY")

    expected_params = {
        "TopicArn": "test-schedule-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "MODIFY", "DataType": "String"},
            "nextTime": {"StringValue": "2021-05-16T12", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "234"},
        expected_params=expected_params,
    )
    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_remove_records_to_correct_topic(sns_stub, logger):
    record = schedule_stream_record(event_name="REMOVE")

    expected_params = {
        "TopicArn": "test-schedule-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "REMOVE", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "345"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sets_next_time_to_no_next_time(sns_stub, logger):
    record = schedule_stream_record(
        event_name="MODIFY", next_time=ONE_OFF_SCHEDULE_NEXT_TIME
    )

    expected_params = {
        "TopicArn": "test-schedule-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "MODIFY", "DataType": "String"},
            "nextTime": {"StringValue": "NO_NEXT_TIME", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "345"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)
