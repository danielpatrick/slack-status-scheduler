import json
from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from application.user_enrich import handler
from domain.db.users import UserInfo
from infrastructure.logger import Logger


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture
def event():
    image = {
        "Keys": {
            "TeamId": {"S": "T1234"},
            "EntityId": {"S": "U1234"},
        }
    }
    yield {"Records": [{"body": json.dumps(image)}]}


@pytest.fixture(autouse=True)
def get_user_info():
    with patch("application.user_enrich.get_user_info", autospec=True) as mock:
        mock.return_value = "user-token"
        yield mock


@pytest.fixture(autouse=True)
def WebClient():
    with patch("application.user_enrich.WebClient", autospec=True) as mock:
        yield mock


@pytest.fixture
def web_client(WebClient):
    yield WebClient.return_value


@pytest.fixture(autouse=True)
def set_user_timezone():
    with patch(
        "application.user_enrich.set_user_timezone", autospec=True
    ) as mock_set_user_tz:
        yield mock_set_user_tz


def test_handler_updates_database_with_timezone_from_slack(
    event, WebClient, web_client, set_user_timezone, get_user_info
):
    web_client.users_info.return_value = {"user": {"tz": "Continent/City"}}

    assert handler(event, None) is None

    get_user_info.assert_called_once_with(
        "T1234", "U1234", UserInfo.USER_TOKEN
    )
    WebClient.assert_called_once_with(token="user-token")
    web_client.users_info.assert_called_once_with(user="U1234")

    set_user_timezone.assert_called_once_with(
        user_id="U1234", team_id="T1234", timezone="Continent/City"
    )


def test_handler_does_not_swallow_slack_api_errors(
    event, WebClient, web_client, set_user_timezone, get_user_info
):
    web_client.users_info.side_effect = SlackApiError(
        "Something went wrong", {"ok": False}
    )

    with pytest.raises(SlackApiError, match="Something went wrong"):
        handler(event, None)

    set_user_timezone.assert_not_called()


def test_handler_errors_if_unable_to_fetch_user_token(
    event, WebClient, web_client, set_user_timezone, get_user_info
):
    get_user_info.return_value = None

    with pytest.raises(Exception, match="Unable to retreive Slack user token"):
        handler(event, None)

    WebClient.assert_not_called()
    web_client.assert_not_called()
    set_user_timezone.assert_not_called()


def test_handler_exits_early_if_no_records(
    event, WebClient, web_client, set_user_timezone, get_user_info, logger
):
    assert handler({"Records": []}, None) is None

    get_user_info.assert_not_called()
    WebClient.assert_not_called()
    web_client.assert_not_called()
    set_user_timezone.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")
