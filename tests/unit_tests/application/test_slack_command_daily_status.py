from unittest.mock import patch

import pytest

from domain.exceptions import MaxSchedulesError
from domain.slack.exceptions import NotAuthenticatedError
from unit_tests.factories import slash_event


def schedule_event(
    *,
    command="/daily-status",
    emojis=None,
    valid_signature=True,
    valid_timestamp=True
):
    payload = {
        "token": "TOKEN",
        "team_id": "T1234",
        "team_domain": "slash-test",
        "channel_id": "D1234",
        "channel_name": "directmessage",
        "user_id": "U1234",
        "user_name": "USERNAME",
        "command": command,
        "api_app_id": "A1234",
        "is_enterprise_install": "false",
        "response_url": "RESPONSE-URL",
        "trigger_id": "TRIGGER-ID",
    }

    if emojis is not None:
        payload["text"] = emojis

    return slash_event(
        payload,
        valid_signature=valid_signature,
        valid_timestamp=valid_timestamp,
    )


@pytest.fixture
def handler(secretsmanager_stub):
    import application.slack_command_daily_status

    yield application.slack_command_daily_status.handler


@pytest.fixture(autouse=True)
def send_form():
    with patch(
        "application.slack_command_daily_status.send_form",
        autospec=True,
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def daily_status_form():
    with patch(
        "application.slack_command_daily_status.daily_status_form",
        autospec=True,
    ) as mock:
        yield mock


def test_handler_returns_403_if_auth_fails(handler):
    event = schedule_event(valid_signature=False)

    assert handler(event, None) == {"statusCode": 403, "body": "Forbidden"}


def test_handler_does_not_send_form_if_auth_fails(handler, send_form):
    event = schedule_event(valid_signature=False)

    handler(event, None)

    assert send_form.call_count == 0


def test_handler_returns_500_if_unexpected_command(handler):
    event = schedule_event(command="/reminder")

    expected_response = {"statusCode": 400, "body": "Bad request"}

    assert handler(event, None) == expected_response


def test_handler_return_value(handler):
    assert handler(schedule_event(), None) == {"statusCode": 200}


def test_handler_calls_send_daily_status_form(
    handler, send_form, daily_status_form
):
    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [],
    }

    handler(schedule_event(), None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_sends_emojis_if_provided(
    handler, send_form, daily_status_form
):
    event = schedule_event(emojis=":pizza::bagel:")

    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [":pizza:", ":bagel:"],
    }

    handler(event, None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_matches_emojis_with_non_alpha_chars(
    handler, send_form, daily_status_form
):
    event = schedule_event(emojis=":dog2::black_cat::t-rex:")

    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [":dog2:", ":black_cat:", ":t-rex:"],
    }

    handler(event, None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_only_sends_valid_emojis(
    handler, send_form, daily_status_form
):
    event = schedule_event(emojis=":not-a-real-emoji::bagel:")

    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [":bagel:"],
    }

    handler(event, None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_ignores_skin_tone_modifiers(
    handler, send_form, daily_status_form
):
    event = schedule_event(emojis=":+1::skin-tone-2::bagel:")

    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [":+1:", ":bagel:"],
    }

    handler(event, None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_sends_max_ten_emojis(handler, send_form, daily_status_form):
    event = schedule_event(
        emojis=(
            ":pizza::bagel::fries::hamburger::burrito:"
            ":hotdog::spaghetti::sandwich::taco::curry::sushi:"
        )
    )

    expected_kwargs = {
        "user_id": "U1234",
        "team_id": "T1234",
        "trigger_id": "TRIGGER-ID",
        "status_emojis": [
            ":pizza:",
            ":bagel:",
            ":fries:",
            ":hamburger:",
            ":burrito:",
            ":hotdog:",
            ":spaghetti:",
            ":sandwich:",
            ":taco:",
            ":curry:",
        ],
    }

    handler(event, None)

    send_form.assert_called_once_with(daily_status_form, **expected_kwargs)


def test_handler_returns_message_if_user_has_max_schedules(handler, send_form):
    send_form.side_effect = MaxSchedulesError()

    expected_response = {
        "statusCode": 200,
        "body": (
            '{"blocks": [{'
            '"type": "section", "text": {"type": "mrkdwn", "text": "'
            "Sorry, you can only create up to 5 status schedules.\\n\\n"
            "Please delete one or more schedules to create another."
            '"}}]}'
        ),
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(schedule_event(), None) == expected_response


def test_handler_returns_message_if_user_not_authorized(handler, send_form):
    send_form.side_effect = NotAuthenticatedError()

    expected_response = {
        "statusCode": 200,
        "body": (
            '{"blocks": [{'
            '"type": "section", "text": {"type": "mrkdwn", "text": "'
            "You must <https://www.example.com/install/|authorize the app> "
            "before you can perform this action."
            '"}}]}'
        ),
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(schedule_event(), None) == expected_response
