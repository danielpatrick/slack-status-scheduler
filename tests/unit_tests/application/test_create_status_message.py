import json
from unittest.mock import MagicMock, Mock, call, patch

import pendulum
import pytest
from botocore.stub import Stubber

from application.create_status_message import handler
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def sqs_stub():
    with Stubber(handler.__self__.sqs_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


@pytest.fixture
def random_choice():
    with patch(
        "application.create_status_message.random.choice", autospec=True
    ) as choice:
        yield choice


@pytest.fixture(autouse=True)
def mock_pendulum():
    with patch(
        "application.create_status_message.pendulum", Mock(wraps=pendulum)
    ) as mock:
        yield mock


def message_attributes(event_name):
    return {"eventName": {"stringValue": event_name, "dataType": "String"}}


def sqs_event(event_name, **kwargs):
    user_id = kwargs.pop("user_id", "U1234")
    team_id = kwargs.pop("team_id", "T1234")

    stream_record = schedule_stream_record(
        event_name=event_name,
        user_id=user_id,
        team_id=team_id,
        **kwargs,
    )
    body = stream_record["dynamodb"]

    if "OldImage" in body:
        body["OldImage"]["StatusText"]["S"] = "An old status"
    if "NewImage" in body:
        body["NewImage"]["StatusText"]["S"] = "A new status"

    record = {
        "body": json.dumps(body),
        "messageAttributes": {
            "eventName": {"stringValue": event_name, "dataType": "String"}
        },
    }
    return {"Records": [record]}


def test_handler_exits_early_if_no_records(logger):
    event = {"Records": []}

    assert handler(event, None) is None

    logger.info.assert_any_call("No record in event; nothing to do")


def test_handler_exits_early_if_unexpected_event_type(logger):
    event = sqs_event("MADE_UP_EVENT_TYPE")

    assert handler(event, None) is None

    logger.info.assert_any_call("Ignoring event type MADE_UP_EVENT_TYPE")


def test_handler_uses_old_image_for_modify(sqs_stub, mock_pendulum, logger):
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[":pizza:"],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": ":pizza:", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_handler_uses_new_image_for_insert(sqs_stub, mock_pendulum, logger):
    event = sqs_event(
        "INSERT",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[":pizza:"],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "A new status", '
        '"status_emoji": ":pizza:", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_handler_uses_random_emoji(
    random_choice, sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)
    random_choice.return_value = ":sandwich:"
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[":pizza:", ":burrito:"],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": ":sandwich:", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    random_choice.assert_called_once_with([":pizza:", ":burrito:"])

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_handler_uses_empty_string_for_emoji_if_none(
    sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": "", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_handler_uses_overwrite_false(sqs_stub, mock_pendulum, logger):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[":pizza:"],
        overwrite=False,
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": ":pizza:", '
        '"overwrite": false, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_uses_correct_times_when_bst_ends(sqs_stub, mock_pendulum, logger):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 10, 30, 16)
    event = sqs_event(
        "MODIFY",
        hour=17,
        timezone="Europe/London",
        duration=16,
        next_time="2021-10-30T16",
        status_emojis=[],
    )
    START_TIME = 1635609600  # 2021-10-30T16
    END_TIME = 1635670800  # 2021-10-31T09
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": "", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_send_status_update_to_sqs_uses_correct_delay(
    sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 10, 55, 30)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": "", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
        "DelaySeconds": 270,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    assert mock_pendulum.now.call_count == 2

    mock_pendulum.now.assert_has_calls([call(tz="UTC"), call(tz="UTC")])

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_send_satus_update_to_sqs_uses_no_delay_if_start_time_passed(
    sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[],
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": "", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    assert mock_pendulum.now.call_count == 2

    mock_pendulum.now.assert_has_calls([call(tz="UTC"), call(tz="UTC")])

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_handler_handles_empty_days_list(sqs_stub, mock_pendulum, logger):
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[":pizza:"],
        days=None,
    )
    START_TIME = 1621162800  # 2021-05-16T11
    END_TIME = 1621166400  # 2021-05-16T12
    message_body = (
        "{"
        f'"start_time": {START_TIME}, '
        f'"end_time": {END_TIME}, '
        '"status_text": "An old status", '
        '"status_emoji": ":pizza:", '
        '"overwrite": true, '
        '"user_id": "U1234", '
        '"team_id": "T1234"'
        "}"
    )
    expected_params = {
        "QueueUrl": "test-slack-queue",
        "MessageBody": message_body,
    }
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 11)

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending SQS message", expected_params)


def test_send_status_update_to_sqs_rejects_if_start_time_too_distant(
    sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 10)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[],
    )

    expected_log = (
        "It's too early to send this status message",
        {"start_time": "2021-05-16T11:00:00+00:00"},
    )

    assert handler(event, None) is None

    assert mock_pendulum.now.call_count == 2

    mock_pendulum.now.assert_has_calls([call(tz="UTC"), call(tz="UTC")])

    logger.warning.assert_called_once_with(*expected_log)


def test_send_status_update_to_sqs_rejects_if_end_time_passed(
    sqs_stub, mock_pendulum, logger
):
    mock_pendulum.now.return_value = pendulum.datetime(2021, 5, 16, 12)
    event = sqs_event(
        "MODIFY",
        hour=12,
        timezone="Europe/London",
        duration=1,
        next_time="2021-05-16T11",
        status_emojis=[],
    )

    expected_log = (
        "Not setting status because end time has elapsed or is about to",
        {"end_time": "2021-05-16T12:00:00+00:00"},
    )

    assert handler(event, None) is None

    mock_pendulum.now.assert_called_once_with(tz="UTC")

    logger.warning.assert_called_once_with(*expected_log)
