from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from infrastructure.logger import Logger
from unit_tests.factories import api_event, slack_success_response


@pytest.fixture
def handler(secretsmanager_stub):
    import application.slack_auth_callback

    yield application.slack_auth_callback.handler


@pytest.fixture(autouse=True)
def logger(handler):
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def WebClient():
    with patch(
        "application.slack_auth_callback.WebClient", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def web_client(WebClient):
    client = WebClient.return_value

    client.oauth_v2_access.return_value = slack_success_response(
        app_id="APP1234",
        bot_user_id="BOT1234",
        bot_scope="commands",
        bot_access_token="BOT_ACCESS_TOKEN",
        team_id="TEAM1234",
        team_name="MyTeam",
        user_id="USER1234",
        user_scope="item:action",
        user_access_token="USER_ACCESS_TOKEN",
    )

    yield client


@pytest.fixture(autouse=True)
def add_user():
    with patch(
        "application.slack_auth_callback.add_user", autospec=True
    ) as mock:
        yield mock


def callback_event(user_agent=None):
    if user_agent is None:
        headers = {}
    else:
        headers = {"user-agent": user_agent}

    return api_event(
        "GET",
        "/auth/callback",
        {"code": "SOME_CODE"},
        headers=headers,
    )


def test_handler_redirects_on_success(handler):
    event = callback_event(
        "Mozilla/5.0 (X11; Linux i686; rv:88.0) Gecko/20100101 Firefox/88.0"
    )

    expected_response = {
        "statusCode": 302,
        "headers": {
            "Location": "https://www.example.com/install/success",
        },
    }

    assert handler(event, None) == expected_response


def test_handler_redirects_with_params_on_success(handler):
    android = (
        "Mozilla/5.0 (Linux; Android 11) AppleWebKit/537.36 (KHTML, like "
        "Gecko) Chrome/91.0.4472.77 Mobile Safari/537.36"
    )
    ios = (
        "Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/6"
        "05.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1"
    )
    osx = (
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_4) AppleWebKit/605.1.15 "
        "(KHTML, like Gecko) Version/14.1 Safari/605.1.15"
    )
    windows = (
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
        "(KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36"
    )

    expected_response = {
        "statusCode": 302,
        "headers": {
            "Location": (
                "https://www.example.com/install/success"
                "?team_id=TEAM1234&app_id=APP1234"
            ),
        },
    }

    for user_agent in [android, ios, osx, windows]:
        event = callback_event(user_agent)

        assert handler(event, None) == expected_response


def test_handler_passes_params_to_slack_api(handler, WebClient, web_client):
    expected_data = {
        "client_id": "test-slack-client-id",
        "client_secret": "test-slack-client-secret",
        "code": "SOME_CODE",
        "redirect_uri": "https://www.example.com/auth/callback",
    }

    handler(callback_event(), None)

    WebClient.assert_called_once_with()
    web_client.oauth_v2_access.assert_called_once_with(**expected_data)


def test_handler_stores_token(handler, add_user):
    expected_kwargs = {
        "user_id": "USER1234",
        "user_scope": "item:action",
        "user_token": "USER_ACCESS_TOKEN",
        "team_id": "TEAM1234",
        "team_name": "MyTeam",
        "bot_id": "BOT1234",
        "bot_scope": "commands",
        "bot_token": "BOT_ACCESS_TOKEN",
    }

    handler(callback_event(), None)

    add_user.assert_called_once_with(**expected_kwargs)


def test_handler_redirects_on_slack_failure(handler, web_client, logger):
    error = SlackApiError("Nope", {"ok": False})
    web_client.oauth_v2_access.side_effect = error
    expected_response = {
        "statusCode": 302,
        "headers": {"Location": "https://www.example.com/install/failure"},
    }

    assert handler(callback_event(), None) == expected_response

    logger.error.assert_any_call(
        "Slack API call failed with error:",
        {"response": {"ok": False}},
        exception=error,
    )


def test_handler_redirects_on_db_save_failure(
    handler, web_client, add_user, logger
):
    web_client.oauth_v2_access.return_value = slack_success_response()
    error = Exception("Nope")
    add_user.side_effect = error

    expected_response = {
        "statusCode": 302,
        "headers": {"Location": "https://www.example.com/install/failure"},
    }

    assert handler(callback_event(), None) == expected_response

    logger.error.assert_any_call(
        "Error encountered in oauth callback - redirecting to failure",
        exception=error,
    )
