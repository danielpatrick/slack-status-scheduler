from unittest.mock import MagicMock, patch

import pytest

from domain.db.schedules import (
    DatePastError,
    InvalidEndDateError,
    InvalidEndTimeError,
    TimePastError,
)
from domain.exceptions import MaxSchedulesError
from infrastructure.logger import Logger
from unit_tests.factories import (
    interact_event,
    slack_daily_schedule_view_submission_payload,
    slack_one_off_schedule_view_submission_payload,
)


def shortcut_event(
    *,
    callback_id="schedule-status-begin",
    valid_signature=True,
    valid_timestamp=True,
):
    payload = {
        "type": "shortcut",
        "token": "TOKEN",
        "action_ts": "1581106241.371594",
        "team": {"id": "T1234", "domain": "shortcuts-test"},
        "user": {"id": "U1234", "username": "user1", "team_id": "T1234"},
        "callback_id": callback_id,
        "trigger_id": "TRIGGER-ID",
    }

    return interact_event(
        payload,
        valid_signature=valid_signature,
        valid_timestamp=valid_timestamp,
    )


def daily_schedule_view_submission_event(
    *,
    emojis=None,
    valid_signature=True,
    valid_timestamp=True,
    **kwargs,
):
    days = kwargs.pop("days", [5, 6])

    payload = slack_daily_schedule_view_submission_payload(
        user_id="U1234",
        team_id="T1234",
        status_text="Brunch",
        status_emojis=emojis,
        timezone="Europe/London",
        start_time=10,
        duration=1,
        days=days,
        **kwargs,
    )

    return interact_event(
        payload,
        valid_signature=valid_signature,
        valid_timestamp=valid_timestamp,
    )


def one_off_schedule_view_submission_event(
    *,
    emojis=None,
    valid_signature=True,
    valid_timestamp=True,
    **kwargs,
):
    payload = slack_one_off_schedule_view_submission_payload(
        user_id="U1234",
        team_id="T1234",
        status_text="Week off",
        status_emojis=emojis,
        timezone="Europe/London",
        start_date="2021-08-02",
        start_time=9,
        end_date="2021-08-06",
        end_time=17,
        **kwargs,
    )

    return interact_event(
        payload,
        valid_signature=valid_signature,
        valid_timestamp=valid_timestamp,
    )


def block_actions_event(
    view_type,
    actions,
    *,
    valid_signature=True,
    valid_timestamp=True,
):
    payload = {
        "type": "block_actions",
        "user": {"id": "U1234", "team_id": "T1234", "username": "user1"},
        "team": {"id": "T1234", "domain": "some-team-name"},
        "view": {"type": view_type},
        "actions": actions,
    }

    return interact_event(
        payload,
        valid_signature=valid_signature,
        valid_timestamp=valid_timestamp,
    )


@pytest.fixture
def handler(secretsmanager_stub):
    import application.slack_interact

    yield application.slack_interact.handler


@pytest.fixture(autouse=True)
def create_daily_schedule():
    with patch(
        "application.slack_interact.create_daily_schedule", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def create_one_off_schedule():
    with patch(
        "application.slack_interact.create_one_off_schedule", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def delete_schedule():
    with patch(
        "application.slack_interact.delete_schedule", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def logger(handler):
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


def test_handler_returns_403_if_auth_fails(handler):
    event = shortcut_event(valid_signature=False)

    assert handler(event, None) == {"statusCode": 403, "body": "Forbidden"}


def test_handler_rejects_shortcuts(handler):
    event = shortcut_event()

    assert handler(event, None) == {"statusCode": 400, "body": "Bad request"}


def test_handler_rejects_invalid_view_submission_callback_id(
    handler, create_daily_schedule
):
    event = daily_schedule_view_submission_event(callback_id="made_up_id")

    assert handler(event, None) == {"statusCode": 400, "body": "Bad request"}

    create_daily_schedule.assert_not_called()


def test_handler_ignores_modal_block_actions(handler, delete_schedule, logger):
    actions = [
        {
            "action_id": "randomstring",
            "block_id": "2b6baa65-f050-4a21-a64f-513bac4f9c66",
            "text": {
                "type": "plain_text",
                "text": "Delete schedule",
            },
            "value": "delete_schedule",
            "type": "button",
            "action_ts": "1622569415.904575",
        },
    ]

    event = block_actions_event("modal", actions)

    assert handler(event, None) == {"statusCode": 200}

    delete_schedule.assert_not_called()
    logger.info.assert_any_call("Ignoring block_actions type 'modal'")


def test_handler_rejects_invalid_block_actions_view_type(
    handler, delete_schedule
):
    actions = [
        {
            "action_id": "randomstring",
            "block_id": "2b6baa65-f050-4a21-a64f-513bac4f9c66",
            "text": {
                "type": "plain_text",
                "text": "Delete schedule",
            },
            "value": "delete_schedule",
            "type": "button",
            "action_ts": "1622569415.904575",
        },
    ]

    event = block_actions_event("made_up_view_type", actions)

    assert handler(event, None) == {"statusCode": 400, "body": "Bad request"}

    delete_schedule.assert_not_called()


def test_handler_calls_create_daily_schedule(handler, create_daily_schedule):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Brunch",
        "status_emojis": [":pancakes:", ":waffle:"],
        "timezone": "Europe/London",
        "hour": 10,
        "duration": 1,
        "overwrite": True,
        "days": [5, 6],
    }

    event = daily_schedule_view_submission_event(
        emojis=[":pancakes:", ":waffle:"]
    )

    assert handler(event, None) == {"statusCode": 200}

    create_daily_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_daily_schedule_without_emojis(
    handler, create_daily_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Brunch",
        "status_emojis": [],
        "timezone": "Europe/London",
        "hour": 10,
        "duration": 1,
        "overwrite": True,
        "days": [5, 6],
    }

    event = daily_schedule_view_submission_event(emojis=None)

    assert handler(event, None) == {"statusCode": 200}

    create_daily_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_daily_schedule_with_overwrite_true(
    handler, create_daily_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Brunch",
        "status_emojis": [],
        "timezone": "Europe/London",
        "hour": 10,
        "duration": 1,
        "overwrite": True,
        "days": [5, 6],
    }

    event = daily_schedule_view_submission_event(emojis=None, overwrite=True)

    assert handler(event, None) == {"statusCode": 200}

    create_daily_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_daily_schedule_with_overwrite_false(
    handler, create_daily_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Brunch",
        "status_emojis": [],
        "timezone": "Europe/London",
        "hour": 10,
        "duration": 1,
        "overwrite": False,
        "days": [5, 6],
    }

    event = daily_schedule_view_submission_event(emojis=None, overwrite=False)

    assert handler(event, None) == {"statusCode": 200}

    create_daily_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_returns_error_if_days_is_empty(
    handler, create_daily_schedule
):
    event = daily_schedule_view_submission_event(days=[])
    body = (
        '{"response_action": "errors", "errors": '
        '{"days-of-week": "You must select at least one day."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_returns_error_msg_if_max_schedules_for_daily(
    handler, create_daily_schedule
):
    create_daily_schedule.side_effect = MaxSchedulesError()
    event = daily_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": '
        '{"status-text": "You cannot create more than 5 schedules."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_calls_create_one_off_schedule(
    handler, create_one_off_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Week off",
        "status_emojis": [":palm_tree:"],
        "timezone": "Europe/London",
        "start_date_str": "2021-08-02",
        "start_hour": 9,
        "end_date_str": "2021-08-06",
        "end_hour": 17,
        "overwrite": True,
    }

    event = one_off_schedule_view_submission_event(emojis=[":palm_tree:"])

    assert handler(event, None) == {"statusCode": 200}

    create_one_off_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_one_off_schedule_without_emojis(
    handler, create_one_off_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Week off",
        "status_emojis": [],
        "timezone": "Europe/London",
        "start_date_str": "2021-08-02",
        "start_hour": 9,
        "end_date_str": "2021-08-06",
        "end_hour": 17,
        "overwrite": True,
    }

    event = one_off_schedule_view_submission_event(emojis=None)

    assert handler(event, None) == {"statusCode": 200}

    create_one_off_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_one_off_schedule_with_overwrite_true(
    handler, create_one_off_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Week off",
        "status_emojis": [],
        "timezone": "Europe/London",
        "start_date_str": "2021-08-02",
        "start_hour": 9,
        "end_date_str": "2021-08-06",
        "end_hour": 17,
        "overwrite": True,
    }

    event = one_off_schedule_view_submission_event(emojis=None, overwrite=True)

    assert handler(event, None) == {"statusCode": 200}

    create_one_off_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_calls_create_one_off_schedule_with_overwrite_false(
    handler, create_one_off_schedule
):
    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "status_text": "Week off",
        "status_emojis": [],
        "timezone": "Europe/London",
        "start_date_str": "2021-08-02",
        "start_hour": 9,
        "end_date_str": "2021-08-06",
        "end_hour": 17,
        "overwrite": False,
    }

    event = one_off_schedule_view_submission_event(
        emojis=None, overwrite=False
    )

    assert handler(event, None) == {"statusCode": 200}

    create_one_off_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_returns_error_msg_if_max_schedules_for_one_off(
    handler, create_one_off_schedule
):
    create_one_off_schedule.side_effect = MaxSchedulesError()
    event = one_off_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": '
        '{"status-text": "You cannot create more than 5 schedules."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_returns_error_msg_if_date_past_for_one_off(
    handler, create_one_off_schedule
):
    create_one_off_schedule.side_effect = DatePastError()
    event = one_off_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": {"start-date": '
        '"Start date and time must be at least one minute in the future."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_returns_error_msg_if_time_past_for_one_off(
    handler, create_one_off_schedule
):
    create_one_off_schedule.side_effect = TimePastError()
    event = one_off_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": {"start-time": '
        '"Start date and time must be at least one minute in the future."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_returns_error_msg_if_end_date_before_start_date(
    handler, create_one_off_schedule
):
    create_one_off_schedule.side_effect = InvalidEndDateError()
    event = one_off_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": {"end-date": '
        '"End date cannot be before start date."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_returns_error_msg_if_end_time_too_early(
    handler, create_one_off_schedule
):
    create_one_off_schedule.side_effect = InvalidEndTimeError()
    event = one_off_schedule_view_submission_event(emojis=None)
    body = (
        '{"response_action": "errors", "errors": {"end-time": '
        '"End date/time must be at least one hour after start date/time."}}'
    )
    expected_response = {
        "statusCode": 200,
        "body": body,
        "headers": {"Content-Type": "application/json"},
    }

    assert handler(event, None) == expected_response


def test_handler_processes_delete_schedule(handler, delete_schedule):
    actions = [
        {
            "action_id": "randomstring",
            "block_id": "2b6baa65-f050-4a21-a64f-513bac4f9c66",
            "text": {
                "type": "plain_text",
                "text": "Delete schedule",
            },
            "value": "delete_schedule",
            "type": "button",
            "action_ts": "1622569415.904575",
        },
    ]

    event = block_actions_event("home", actions)

    assert handler(event, None) == {"statusCode": 200}

    delete_schedule.assert_called_once_with(
        team_id="T1234",
        user_id="U1234",
        schedule_id="2b6baa65-f050-4a21-a64f-513bac4f9c66",
    )
