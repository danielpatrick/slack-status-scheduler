import json
from unittest.mock import MagicMock, Mock, patch

import pendulum
import pytest
from botocore.stub import Stubber

from application.delete_status_schedule import handler
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture
def delete_schedule():
    with patch(
        "application.delete_status_schedule.delete_schedule", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def mock_pendulum():
    with patch(
        "application.delete_status_schedule.pendulum", Mock(wraps=pendulum)
    ) as mock:
        mock.now.return_value = pendulum.datetime(
            2021, 5, 16, 13, tz="Europe/London"
        )

        yield mock


@pytest.fixture(autouse=True)
def sqs_stub():
    with Stubber(handler.__self__.sqs_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


def sqs_event(old_next_time="2021-05-16T12", new_next_time="NO_NEXT_TIME"):
    stream_record = schedule_stream_record(
        event_name="MODIFY",
        user_id="U1234",
        team_id="T1234",
        schedule_id="abc123",
        old_next_time=old_next_time,
        new_next_time=new_next_time,
        days=None,
    )
    body = stream_record["dynamodb"]

    record = {
        "body": json.dumps(body),
        "messageAttributes": {
            "eventName": {"stringValue": "MODIFY", "dataType": "String"},
            "nextTime": {"stringValue": new_next_time, "dataType": "String"},
        },
    }
    return {"Records": [record]}


def test_handler_exits_early_if_no_records(logger):
    event = {"Records": []}

    assert handler(event, None) is None

    logger.info.assert_any_call("No record in event; nothing to do")


def test_handler_deletes_schedule(delete_schedule, logger):
    event = sqs_event()

    expected_kwargs = {
        "team_id": "T1234",
        "user_id": "U1234",
        "schedule_id": "abc123",
    }

    assert handler(event, None) is None

    logger.info.assert_any_call("Deleting schedule", expected_kwargs)
    delete_schedule.assert_called_once_with(**expected_kwargs)


def test_handler_sends_to_sqs_with_delay_if_time_in_future(
    delete_schedule, logger, sqs_stub, mock_pendulum
):
    event = sqs_event()
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 16, 11, 48, tz="UTC"
    )
    expected_params = {
        "QueueUrl": "test-delete-schedule-queue",
        "MessageBody": event["Records"][0]["body"],
        "DelaySeconds": 720,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    assert delete_schedule.call_count == 0


def test_handler_sends_to_sqs_with_delay_for_over_fifteen_minutes(
    delete_schedule, logger, sqs_stub, mock_pendulum
):
    event = sqs_event()
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 16, 11, 40, tz="UTC"
    )
    expected_params = {
        "QueueUrl": "test-delete-schedule-queue",
        "MessageBody": event["Records"][0]["body"],
        "DelaySeconds": 900,
    }

    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) is None

    assert delete_schedule.call_count == 0
