from datetime import datetime
from unittest.mock import MagicMock
from uuid import uuid4

import pytest
from botocore.stub import Stubber

from application.scheduled_job import handler
from infrastructure.logger import Logger
from unit_tests.factories import scheduled_event


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def dynamodb_stub():
    with Stubber(handler.__self__.dynamodb_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


def test_handler_makes_expected_calls_to_dynamodb(dynamodb_stub, logger):
    event = scheduled_event(datetime(2021, 5, 16, 11, 45))
    user_id_schedule_id = "U1234#565289c9-24b6-41e5-b2b4-0d095a5dd9d4"
    query_params = {
        "TableName": "test-schedule-table",
        "IndexName": "NextEventIndex",
        "Select": "ALL_PROJECTED_ATTRIBUTES",
        "Limit": 100,
        "KeyConditionExpression": "NextEventBeginHour = :timestamp",
        "ExpressionAttributeValues": {":timestamp": {"S": "2021-05-16T12"}},
    }
    query_response = {
        "Items": [
            {
                "NextEventBeginHour": {"S": "2021-05-16T12"},
                "TeamId": {"S": "T1234"},
                "UserIdScheduleId": {"S": user_id_schedule_id},
                "Timezone": {"S": "Europe/London"},
                "Hour": {"N": "13"},
                "Duration": {"N": "1"},
                "Days": {"NS": ["0", "1", "2", "3", "4", "5", "6"]},
            }
        ],
    }
    update_item_params = {
        "TableName": "test-schedule-table",
        "Key": {
            "TeamId": {"S": "T1234"},
            "UserIdScheduleId": {"S": user_id_schedule_id},
        },
        "UpdateExpression": (
            "SET NextEventBeginHour = :next_time REMOVE CreatedBy"
        ),
        "ConditionExpression": "NextEventBeginHour = :prev_time",
        "ExpressionAttributeValues": {
            ":next_time": {"S": "2021-05-17T12"},
            ":prev_time": {"S": "2021-05-16T12"},
        },
    }

    dynamodb_stub.add_response(
        "query",
        service_response=query_response,
        expected_params=query_params,
    )
    dynamodb_stub.add_response(
        "update_item",
        service_response={},
        expected_params=update_item_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Response from dynamodb", query_response)
    logger.info.assert_any_call(
        "Updating schedule begin hour", update_item_params
    )


def test_handler_with_one_off_schedule(dynamodb_stub, logger):
    event = scheduled_event(datetime(2021, 5, 16, 11, 45))
    user_id_schedule_id = "U1234#565289c9-24b6-41e5-b2b4-0d095a5dd9d4"
    query_params = {
        "TableName": "test-schedule-table",
        "IndexName": "NextEventIndex",
        "Select": "ALL_PROJECTED_ATTRIBUTES",
        "Limit": 100,
        "KeyConditionExpression": "NextEventBeginHour = :timestamp",
        "ExpressionAttributeValues": {":timestamp": {"S": "2021-05-16T12"}},
    }
    query_response = {
        "Items": [
            {
                "NextEventBeginHour": {"S": "2021-05-16T12"},
                "TeamId": {"S": "T1234"},
                "UserIdScheduleId": {"S": user_id_schedule_id},
                "Timezone": {"S": "Europe/London"},
                "Hour": {"N": "13"},
                "Duration": {"N": "1"},
                "Days": {"NS": []},
            }
        ],
    }
    update_item_params = {
        "TableName": "test-schedule-table",
        "Key": {
            "TeamId": {"S": "T1234"},
            "UserIdScheduleId": {"S": user_id_schedule_id},
        },
        "UpdateExpression": (
            "SET NextEventBeginHour = :next_time REMOVE CreatedBy"
        ),
        "ConditionExpression": "NextEventBeginHour = :prev_time",
        "ExpressionAttributeValues": {
            ":next_time": {"S": "NO_NEXT_TIME"},
            ":prev_time": {"S": "2021-05-16T12"},
        },
    }

    dynamodb_stub.add_response(
        "query",
        service_response=query_response,
        expected_params=query_params,
    )
    dynamodb_stub.add_response(
        "update_item",
        service_response={},
        expected_params=update_item_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Response from dynamodb", query_response)
    logger.info.assert_any_call(
        "Updating schedule begin hour", update_item_params
    )


def test_handler_works_for_paged_responses(dynamodb_stub, logger):
    event = scheduled_event(datetime(2021, 5, 16, 11, 45))
    query_params_base = {
        "TableName": "test-schedule-table",
        "IndexName": "NextEventIndex",
        "Select": "ALL_PROJECTED_ATTRIBUTES",
        "Limit": 100,
        "KeyConditionExpression": "NextEventBeginHour = :timestamp",
        "ExpressionAttributeValues": {":timestamp": {"S": "2021-05-16T12"}},
    }
    update_logs_data = []
    query_responses = []
    user_id_schedule_id = None

    for i in range(3):
        if i == 0:
            query_params = {**query_params_base}
        else:
            query_params = {
                **query_params_base,
                "ExclusiveStartKey": {
                    "NextEventBeginHour": {"S": "2021-05-16T12"},
                    "TeamId": {"S": "T1234"},
                    "UserIdScheduleId": {"S": user_id_schedule_id},
                },
            }

        query_response = {"Items": []}

        for _ in range(100 if i < 2 else 50):
            schedule_id = str(uuid4())
            user_id_schedule_id = f"U1234#{schedule_id}"
            item = {
                "NextEventBeginHour": {"S": "2021-05-16T12"},
                "TeamId": {"S": "T1234"},
                "UserIdScheduleId": {"S": user_id_schedule_id},
                "Timezone": {"S": "Europe/London"},
                "Hour": {"N": "13"},
                "Duration": {"N": "1"},
                "Days": {"NS": ["0", "1", "2", "3", "4", "5", "6"]},
            }
            query_response["Items"].append(item)

        if i != 2:
            query_response["LastEvaluatedKey"] = {
                "NextEventBeginHour": {"S": "2021-05-16T12"},
                "TeamId": {"S": "T1234"},
                "UserIdScheduleId": {"S": user_id_schedule_id},
            }

        query_responses.append(query_response)

        dynamodb_stub.add_response(
            "query",
            service_response=query_response,
            expected_params=query_params,
        )

        for item in query_response["Items"]:
            update_item_params = {
                "TableName": "test-schedule-table",
                "Key": {
                    "TeamId": item["TeamId"],
                    "UserIdScheduleId": item["UserIdScheduleId"],
                },
                "UpdateExpression": (
                    "SET NextEventBeginHour = :next_time REMOVE CreatedBy"
                ),
                "ConditionExpression": "NextEventBeginHour = :prev_time",
                "ExpressionAttributeValues": {
                    ":next_time": {"S": "2021-05-17T12"},
                    ":prev_time": {"S": "2021-05-16T12"},
                },
            }

            update_logs_data.append(update_item_params)

            dynamodb_stub.add_response(
                "update_item",
                service_response={},
                expected_params=update_item_params,
            )

    assert handler(event, None) is None

    for query_response in query_responses:
        logger.info.assert_any_call("Response from dynamodb", query_response)

    for data in update_logs_data:
        logger.info.assert_any_call("Updating schedule begin hour", data)


def test_handler_with_on_the_hour_schedule(dynamodb_stub, logger):
    event = scheduled_event(datetime(2021, 5, 16, 12, 0))
    user_id_schedule_id = "U1234#565289c9-24b6-41e5-b2b4-0d095a5dd9d4"
    query_params = {
        "TableName": "test-schedule-table",
        "IndexName": "NextEventIndex",
        "Select": "ALL_PROJECTED_ATTRIBUTES",
        "Limit": 100,
        "KeyConditionExpression": "NextEventBeginHour = :timestamp",
        "ExpressionAttributeValues": {":timestamp": {"S": "2021-05-16T12"}},
    }
    query_response = {
        "Items": [
            {
                "NextEventBeginHour": {"S": "2021-05-16T12"},
                "TeamId": {"S": "T1234"},
                "UserIdScheduleId": {"S": user_id_schedule_id},
                "Timezone": {"S": "Europe/London"},
                "Hour": {"N": "13"},
                "Duration": {"N": "1"},
                "Days": {"NS": []},
            }
        ],
    }
    update_item_params = {
        "TableName": "test-schedule-table",
        "Key": {
            "TeamId": {"S": "T1234"},
            "UserIdScheduleId": {"S": user_id_schedule_id},
        },
        "UpdateExpression": (
            "SET NextEventBeginHour = :next_time REMOVE CreatedBy"
        ),
        "ConditionExpression": "NextEventBeginHour = :prev_time",
        "ExpressionAttributeValues": {
            ":next_time": {"S": "NO_NEXT_TIME"},
            ":prev_time": {"S": "2021-05-16T12"},
        },
    }

    dynamodb_stub.add_response(
        "query",
        service_response=query_response,
        expected_params=query_params,
    )
    dynamodb_stub.add_response(
        "update_item",
        service_response={},
        expected_params=update_item_params,
    )

    assert handler(event, None) is None

    logger.info.assert_any_call("Response from dynamodb", query_response)
    logger.info.assert_any_call(
        "Updating schedule begin hour", update_item_params
    )


def test_handler_with_unexpected_schedule(dynamodb_stub, logger):
    event = scheduled_event(datetime(2021, 5, 16, 12, 5))
    expected_err_msg = "Unexpected invocation at 5 past the hour"

    with pytest.raises(Exception, match=expected_err_msg):
        assert handler(event, None) is None
