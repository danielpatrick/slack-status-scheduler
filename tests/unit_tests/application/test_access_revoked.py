import json
from unittest.mock import MagicMock, patch

import pytest

from application.access_revoked import handler
from infrastructure.logger import Logger


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture
def event():
    image = {
        "team_id": "T1234",
        "entity_ids": ["U1234", "U2345", "U3456"],
    }
    yield {"Records": [{"body": json.dumps(image)}]}


@pytest.fixture(autouse=True)
def delete_user_or_team():
    with patch(
        "application.access_revoked.delete_user_or_team", autospec=True
    ) as mock:
        yield mock


def test_handler_deletes_all_entities(event, delete_user_or_team, logger):
    assert handler(event, None) is None

    assert delete_user_or_team.call_count == 3

    delete_user_or_team.assert_any_call(team_id="T1234", entity_id="U1234")
    delete_user_or_team.assert_any_call(team_id="T1234", entity_id="U2345")
    delete_user_or_team.assert_any_call(team_id="T1234", entity_id="U3456")

    logger.info.assert_any_call(
        "Deleting user/team", {"team_id": "T1234", "entity_id": "U1234"}
    )
    logger.info.assert_any_call(
        "Deleting user/team", {"team_id": "T1234", "entity_id": "U2345"}
    )
    logger.info.assert_any_call(
        "Deleting user/team", {"team_id": "T1234", "entity_id": "U3456"}
    )


def test_handler_exits_early_if_no_records(delete_user_or_team, logger):
    assert handler({"Records": []}, None) is None

    delete_user_or_team.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")
