import json
from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber

from application.user_table_stream import handler
from infrastructure.logger import Logger
from unit_tests.factories import team_user_stream_record


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def sns_stub():
    with Stubber(handler.__self__.sns_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


def test_handler_sends_user_modify_events_to_correct_topic_and_logs(
    sns_stub, logger
):
    record = team_user_stream_record(entity="USER", event_name="MODIFY")

    expected_params = {
        "TopicArn": "test-user-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "MODIFY", "DataType": "String"},
        },
    }
    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "123"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_user_insert_events_to_correct_topic_and_logs(
    sns_stub, logger
):
    record = team_user_stream_record(entity="USER", event_name="INSERT")

    expected_params = {
        "TopicArn": "test-user-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "INSERT", "DataType": "String"},
        },
    }
    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "123"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_user_remove_events_to_correct_topic_and_logs(
    sns_stub, logger
):
    record = team_user_stream_record(entity="USER", event_name="REMOVE")

    expected_params = {
        "TopicArn": "test-user-stream-topic",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "REMOVE", "DataType": "String"},
        },
    }
    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "123"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_does_not_send_team_events(sns_stub, logger):
    # Stubber would error if it got undeclared requests

    record1 = team_user_stream_record(entity="TEAM", event_name="MODIFY")
    record2 = team_user_stream_record(entity="TEAM", event_name="INSERT")
    record3 = team_user_stream_record(entity="TEAM", event_name="REMOVE")

    assert handler({"Records": [record1]}, None) is None
    assert handler({"Records": [record2]}, None) is None
    assert handler({"Records": [record3]}, None) is None

    logger.info.assert_any_call(
        "Skipping filtered record",
        {"record": record1},
    )
    logger.info.assert_any_call(
        "Skipping filtered record",
        {"record": record2},
    )
    logger.info.assert_any_call(
        "Skipping filtered record",
        {"record": record3},
    )
