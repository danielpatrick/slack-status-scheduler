import json
from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from application.user_home import handler
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture
def user_keys_event():
    image = {
        "Keys": {
            "TeamId": {"S": "T1234"},
            "EntityId": {"S": "U1234"},
        }
    }
    yield {"Records": [{"body": json.dumps(image)}]}


@pytest.fixture(autouse=True)
def update_home_tab():
    with patch("application.user_home.update_home_tab", autospec=True) as mock:
        yield mock


def test_handler_updates_user_home(user_keys_event, update_home_tab):
    handler(user_keys_event, None)

    update_home_tab.assert_called_once_with(user_id="U1234", team_id="T1234")


def test_handler_works_for_modify_schedule_records(update_home_tab):
    record = schedule_stream_record(
        event_name="MODIFY", user_id="U1234", team_id="T1234"
    )
    event = {"Records": [{"body": json.dumps(record["dynamodb"])}]}

    handler(event, None)

    update_home_tab.assert_called_once_with(user_id="U1234", team_id="T1234")


def test_handler_works_for_insert_schedule_records(update_home_tab):
    record = schedule_stream_record(
        event_name="INSERT", user_id="U1234", team_id="T1234"
    )
    event = {"Records": [{"body": json.dumps(record["dynamodb"])}]}

    handler(event, None)

    update_home_tab.assert_called_once_with(user_id="U1234", team_id="T1234")


def test_handler_does_not_swallow_errors(user_keys_event, update_home_tab):
    update_home_tab.side_effect = SlackApiError(
        "Something went wrong", {"ok": False}
    )

    with pytest.raises(SlackApiError, match="Something went wrong"):
        handler(user_keys_event, None)


def test_handler_exits_early_if_no_records(update_home_tab, logger):
    assert handler({"Records": []}, None) is None

    update_home_tab.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")
