import json
from unittest.mock import MagicMock, patch

import pytest

from application.delete_user_schedules import handler
from infrastructure.logger import Logger


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture
def event():
    image = {
        "Keys": {
            "TeamId": {"S": "T1234"},
            "EntityId": {"S": "U1234"},
        }
    }

    yield {"Records": [{"body": json.dumps(image)}]}


@pytest.fixture(autouse=True)
def delete_schedule():
    with patch(
        "application.delete_user_schedules.delete_schedule", autospec=True
    ) as mock:
        yield mock


@pytest.fixture(autouse=True)
def get_user_schedule_ids():
    with patch(
        "application.delete_user_schedules.get_user_schedule_ids",
        autospec=True,
    ) as mock:
        mock.return_value = ["schedule1", "schedule2"]

        yield mock


def test_handler_deletes_all_schedules(
    event, delete_schedule, get_user_schedule_ids, logger
):
    assert handler(event, None) is None

    get_user_schedule_ids.assert_called_once_with(
        team_id="T1234", user_id="U1234"
    )

    assert delete_schedule.call_count == 2

    delete_schedule.assert_any_call(
        user_id="U1234",
        team_id="T1234",
        schedule_id="schedule1",
        update_user=False,
    )
    delete_schedule.assert_any_call(
        user_id="U1234",
        team_id="T1234",
        schedule_id="schedule2",
        update_user=False,
    )

    logger.info.assert_any_call(
        "Deleting schedule",
        {"user": "U1234", "team": "T1234", "schedule_id": "schedule1"},
    )
    logger.info.assert_any_call(
        "Deleting schedule",
        {"user": "U1234", "team": "T1234", "schedule_id": "schedule2"},
    )


def test_handler_exits_early_if_no_records(
    delete_schedule, get_user_schedule_ids, logger
):
    assert handler({"Records": []}, None) is None

    get_user_schedule_ids.assert_not_called()
    delete_schedule.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")
