import json
from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from application.send_to_slack import handler
from infrastructure.logger import Logger


@pytest.fixture(autouse=True)
def logger():
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def get_user_info():
    with patch(
        "application.send_to_slack.get_user_info", autospec=True
    ) as mock:
        mock.return_value = "token123"
        yield mock


@pytest.fixture(autouse=True)
def WebClient():
    with patch("application.send_to_slack.WebClient", autospec=True) as mock:
        yield mock


@pytest.fixture
def web_client(WebClient):
    client = WebClient.return_value
    client.users_profile_set.return_value = users_profile_response()
    client.users_profile_get.return_value = users_profile_response()

    yield client


def sqs_event(overwrite=True):
    return {
        "Records": [
            {
                "body": json.dumps(
                    {
                        "start_time": 1621166400,
                        "end_time": 1621170000,
                        "status_text": "Out for lunch",
                        "status_emoji": ":sandwich:",
                        "overwrite": overwrite,
                        "user_id": "user-id",
                        "team_id": "team-id",
                    }
                ),
            }
        ]
    }


def users_profile_response(text="", emoji="", expiration=0, team_id="T1234"):
    return {
        "ok": True,
        "profile": {
            "avatar_hash": "ge3b51ca72de",
            "status_text": text,
            "status_emoji": emoji,
            "status_expiration": expiration,
            "real_name": "Bob",
            "display_name": "bob",
            "real_name_normalized": "Bob",
            "display_name_normalized": "bob",
            "email": "bob@example.com",
            "image_original": "some-url",
            "image_24": "some-url",
            "image_32": "some-url",
            "image_48": "some-url",
            "image_72": "some-url",
            "image_192": "some-url",
            "image_512": "some-url",
            "team": team_id,
        },
    }


def test_handler_sends_expected_payload(WebClient, web_client, logger):
    event = sqs_event()
    expected_kwargs = {
        "profile": {
            "status_text": "Out for lunch",
            "status_emoji": ":sandwich:",
            "status_expiration": 1621170000,
        }
    }

    handler(event, None)

    WebClient.assert_called_once_with(token="token123")

    assert web_client.users_profile_get.call_count == 0
    web_client.users_profile_set.assert_called_once_with(**expected_kwargs)
    logger.info.assert_any_call("Setting user status", expected_kwargs)


def test_handler_does_not_set_status_if_overwrite_false_and_existing_Status(
    WebClient, web_client, logger
):
    event = sqs_event(False)

    # Ends one second after new status would begin
    web_client.users_profile_get.return_value = users_profile_response(
        "Lunch", ":pizza:", 1621166401
    )

    handler(event, None)

    WebClient.assert_called_once_with(token="token123")

    web_client.users_profile_get.assert_called_once_with()
    assert web_client.users_profile_set.call_count == 0

    logger.info.assert_any_call(
        "Not setting status because user already has one set"
    )


def test_handler_sets_status_if_overwrite_false_and_expiring_existing_status(
    WebClient, web_client, logger
):
    event = sqs_event(False)
    expected_kwargs = {
        "profile": {
            "status_text": "Out for lunch",
            "status_emoji": ":sandwich:",
            "status_expiration": 1621170000,
        }
    }
    # Ends at same time new status would begin
    web_client.users_profile_get.return_value = users_profile_response(
        "Lunch", ":pizza:", 1621166400
    )

    handler(event, None)

    WebClient.assert_called_once_with(token="token123")

    web_client.users_profile_get.assert_called_once_with()

    web_client.users_profile_set.assert_called_once_with(**expected_kwargs)
    logger.info.assert_any_call("Setting user status", expected_kwargs)


def test_handler_sets_status_if_overwrite_false_and_no_existing_status(
    WebClient, web_client, logger
):
    event = sqs_event(False)
    expected_kwargs = {
        "profile": {
            "status_text": "Out for lunch",
            "status_emoji": ":sandwich:",
            "status_expiration": 1621170000,
        }
    }
    # No status set
    web_client.users_profile_get.return_value = users_profile_response()

    handler(event, None)

    WebClient.assert_called_once_with(token="token123")

    web_client.users_profile_get.assert_called_once_with()

    web_client.users_profile_set.assert_called_once_with(**expected_kwargs)
    logger.info.assert_any_call("Setting user status", expected_kwargs)


def test_handler_raises_if_no_token(get_user_info, WebClient, web_client):
    event = sqs_event()
    get_user_info.return_value = None

    with pytest.raises(Exception, match="Unable to retreive Slack user token"):
        handler(event, None)


def test_handler_raises_on_slack_api_error_for_get(WebClient, web_client):
    event = sqs_event(False)
    web_client.users_profile_get.side_effect = SlackApiError(
        "Some Slack API error", {"ok": False}
    )

    with pytest.raises(SlackApiError, match="Some Slack API error"):
        handler(event, None)


def test_handler_raises_on_slack_api_error_for_set(WebClient, web_client):
    event = sqs_event()
    web_client.users_profile_set.side_effect = SlackApiError(
        "Some Slack API error", {"ok": False}
    )

    with pytest.raises(SlackApiError, match="Some Slack API error"):
        handler(event, None)


def test_handler_exits_early_if_no_records(WebClient, web_client, logger):
    assert handler({"Records": []}, None) is None

    WebClient.assert_not_called()
    web_client.users_profile_get.assert_not_called()
    web_client.users_profile_set.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")
