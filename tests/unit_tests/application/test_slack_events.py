from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber

from infrastructure.logger import Logger
from unit_tests.factories import events_api_event


@pytest.fixture
def handler(secretsmanager_stub):
    import application.slack_events

    yield application.slack_events.handler


@pytest.fixture(autouse=True)
def sqs_stub(handler):
    with Stubber(handler.__self__.sqs_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


@pytest.fixture(autouse=True)
def logger(handler):
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


def app_home_opened_event(
    *, tab="home", include_view=False, valid_signature=True
):
    body = {
        "token": "some-token",
        "team_id": "T1234",
        "api_app_id": "A1234",
        "event": {
            "type": "app_home_opened",
            "user": "U1234",
            "channel": "D1234",
            "tab": tab,
            "event_ts": "1626352405.040112",
        },
        "type": "event_callback",
        "event_id": "Ev1234",
        "event_time": 1626352405,
        "authorizations": [
            {
                "enterprise_id": "None",
                "team_id": "T1234",
                "user_id": "U1234",
                "is_bot": True,
                "is_enterprise_install": False,
            }
        ],
        "is_ext_shared_channel": False,
    }

    if include_view:
        body["event"]["view"] = {
            "id": "V1234",
            "team_id": "T1234",
            "type": "home",
            "blocks": [],
            "private_metadata": "",
            "callback_id": "",
            "state": {"values": {}},
            "hash": "1234.abcd",
            "title": {
                "type": "plain_text",
                "text": "View Title",
                "emoji": True,
            },
            "clear_on_close": False,
            "notify_on_close": False,
            "close": "None",
            "submit": "None",
            "previous_view_id": "None",
            "root_view_id": "V1234",
            "app_id": "A1234",
            "external_id": "",
            "app_installed_team_id": "T1234",
            "bot_id": "B1234",
        }

    return events_api_event(body, valid_signature)


def tokens_revoked_event():
    body = {
        "token": "some-token",
        "team_id": "T1234",
        "api_app_id": "A1234",
        "event": {
            "type": "tokens_revoked",
            "tokens": {
                "oauth": [
                    "U1234",
                    "U2345",
                ],
                "bot": [
                    "U6789",
                ],
            },
            "event_ts": "1234567890.123456",
        },
        "type": "event_callback",
        "event_id": "Ev1234",
        "event_time": 1626505853,
    }

    return events_api_event(body)


def test_handler_returns_403_if_auth_fails(handler):
    event = app_home_opened_event(valid_signature=False)

    assert handler(event, None) == {"statusCode": 403, "body": "Forbidden"}


def test_handler_rejects_unhandled_event_type(handler):
    event = events_api_event({"event_type": "some_unhandled_event"})

    assert handler(event, None) == {"statusCode": 400, "body": "Bad request"}


def test_handler_sends_home_opened_message_to_sqs(handler, sqs_stub):
    event = app_home_opened_event()

    expected_body = (
        '{"Keys": {"TeamId": {"S": "T1234"}, "EntityId": {"S": "U1234"}}}'
    )
    expected_params = {
        "QueueUrl": "test-home-tab-queue",
        "MessageBody": expected_body,
    }
    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) == {"statusCode": 200}


def test_handler_ignores_non_home_tab(handler, logger):
    event = app_home_opened_event(tab="messages")

    assert handler(event, None) == {"statusCode": 200}

    logger.info.assert_any_call(
        "Ignoring event because not home tab",
        {"tab": "messages"},
    )


def test_handler_ignores_if_view_exists(handler, logger):
    event = app_home_opened_event(include_view=True)

    assert handler(event, None) == {"statusCode": 200}

    logger.info.assert_any_call(
        "Ignoring event because user already has a home tab view"
    )


def test_handler_sends_tokens_revoked_message_to_sqs(handler, sqs_stub):
    event = tokens_revoked_event()

    expected_body = (
        '{"team_id": "T1234", "entity_ids": ["U1234", "U2345", "T1234"]}'
    )
    expected_params = {
        "QueueUrl": "test-access-revoked-queue",
        "MessageBody": expected_body,
    }
    sqs_stub.add_response(
        "send_message",
        service_response={},
        expected_params=expected_params,
    )

    assert handler(event, None) == {"statusCode": 200}
