import json
from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from domain.db.users import BotInfo
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record

module_path = "application.schedule_created"


@pytest.fixture
def handler(secretsmanager_stub):
    import application.schedule_created

    yield application.schedule_created.handler


@pytest.fixture(autouse=True)
def logger(handler):
    mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

    handler.__self__.create_logger = lambda self: mock_logger

    yield mock_logger


@pytest.fixture(autouse=True)
def get_user_info():
    with patch(f"{module_path}.get_user_info", autospec=True) as mock:
        mock.return_value = "bot-token"
        yield mock


@pytest.fixture(autouse=True)
def WebClient():
    with patch(f"{module_path}.WebClient", autospec=True) as mock:
        yield mock


@pytest.fixture(autouse=True)
def web_client(WebClient):
    yield WebClient.return_value


def event(created_by="USER"):
    record = schedule_stream_record(
        event_name="INSERT",
        user_id="U1234",
        team_id="T1234",
        created_by=created_by,
    )

    return {"Records": [{"body": json.dumps(record["dynamodb"])}]}


def test_handler_uses_bot_token(handler, WebClient, get_user_info):
    assert handler(event(), None) is None

    get_user_info.assert_called_once_with("T1234", "U1234", BotInfo.BOT_TOKEN)
    WebClient.assert_called_once_with("bot-token")


def test_handler_exits_early_if_no_records(
    handler, WebClient, web_client, logger
):
    assert handler({"Records": []}, None) is None

    WebClient.assert_not_called()
    web_client.assert_not_called()

    logger.info.assert_any_call("No record in event; nothing to do")


def test_handler_exits_early_if_no_created_by_info(
    handler, WebClient, web_client, logger
):
    record = schedule_stream_record(
        event_name="INSERT", user_id="U1234", team_id="T1234"
    )
    del record["dynamodb"]["NewImage"]["CreatedBy"]
    event = {"Records": [{"body": json.dumps(record["dynamodb"])}]}

    assert handler(event, None) is None

    WebClient.assert_not_called()
    web_client.assert_not_called()

    logger.info.assert_any_call(
        "Item not created by user - not sending message",
        {"created_by": None},
    )


def test_handler_exits_early_if_not_created_by_user(
    handler, WebClient, web_client, logger
):
    assert handler(event(created_by="SYSTEM"), None) is None

    WebClient.assert_not_called()
    web_client.assert_not_called()

    logger.info.assert_any_call(
        "Item not created by user - not sending message",
        {"created_by": "SYSTEM"},
    )


def test_handler_sends_expected_message(handler, web_client):
    expected_text = (
        "Scheduled status created. Click "
        "<slack://app?team=T1234&id=APP1234&tab=home|here> "
        "to view and manage your scheduled statuses."
    )

    assert handler(event(), None) is None

    web_client.chat_postEphemeral.assert_called_once_with(
        channel="U1234", user="U1234", text=expected_text
    )


def test_handler_logs_before_sending(handler, logger):
    expected_text = (
        "Scheduled status created. Click "
        "<slack://app?team=T1234&id=APP1234&tab=home|here> "
        "to view and manage your scheduled statuses."
    )

    handler(event(), None)

    logger.info.assert_any_call(
        "Sending ephemeral message to user",
        {"channel": "U1234", "user": "U1234", "text": expected_text},
    )


def test_confirm_message_raises_on_failure(handler, WebClient, web_client):
    web_client.chat_postEphemeral.side_effect = SlackApiError(
        "Nope", {"ok": False}
    )

    with pytest.raises(SlackApiError, match="Nope"):
        handler(event(), None)


def test_confirm_raises_if_no_token(
    handler, get_user_info, WebClient, web_client
):
    get_user_info.return_value = None

    with pytest.raises(Exception, match="Unable to retreive Slack bot token"):
        handler(event(), None)

    WebClient.assert_not_called()
    web_client.assert_not_called()
