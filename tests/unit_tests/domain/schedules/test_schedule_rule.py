import pytest

from domain.schedules import ScheduleRule, ValidationException
from unit_tests import factories


class TestScheduleRule:
    def test_constructor(self):
        days = [0, 2, 4, 6]

        rule = factories.rule(days=days)

        # Confirm constructor doesn't store references to passed in arguments
        days.append(1)

        assert rule.next_time == "2021-05-16T11"
        assert rule.duration == 1
        assert rule.days == [0, 2, 4, 6]

    def test_constructor_rejects_invalid_next_time(self):
        with pytest.raises(ValidationException):
            factories.rule(next_time="2021-05-16T24")

        with pytest.raises(ValidationException):
            factories.rule(next_time="2021-05-16T1")

    def test_constructor_rejects_invalid_timezone(self):
        with pytest.raises(ValidationException):
            factories.rule(timezone="not-a-real-tz")

    def test_constructor_rejects_invalid_duration(self):
        with pytest.raises(ValidationException):
            factories.rule(duration=0)

        # Confirm valid durations okay
        factories.rule(duration=1)
        factories.rule(duration=24)
        factories.rule(duration=720)

    def test_constructor_rejects_invalid_days(self):
        with pytest.raises(ValidationException):
            factories.rule(days=[-1])

        with pytest.raises(ValidationException):
            factories.rule(days=[7])

        with pytest.raises(ValidationException):
            factories.rule(days=[6, 7])

        # Confirm valid days okay
        factories.rule(days=[0])
        factories.rule(days=[0, 6])
        factories.rule(days=[0, 1, 2, 3, 4, 5, 6])
        factories.rule(days=[6])

    def test_constructor_removes_duplicate_days_and_sorts(self):
        rule = factories.rule(days=[6, 5, 4, 4, 3])
        assert rule.days == [3, 4, 5, 6]

    def test_next(self):
        next_rule = next(factories.rule(next_time="2021-05-16T11"))

        assert type(next_rule) == ScheduleRule

        assert next_rule.duration == 1
        assert next_rule.days == [0, 2, 4, 6]

        assert next_rule.next_time == "2021-05-17T11"
        assert next(next_rule).next_time == "2021-05-19T11"

    def test_next_does_not_use_references(self):
        rule = factories.rule()

        next_rule = next(rule)
        rule.days.append(1)

        assert next_rule.days == [0, 2, 4, 6]

    def test_next_works_across_timezone_adjustments(self):
        rule = factories.rule(
            next_time="2021-10-30T11", timezone="Europe/London"
        )
        assert next(rule).next_time == "2021-10-31T12"
