import pytest

from domain.schedules import StatusScheduleRule, ValidationException
from unit_tests import factories


class TestStatusScheduleRule:
    def test_constructor(self):
        emojis = [":knife_fork_plate:", ":sandwich:"]

        rule = factories.status_rule(status_emojis=emojis)

        # Confirm constructor doesn't store references to passed in arguments
        emojis.append(":doge:")

        assert rule.next_time == "2021-05-16T11"
        assert rule.duration == 1
        assert rule.days == [0, 2, 4, 6]
        assert rule.status_text == "Out for lunch"
        assert rule.status_emojis == [":knife_fork_plate:", ":sandwich:"]
        assert rule.overwrite is True

    def test_constructor_rejects_invalid_status_text(self):
        with pytest.raises(ValidationException):
            factories.status_rule(status_text="a" * 101)

        with pytest.raises(ValidationException):
            factories.status_rule(status_text="")

    def test_constructor_accepts_empty_status_emoji_list(self):
        rule = factories.status_rule(status_emojis=[])
        assert rule.status_emojis == []

    def test_constructor_rejects_invalid_status_emojis(self):
        with pytest.raises(ValidationException):
            factories.status_rule(status_emojis=["::"])

        with pytest.raises(ValidationException):
            factories.status_rule(status_emojis=[":" + "a" * 99 + ":"])

        with pytest.raises(ValidationException):
            factories.status_rule(status_emojis=["nope:"])

        with pytest.raises(ValidationException):
            factories.status_rule(status_emojis=[":nope"])

    def test_constructor_rejects_invalid_overwrite(self):
        with pytest.raises(ValidationException):
            factories.status_rule(overwrite="true")

    def test_next(self):
        next_rule = next(factories.status_rule(next_time="2021-05-16T11"))

        assert type(next_rule) == StatusScheduleRule

        assert next_rule.duration == 1
        assert next_rule.days == [0, 2, 4, 6]
        assert next_rule.status_text == "Out for lunch"
        assert next_rule.status_emojis == [":knife_fork_plate:", ":sandwich:"]
        assert next_rule.overwrite is True

        assert next_rule.next_time == "2021-05-17T11"
        assert next(next_rule).next_time == "2021-05-19T11"

    def test_next_does_not_use_references(self):
        rule = factories.status_rule()

        next_rule = next(rule)
        rule.status_emojis.append(":doge:")

        assert next_rule.status_emojis == [":knife_fork_plate:", ":sandwich:"]
