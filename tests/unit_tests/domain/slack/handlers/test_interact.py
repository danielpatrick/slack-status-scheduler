import os
import traceback
from unittest.mock import ANY, MagicMock

import pytest

from domain.slack.handlers import InteractHandler
from infrastructure.handlers.api_gateway import (
    HttpBadRequestError,
    HttpForbiddenError,
)
from unit_tests.factories import interact_event

SECRET = os.environ["SLACK_VALID_SIGNING_SECRET"]


class TestInteractHandler:
    @pytest.fixture
    def logger(self):
        yield MagicMock()

    @pytest.fixture
    def SlackInteractHandler(self, logger):
        class Handler(InteractHandler):
            SLACK_SIGNING_SECRET = SECRET

            def create_logger(self):
                return logger

        yield Handler

    def assert_dispatch(self, Handler, interaction_type):
        method = MagicMock()
        setattr(Handler, interaction_type, method)

        payload = {
            "type": interaction_type,
            "team": {"id": "T1234"},
            "user": {"id": "U1234"},
        }

        event = interact_event(payload)

        Handler.handler(event, None)

        method.assert_called_once_with(team_id="T1234", user_id="U1234")

    def assert_error_handling(self, Handler, interaction_type):
        method = MagicMock(side_effect=HttpBadRequestError("Invalid request"))
        setattr(Handler, interaction_type, method)

        payload = {
            "type": interaction_type,
            "team": {"id": "T1234"},
            "user": {"id": "U1234"},
        }

        event = interact_event(payload)

        response = Handler.handler(event, None)

        assert response == {"statusCode": 400, "body": "Bad request"}

    def assert_warns_unhandled(self, Handler, interaction_type, logger):
        payload = {
            "type": interaction_type,
            "team": {"id": "T1234"},
            "user": {"id": "U1234"},
        }

        event = interact_event(payload)

        Handler.handler(event, None)

        logger.warning.assert_called_once_with(
            "Unhandled interaction type",
            {
                "interaction_type": interaction_type,
                "payload": payload,
            },
        )

    def test_rejects_invalid_signature(self, SlackInteractHandler, logger):
        event = interact_event({}, False)
        response = SlackInteractHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert "Invalid HMAC signature" in tb
        assert "HMAC verification error" in tb

    def test_rejects_old_timestamp(self, SlackInteractHandler, logger):
        event = interact_event({}, True, False)
        response = SlackInteractHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        expected_err = (
            "Rejecting HMAC signature because timestamp is more than 60 "
            "seconds in the past or in the future"
        )

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert expected_err in tb
        assert "HMAC verification error" in tb

    def test_parse_body(self, SlackInteractHandler):
        payload = {
            "type": "shortcut",
            "token": "TOKEN",
            "action_ts": "1581106241.371594",
            "team": {"id": "T1234", "domain": "shortcuts-test"},
            "user": {
                "id": "U1234",
                "username": "USERNAME",
                "team_id": "T1234",
            },
            "is_enterprise_install": False,
            "enterprise": None,
            "callback_id": "schedule-status-begin",
            "trigger_id": "TRIGGER-ID",
        }

        event = interact_event(payload)

        handler = SlackInteractHandler(event, None)
        handler.parse_event()

        assert handler.body == payload

    def test_shortcut_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(SlackInteractHandler, "shortcut", logger)
        self.assert_error_handling(SlackInteractHandler, "shortcut")
        self.assert_dispatch(SlackInteractHandler, "shortcut")

    def test_message_action_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(
            SlackInteractHandler, "message_action", logger
        )
        self.assert_error_handling(SlackInteractHandler, "message_action")
        self.assert_dispatch(SlackInteractHandler, "message_action")

    def test_block_actions_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(
            SlackInteractHandler, "block_actions", logger
        )
        self.assert_error_handling(SlackInteractHandler, "block_actions")
        self.assert_dispatch(SlackInteractHandler, "block_actions")

    def test_interactive_message_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(
            SlackInteractHandler, "interactive_message", logger
        )
        self.assert_error_handling(SlackInteractHandler, "interactive_message")
        self.assert_dispatch(SlackInteractHandler, "interactive_message")

    def test_view_submission_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(
            SlackInteractHandler, "view_submission", logger
        )
        self.assert_error_handling(SlackInteractHandler, "view_submission")
        self.assert_dispatch(SlackInteractHandler, "view_submission")

    def test_view_closed_handling(self, SlackInteractHandler, logger):
        self.assert_warns_unhandled(
            SlackInteractHandler, "view_closed", logger
        )
        self.assert_error_handling(SlackInteractHandler, "view_closed")
        self.assert_dispatch(SlackInteractHandler, "view_closed")

    def test_rejects_invalid_interaction_type(
        self, SlackInteractHandler, logger
    ):
        payload = {
            "type": "made_up_interaction_type",
            "team": {"id": "T1234"},
            "user": {"id": "U1234"},
        }

        event = interact_event(payload)

        response = SlackInteractHandler.handler(event, None)

        logger.warning.assert_called_once_with(
            "Unknown interaction type",
            {
                "interaction_type": "made_up_interaction_type",
                "payload": payload,
            },
        )
        assert response == {"statusCode": 400, "body": "Bad request"}
