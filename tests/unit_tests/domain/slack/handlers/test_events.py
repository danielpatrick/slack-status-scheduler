import os
import traceback
from unittest.mock import ANY, MagicMock

import pytest

from domain.slack.handlers import EventsHandler
from infrastructure.handlers.api_gateway import (
    HttpBadRequestError,
    HttpForbiddenError,
)
from unit_tests.factories import events_api_event

SECRET = os.environ["SLACK_VALID_SIGNING_SECRET"]


class TestEventsHandler:
    @pytest.fixture
    def logger(self):
        yield MagicMock()

    @pytest.fixture
    def SlackEventsHandler(self, logger):
        class Handler(EventsHandler):
            SLACK_SIGNING_SECRET = SECRET

            def create_logger(self):
                return logger

        yield Handler

    def assert_dispatch(self, Handler, event_type, method_name=None):
        if method_name is None:
            method_name = event_type

        method = MagicMock()
        setattr(Handler, method_name, method)

        payload = {
            "type": "event_callback",
            "event": {"type": event_type},
        }

        event = events_api_event(payload)

        Handler.handler(event, None)

        method.assert_called_once_with(payload)

    def assert_error_handling(self, Handler, event_type, method_name=None):
        if method_name is None:
            method_name = event_type

        method = MagicMock(side_effect=HttpBadRequestError("Invalid request"))
        setattr(Handler, method_name, method)

        payload = {"type": "event_callback", "event": {"type": event_type}}

        event = events_api_event(payload)

        response = Handler.handler(event, None)

        assert response == {"statusCode": 400, "body": "Bad request"}

    def assert_warns_unhandled(self, Handler, event_type, logger):
        payload = {"type": "event_callback", "event": {"type": event_type}}

        Handler.handler(events_api_event(payload), None)

        logger.warning.assert_called_once_with(
            "Unhandled event type",
            {
                "event_type": event_type,
                "payload": payload,
            },
        )

    def assert_return_value_with_body(
        self, Handler, event_type, method_name=None
    ):
        if method_name is None:
            method_name = event_type

        setattr(Handler, method_name, lambda self, body: body["some_key"])

        payload = {
            "type": "event_callback",
            "event": {"type": event_type},
            "some_key": "some_value",
        }
        event = events_api_event(payload)

        expected_response = {
            "statusCode": 200,
            "body": "some_value",
            "headers": {"Content-Type": "application/json"},
        }

        assert Handler.handler(event, None) == expected_response

    def assert_return_value_without_body(
        self, Handler, event_type, method_name=None
    ):
        if method_name is None:
            method_name = event_type

        setattr(Handler, method_name, lambda self, body: None)

        payload = {
            "type": "event_callback",
            "event": {"type": event_type},
            "some_key": "some_value",
        }
        event = events_api_event(payload)

        expected_response = {"statusCode": 200}

        assert Handler.handler(event, None) == expected_response

    def test_rejects_invalid_signature(self, SlackEventsHandler, logger):
        event = events_api_event({"type": "some_event"}, False)
        response = SlackEventsHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert "Invalid HMAC signature" in tb
        assert "HMAC verification error" in tb

    def test_rejects_old_timestamp(self, SlackEventsHandler, logger):
        event = events_api_event({}, True, False)
        response = SlackEventsHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        expected_err = (
            "Rejecting HMAC signature because timestamp is more than 60 "
            "seconds in the past or in the future"
        )

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert expected_err in tb
        assert "HMAC verification error" in tb

    def test_parse_body(self, SlackEventsHandler):
        payload = {
            "type": "event_callback",
            "event": {
                "type": "app_home_opened",
                "user": "U061F7AUR",
                "channel": "D0LAN2Q65",
                "event_ts": "1515449522000016",
                "tab": "home",
            },
        }

        event = events_api_event(payload)

        handler = SlackEventsHandler(event, None)
        handler.parse_event()

        assert handler.body == payload

    def test_dispatch_handling(self, SlackEventsHandler, logger):
        self.assert_warns_unhandled(SlackEventsHandler, "message", logger)
        self.assert_error_handling(SlackEventsHandler, "message")
        self.assert_dispatch(SlackEventsHandler, "message")
        self.assert_return_value_with_body(SlackEventsHandler, "message")
        self.assert_return_value_without_body(SlackEventsHandler, "message")

    def test_dispatch_handling_with_dotted_event(
        self, SlackEventsHandler, logger
    ):
        self.assert_warns_unhandled(SlackEventsHandler, "message.im", logger)
        self.assert_error_handling(
            SlackEventsHandler, "message.im", "message__im"
        )
        self.assert_dispatch(SlackEventsHandler, "message.im", "message__im")
        self.assert_return_value_with_body(
            SlackEventsHandler, "message.im", "message__im"
        )
        self.assert_return_value_without_body(
            SlackEventsHandler, "message.im", "message__im"
        )

    def test_rejects_invalid_event_type(self, SlackEventsHandler, logger):
        payload = {
            "type": "event_callback",
            "event": {
                "type": "made_up_event_type",
            },
        }

        event = events_api_event(payload)

        expected_log = (
            "Unknown event type",
            {
                "event_type": "made_up_event_type",
                "payload": payload,
            },
        )

        expected_response = {"statusCode": 400, "body": "Bad request"}

        assert SlackEventsHandler.handler(event, None) == expected_response

        logger.warning.assert_called_once_with(*expected_log)

    def test_url_verification_response(self, SlackEventsHandler):
        payload = {"type": "url_verification", "challenge": "helloworld"}
        event = events_api_event(payload)

        expected_response = {
            "statusCode": 200,
            "body": '{"challenge": "helloworld"}',
            "headers": {"Content-Type": "application/json"},
        }

        assert SlackEventsHandler.handler(event, None) == expected_response

    def test_rejects_unknown_body_type(self, SlackEventsHandler, logger):
        payload = {"type": "made_up_type"}
        event = events_api_event(payload)

        expected_response = {
            "statusCode": 200,
            "body": '{"challenge": "helloworld"}',
            "headers": {"Content-Type": "application/json"},
        }

        expected_log = (
            "Unexpected body type",
            {
                "body_type": "made_up_type",
                "payload": payload,
            },
        )

        expected_response = {"statusCode": 400, "body": "Bad request"}

        assert SlackEventsHandler.handler(event, None) == expected_response

        logger.warning.assert_called_once_with(*expected_log)
