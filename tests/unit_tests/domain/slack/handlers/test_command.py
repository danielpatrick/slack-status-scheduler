import os
import traceback
from unittest.mock import ANY, MagicMock

import pytest

from domain.slack.handlers import SlashCommandHandler
from infrastructure.handlers.api_gateway import HttpForbiddenError
from unit_tests.factories import slash_event

SECRET = os.environ["SLACK_VALID_SIGNING_SECRET"]


@pytest.fixture
def logger():
    yield MagicMock()


class TestSlashCommandHandler:
    @pytest.fixture
    def ScheduleStatusHandler(self, logger):
        class Handler(SlashCommandHandler):
            SLACK_SIGNING_SECRET = SECRET
            COMMAND_NAME = "/schedule-status"

            def create_logger(self):
                return logger

        yield Handler

    def test_rejects_invalid_signature(self, ScheduleStatusHandler, logger):
        event = slash_event({}, valid_signature=False)
        response = ScheduleStatusHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert "Invalid HMAC signature" in tb
        assert "HMAC verification error" in tb

    def test_rejects_old_timestamp(self, ScheduleStatusHandler, logger):
        event = slash_event({}, valid_timestamp=False)
        response = ScheduleStatusHandler.handler(event, None)

        assert response == {"statusCode": 403, "body": "Forbidden"}

        expected_err = (
            "Rejecting HMAC signature because timestamp is more than 60 "
            "seconds in the past or in the future"
        )

        logger.error.assert_called_with(
            "Lambda invocation failure",
            exception=ANY,
        )

        e = logger.error.call_args.kwargs["exception"]

        assert isinstance(e, HttpForbiddenError)
        assert str(e) == "HMAC verification error"

        tb = "".join(
            traceback.format_exception(HttpForbiddenError, e, e.__traceback__)
        )

        assert expected_err in tb
        assert "HMAC verification error" in tb

    def test_parse_body(self, ScheduleStatusHandler):
        payload = {
            "token": "TOKEN",
            "team_id": "T1234",
            "team_domain": "slash-test",
            "channel_id": "D1234",
            "channel_name": "directmessage",
            "user_id": "U1234",
            "user_name": "USERNAME",
            "command": "/schedule-status",
            "api_app_id": "A1234",
            "is_enterprise_install": "false",
            "response_url": "RESPONSE-URL",
            "trigger_id": "TRIGGER-ID",
        }
        event = slash_event(payload)

        handler = ScheduleStatusHandler(event, None)
        handler.parse_event()

        assert handler.body == payload

    def test_rejects_wrong_command_name(self, ScheduleStatusHandler):
        payload = {
            "token": "TOKEN",
            "team_id": "T1234",
            "team_domain": "slash-test",
            "channel_id": "D1234",
            "channel_name": "directmessage",
            "user_id": "U1234",
            "user_name": "USERNAME",
            "command": "/shrug",
            "api_app_id": "A1234",
            "is_enterprise_install": "false",
            "response_url": "RESPONSE-URL",
            "trigger_id": "TRIGGER-ID",
        }
        event = slash_event(payload)

        response = ScheduleStatusHandler.handler(event, None)

        assert response == {"statusCode": 400, "body": "Bad request"}
