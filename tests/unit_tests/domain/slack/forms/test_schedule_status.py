from unittest.mock import Mock, patch

import pendulum
import pytest

from domain.slack.forms import schedule_status_form


@pytest.fixture(autouse=True)
def mock_pendulum():
    with patch(
        "domain.slack.forms.schedule_status.pendulum", Mock(wraps=pendulum)
    ) as mock:
        mock.now.return_value = pendulum.datetime(2021, 7, 30)

        yield mock


@pytest.fixture
def modal():
    yield schedule_status_form("ContinentA/CityB", [":pizza:", ":burrito:"])


@pytest.fixture
def no_emojis_modal():
    yield schedule_status_form("ContinentA/CityB")


def test_schedule_status_form_has_top_level_keys(modal):
    expected_view_keys = {
        "type",
        "title",
        "submit",
        "close",
        "private_metadata",
        "callback_id",
        "blocks",
    }
    assert modal.keys() == expected_view_keys


def test_schedule_modal_has_correct_heading(modal):
    assert modal["title"]["text"] == "Schedule a status"


def test_schedule_modal_has_create_button(modal):
    assert modal["submit"]["text"] == "Create"


def test_schedule_modal_has_cancel_button(modal):
    assert modal["close"]["text"] == "Cancel"


def test_schedule_modal_includes_timezone_in_metadata(no_emojis_modal):
    expected_metadata = '{"timezone": "ContinentA/CityB"}'

    assert no_emojis_modal["private_metadata"] == expected_metadata


def test_schedule_modal_includes_emojis_in_metadata(modal):
    expected_metadata = (
        '{"timezone": "ContinentA/CityB", "emojis": [":pizza:", ":burrito:"]}'
    )

    assert modal["private_metadata"] == expected_metadata


def test_schedule_modal_has_expected_callback_id(modal):
    assert modal["callback_id"] == "schedule-status-submit"


def test_schedule_modal_has_expected_block_ids(modal):
    blocks = modal["blocks"]

    assert blocks[0]["block_id"] == "status-text"
    assert blocks[1]["block_id"] == "emojis"
    assert blocks[2]["block_id"] == "overwrite"
    assert blocks[3]["block_id"] == "start-date"
    assert blocks[4]["block_id"] == "start-time"
    assert blocks[5]["block_id"] == "timezone"
    assert blocks[6]["block_id"] == "end-date"
    assert blocks[7]["block_id"] == "end-time"


def test_schedule_modal_has_expected_action_ids(modal):
    blocks = modal["blocks"]

    assert blocks[0]["element"]["action_id"] == "input-text"
    assert blocks[2]["elements"][0]["action_id"] == "toggle-overwrite"
    assert blocks[3]["element"]["action_id"] == "select-start-date"
    assert blocks[4]["element"]["action_id"] == "select-start-time"
    assert blocks[6]["element"]["action_id"] == "select-end-date"
    assert blocks[7]["element"]["action_id"] == "select-end-time"


def test_schedule_modal_status_text_length_requirements(modal):
    assert modal["blocks"][0]["element"]["min_length"] == 1
    assert modal["blocks"][0]["element"]["max_length"] == 100


def test_schedule_modal_emojis_has_expected_text_without_emojis(
    no_emojis_modal,
):
    expected = "*Emojis* (none selected, using default)\n\n:speech_balloon:"

    assert no_emojis_modal["blocks"][1]["text"]["text"] == expected


def test_schedule_modal_emojis_has_expected_text_with_emojis(modal):
    expected = "*Emojis*\n\n:pizza: :burrito:"

    assert modal["blocks"][1]["text"]["text"] == expected


def test_schedule_modal_should_overwrite_is_preselected(modal):
    options = modal["blocks"][2]["elements"][0]["initial_options"]
    initial = modal["blocks"][2]["elements"][0]["initial_options"]

    assert options == initial


def test_schedule_modal_start_date_has_today_placeholder(modal):
    start_date_element = modal["blocks"][3]["element"]
    assert start_date_element["initial_date"] == "2021-07-30"


def test_schedule_modal_start_time_has_noon_preselected(modal):
    start_time_element = modal["blocks"][4]["element"]
    assert start_time_element["initial_option"]["value"] == "12"


def test_schedule_modal_timezone_block(modal):
    timezone_str = "Your timezone is ContinentA/CityB"
    assert modal["blocks"][5]["elements"][0]["text"] == timezone_str


def test_schedule_modal_end_date_has_today_placeholder(modal):
    start_date_element = modal["blocks"][6]["element"]
    assert start_date_element["initial_date"] == "2021-07-30"


def test_schedule_modal_end_time_has_one_pm_preselected(modal):
    start_time_element = modal["blocks"][7]["element"]
    assert start_time_element["initial_option"]["value"] == "13"
