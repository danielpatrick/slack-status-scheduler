import pytest

from domain.slack.forms import daily_status_form


@pytest.fixture
def modal():
    yield daily_status_form("ContinentA/CityB", [":pizza:", ":burrito:"])


@pytest.fixture
def no_emojis_modal():
    yield daily_status_form("ContinentA/CityB")


def test_daily_modal_has_top_level_keys(modal):
    expected_view_keys = {
        "type",
        "title",
        "submit",
        "close",
        "private_metadata",
        "callback_id",
        "blocks",
    }

    assert modal.keys() == expected_view_keys


def test_daily_modal_has_correct_heading(modal):
    assert modal["title"]["text"] == "Schedule a daily status"


def test_daily_modal_has_create_button(modal):
    assert modal["submit"]["text"] == "Create"


def test_daily_modal_has_cancel_button(modal):
    assert modal["close"]["text"] == "Cancel"


def test_daily_modal_includes_timezone_in_metadata(no_emojis_modal):
    expected_metadata = '{"timezone": "ContinentA/CityB"}'

    assert no_emojis_modal["private_metadata"] == expected_metadata


def test_daily_modal_includes_emojis_in_metadata(modal):
    expected_metadata = (
        '{"timezone": "ContinentA/CityB", "emojis": [":pizza:", ":burrito:"]}'
    )

    assert modal["private_metadata"] == expected_metadata


def test_daily_modal_has_expected_callback_id(modal):
    assert modal["callback_id"] == "daily-status-submit"


def test_daily_modal_has_expected_block_ids(modal):
    blocks = modal["blocks"]

    assert blocks[0]["block_id"] == "status-text"
    assert blocks[1]["block_id"] == "emojis"
    assert blocks[2]["block_id"] == "overwrite"
    assert blocks[3]["block_id"] == "start-time"
    assert blocks[4]["block_id"] == "timezone"
    assert blocks[5]["block_id"] == "duration"
    assert blocks[6]["block_id"] == "days-of-week"


def test_daily_modal_has_expected_action_ids(modal):
    blocks = modal["blocks"]

    assert blocks[0]["element"]["action_id"] == "input-text"
    assert blocks[2]["elements"][0]["action_id"] == "toggle-overwrite"
    assert blocks[3]["element"]["action_id"] == "select-start-time"
    assert blocks[5]["element"]["action_id"] == "select-duration"
    assert blocks[6]["element"]["action_id"] == "select-days"


def test_daily_modal_status_text_length_requirements(modal):
    assert modal["blocks"][0]["element"]["min_length"] == 1
    assert modal["blocks"][0]["element"]["max_length"] == 100


def test_daily_modal_emojis_has_expected_text_without_emojis(no_emojis_modal):
    expected = "*Emojis* (none selected, using default)\n\n:speech_balloon:"

    assert no_emojis_modal["blocks"][1]["text"]["text"] == expected


def test_daily_modal_emojis_has_expected_text_with_emojis(modal):
    expected = "*Emojis*\n\n:pizza: :burrito:"

    assert modal["blocks"][1]["text"]["text"] == expected


def test_daily_modal_should_overwrite_is_pre_selected(modal):
    options = modal["blocks"][2]["elements"][0]["initial_options"]
    initial = modal["blocks"][2]["elements"][0]["initial_options"]

    assert options == initial


def test_daily_modal_start_time_has_noon_preselected(modal):
    start_time_element = modal["blocks"][3]["element"]
    assert start_time_element["initial_option"]["value"] == "12"


def test_daily_modal_timezone_block(modal):
    timezone_str = "Your timezone is ContinentA/CityB"
    assert modal["blocks"][4]["elements"][0]["text"] == timezone_str


def test_daily_modal_duration_has_one_hour_preselected(modal):
    duration_element = modal["blocks"][5]["element"]
    assert duration_element["initial_option"]["value"] == "1"


def test_daily_modal_day_checkboxes_are_pre_selected(modal):
    assert len(modal["blocks"][6]["element"]["initial_options"]) == 5
