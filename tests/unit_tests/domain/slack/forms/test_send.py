from unittest.mock import MagicMock, patch

import pytest
from slack_sdk.errors import SlackApiError

from domain.db.users import BotInfo, RecordNotFoundError, UserInfo
from domain.exceptions import MaxSchedulesError
from domain.slack.exceptions import NotAuthenticatedError
from domain.slack.forms.send import send_form


@pytest.fixture(autouse=True)
def get_user_info():
    with patch("domain.slack.forms.send.get_user_info", autospec=True) as mock:
        mock.return_value = ["test-token", "ContinentA/CityB", 4]
        yield mock


@pytest.fixture(autouse=True)
def WebClient():
    with patch("domain.slack.forms.send.WebClient", autospec=True) as mock:
        yield mock


@pytest.fixture
def web_client(WebClient):
    yield WebClient.return_value


@pytest.fixture
def create_form():
    mock = MagicMock()
    mock.return_value = "DUMMY MODAL"

    yield mock


def sent_modal(client, **kwargs):
    send_form(
        user_id="U1234", team_id="T1234", trigger_id="trigger123", **kwargs
    )

    return client.views_open.call_args.kwargs["view"]


@pytest.fixture
def modal(web_client):
    yield sent_modal(web_client)


def test_send_form_sends_modal_to_slack(
    create_form, get_user_info, WebClient, web_client
):
    expected_kwargs = {"trigger_id": "trigger123", "view": "DUMMY MODAL"}

    send_form(
        create_form, user_id="U1234", team_id="T1234", trigger_id="trigger123"
    )

    WebClient.assert_called_once_with(token="test-token")

    web_client.views_open.assert_called_once_with(**expected_kwargs)


def test_send_form_gets_token_tz_and_num_schedules(create_form, get_user_info):
    send_form(
        create_form, user_id="U1234", team_id="T1234", trigger_id="TRIGGER-ID"
    )

    get_user_info.assert_called_once_with(
        "T1234",
        "U1234",
        BotInfo.BOT_TOKEN,
        UserInfo.USER_TIMEZONE,
        UserInfo.NUM_SCHEDULES,
    )


def test_send_form_passes_tz_and_emojis_none_to_create_form(
    create_form, get_user_info
):
    send_form(
        create_form, user_id="U1234", team_id="T1234", trigger_id="TRIGGER-ID"
    )

    create_form.assert_called_once_with("ContinentA/CityB", None)


def test_send_form_passes_tz_and_emojis_to_create_form(
    create_form, get_user_info
):
    send_form(
        create_form,
        user_id="U1234",
        team_id="T1234",
        trigger_id="TRIGGER-ID",
        status_emojis=[":pizza:", ":burrito:"],
    )

    create_form.assert_called_once_with(
        "ContinentA/CityB",
        [":pizza:", ":burrito:"],
    )


def test_send_form_raises_on_max_schedules(create_form, get_user_info):
    expected_err_msg = (
        "User is trying to create more than the allowed number of schedules"
    )
    get_user_info.return_value = ["test-token", "ContinentA/CityB", 5]

    with pytest.raises(MaxSchedulesError, match=expected_err_msg):
        send_form(
            create_form,
            user_id="U1234",
            team_id="T1234",
            trigger_id="trigger123",
        )


def test_send_form_raises_on_failure(
    create_form, get_user_info, WebClient, web_client
):
    web_client.views_open.side_effect = SlackApiError("Nope", {"ok": False})

    with pytest.raises(SlackApiError, match="Nope"):
        send_form(
            create_form,
            user_id="U1234",
            team_id="T1234",
            trigger_id="trigger123",
        )


def test_send_form_raises_if_no_token(
    create_form, get_user_info, WebClient, web_client
):
    expected_err_msg = "Unable to retreive Slack bot token"
    get_user_info.return_value = [None, "Europe/London", 4]

    with pytest.raises(Exception, match=expected_err_msg):
        send_form(
            create_form,
            user_id="U1234",
            team_id="T1234",
            trigger_id="trigger123",
        )

    WebClient.assert_not_called()
    web_client.assert_not_called()


def test_send_form_raises_if_no_timezone(
    create_form, get_user_info, WebClient, web_client
):
    expected_err_msg = "Unable to retreive Slack user's timezone"
    get_user_info.return_value = ["bot-token", None, 4]

    with pytest.raises(Exception, match=expected_err_msg):
        send_form(
            create_form,
            user_id="U1234",
            team_id="T1234",
            trigger_id="trigger123",
        )

    WebClient.assert_not_called()
    web_client.assert_not_called()


def test_send_form_raises_not_authorized(
    create_form, get_user_info, WebClient, web_client
):
    expected_err_msg = (
        "Cannot create schedules until user has authorized the app"
    )
    get_user_info.side_effect = RecordNotFoundError()

    with pytest.raises(NotAuthenticatedError, match=expected_err_msg):
        send_form(
            create_form,
            user_id="U1234",
            team_id="T1234",
            trigger_id="trigger123",
        )

    WebClient.assert_not_called()
    web_client.assert_not_called()
