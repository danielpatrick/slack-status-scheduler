from unittest.mock import ANY, patch

import pytest
from slack_sdk.errors import SlackApiError

from domain.db.users import BotInfo, RecordNotFoundError, UserInfo
from domain.schedules.status_schedule_rule import StatusScheduleRule
from domain.slack.home_tab import update_home_tab


@pytest.fixture(autouse=True)
def get_user_info():
    with patch("domain.slack.home_tab.get_user_info", autospec=True) as mock:
        mock.side_effect = ["bot-token", "user-token"]

        yield mock


@pytest.fixture(autouse=True)
def get_user_schedules():
    with patch(
        "domain.slack.home_tab.get_user_schedules", autospec=True
    ) as mock:
        mock.return_value = {}

        yield mock


@pytest.fixture
def get_user_schedules_two_schedules(get_user_schedules):
    get_user_schedules.return_value = {
        "schedule1": StatusScheduleRule(
            "2021-05-27T11",
            "Europe/London",
            1,
            [0, 1, 2, 3, 4],
            "Out for lunch",
            [":sandwich:", ":pizza:"],
            True,
        ),
        "schedule2": StatusScheduleRule(
            "2021-05-28T16", "Europe/London", 3, [], "Drinks", [], False
        ),
    }

    yield get_user_schedules


@pytest.fixture(autouse=True)
def WebClient():
    with patch("domain.slack.home_tab.WebClient", autospec=True) as mock:
        yield mock


@pytest.fixture
def web_client(WebClient):
    yield WebClient.return_value


def sent_modal(client):
    update_home_tab(user_id="U1234", team_id="T1234")

    return client.views_publish.call_args.kwargs["view"]


@pytest.fixture
def no_schedules_modal(web_client):
    yield sent_modal(web_client)


@pytest.fixture
def two_schedules_modal(get_user_schedules_two_schedules, web_client):
    yield sent_modal(web_client)


def schedules_return_value(length):
    return {
        f"schedule{n}": StatusScheduleRule(
            f"2021-05-2{n + 1}T16",
            "Europe/London",
            5,
            [n],
            "Drinks",
            [":beer:"],
            n % 2 == 0,
        )
        for n in range(length)
    }


def test_update_home_tab_gets_tokens(get_user_info):
    update_home_tab(user_id="U1234", team_id="T1234")

    assert get_user_info.call_count == 2
    get_user_info.assert_any_call("T1234", "U1234", BotInfo.BOT_TOKEN)
    get_user_info.assert_called_with("T1234", "U1234", UserInfo.USER_TOKEN)


def test_update_home_tab_fetches_user_schedules(
    get_user_schedules_two_schedules, web_client
):
    update_home_tab(user_id="U1234", team_id="T1234")

    get_user_schedules_two_schedules.assert_called_once_with(
        team_id="T1234", user_id="U1234"
    )


def test_update_home_tab_sends_modal_to_slack(
    get_user_schedules, WebClient, web_client
):
    expected_kwargs = {
        "user_id": "U1234",
        "view": {"type": "home", "blocks": ANY},
    }

    update_home_tab(user_id="U1234", team_id="T1234")

    WebClient.assert_called_once_with(token="bot-token")

    assert web_client.views_publish.call_count == 1
    web_client.views_publish.assert_called_once_with(**expected_kwargs)


def test_update_home_tab_raises_if_no_bot_token(
    get_user_info, WebClient, web_client
):
    get_user_info.side_effect = [None, "user-token"]

    with pytest.raises(Exception, match="Unable to retreive Slack bot token"):
        update_home_tab(user_id="U1234", team_id="T1234")

    WebClient.assert_not_called()
    web_client.assert_not_called()


def test_update_home_tab_returns_install_instructions_if_no_user_token(
    get_user_info, web_client
):
    get_user_info.side_effect = ["bot-token", RecordNotFoundError()]

    expected_modal = {
        "type": "home",
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": (
                        "For information about the app and how to use it, "
                        "see the <https://www.example.com|home page>.\n\n"
                        "To get started with the app, please "
                        "<https://www.example.com/install/|authorize> it."
                    ),
                },
            },
        ],
    }

    assert sent_modal(web_client) == expected_modal


def test_update_home_tab_raises_on_failure(WebClient, web_client):
    web_client.views_publish.side_effect = SlackApiError("Nope", {"ok": False})

    with pytest.raises(SlackApiError, match="Nope"):
        update_home_tab(user_id="U1234", team_id="T1234")


def test_modal_has_correct_type(no_schedules_modal):
    assert no_schedules_modal["type"] == "home"


def test_modal_instructions(get_user_info, get_user_schedules, web_client):
    for n in [0, 1, 2, 3]:
        get_user_info.side_effect = ["bot-token", "user-token"]
        get_user_schedules.return_value = schedules_return_value(n)

        expected_instructions = (
            f"You can add up to {5 - n} more scheduled statuses. Create a new "
            "one-off scheduled status using the `/schedule-status` command or "
            "a daily scheduled status using the `/daily-status` command.\n\n"
            "Note that one-off schedules due to trigger within the next 15 "
            "minutes may not appear in this list."
        )

        block_one = sent_modal(web_client)["blocks"][0]

        assert block_one["type"] == "section"
        assert block_one["text"]["type"] == "mrkdwn"
        assert block_one["text"]["text"] == expected_instructions


def test_modal_instructions_four_schedules(get_user_schedules, web_client):
    get_user_schedules.return_value = schedules_return_value(4)

    expected_instructions = (
        "You can add up to 1 more scheduled status. Create a new "
        "one-off scheduled status using the `/schedule-status` command or "
        "a daily scheduled status using the `/daily-status` command.\n\n"
        "Note that one-off schedules due to trigger within the next 15 "
        "minutes may not appear in this list."
    )

    block_one = sent_modal(web_client)["blocks"][0]

    assert block_one["type"] == "section"
    assert block_one["text"]["type"] == "mrkdwn"
    assert block_one["text"]["text"] == expected_instructions


def test_modal_instructions_five_schedules(get_user_schedules, web_client):
    get_user_schedules.return_value = schedules_return_value(5)

    expected_instructions = (
        "You have reached the maximum number of scheduled statuses (5) and "
        "will have to delete one or more before creating another one.\n\n"
        "Note that one-off schedules due to trigger within the next 15 "
        "minutes may not appear in this list."
    )

    block_one = sent_modal(web_client)["blocks"][0]

    assert block_one["type"] == "section"
    assert block_one["text"]["type"] == "mrkdwn"
    assert block_one["text"]["text"] == expected_instructions


def test_modal_blocks_len_if_no_schedules(no_schedules_modal):
    assert len(no_schedules_modal["blocks"]) == 1


def test_modal_has_schedules_heading_if_schedules(two_schedules_modal):
    heading = two_schedules_modal["blocks"][1]

    assert heading["type"] == "header"
    assert heading["text"]["type"] == "plain_text"
    assert heading["text"]["text"] == "Your current schedules"


def test_modal_has_divider_per_schedule(two_schedules_modal):
    assert two_schedules_modal["blocks"][2]["type"] == "divider"
    assert two_schedules_modal["blocks"][5]["type"] == "divider"


def test_modal_has_schedules_if_schedules(two_schedules_modal):
    text1 = (
        "*Out for lunch* :sandwich: :pizza:\n"
        "12:00 noon (Europe/London) for 1 hour\n"
        "Every Monday, Tuesday, Wednesday, Thursday and Friday\n"
        "_This status will overwrite any existing status_"
    )
    text2 = (
        "*Drinks*\nFri 28 May 2021 at 5:00 pm (Europe/London)\n"
        "To Fri 28 May 2021 at 8:00 pm"
    )

    schedule1 = two_schedules_modal["blocks"][3]
    schedule2 = two_schedules_modal["blocks"][6]

    assert schedule1["type"] == "section"
    assert schedule1["text"]["type"] == "mrkdwn"
    assert schedule1["text"]["text"] == text1

    assert schedule2["type"] == "section"
    assert schedule2["text"]["type"] == "mrkdwn"
    assert schedule2["text"]["text"] == text2


def test_modal_has_actions_blocks_for_schedules(two_schedules_modal):
    delete_button = {
        "type": "button",
        "text": {
            "type": "plain_text",
            "text": "Delete schedule",
        },
        "style": "danger",
        "value": "delete_schedule",
    }
    actions1 = two_schedules_modal["blocks"][4]
    actions2 = two_schedules_modal["blocks"][7]

    assert actions1["type"] == "actions"
    assert len(actions1["elements"]) == 1
    assert actions1["elements"][0] == delete_button
    assert actions1["block_id"] == "schedule1"

    assert actions2["type"] == "actions"
    assert len(actions2["elements"]) == 1
    assert actions2["elements"][0] == delete_button
    assert actions2["block_id"] == "schedule2"


def test_modal_blocks_len_for_two_schedules(two_schedules_modal):
    assert len(two_schedules_modal["blocks"]) == 8


def test_modal_uses_noon_and_midnight_for_12_oclock(
    get_user_schedules, web_client
):
    get_user_schedules.return_value = {
        "schedule1": StatusScheduleRule(
            "2021-05-27T11",
            "Europe/London",
            1,
            [0, 1, 2, 3, 4],
            "Out for lunch",
            [":sandwich:"],
            True,
        ),
        "schedule2": StatusScheduleRule(
            "2021-05-28T23",
            "Europe/London",
            8,
            [0, 1, 2, 3, 4, 5, 6],
            "Sleep",
            [":zzz:"],
            True,
        ),
    }
    modal = sent_modal(web_client)

    schedule1 = modal["blocks"][3]
    schedule2 = modal["blocks"][6]

    assert "12:00 noon" in schedule1["text"]["text"]

    assert "12:00 midnight" in schedule2["text"]["text"]
