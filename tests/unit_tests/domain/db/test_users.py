from unittest.mock import Mock, patch

import pendulum
import pytest
from botocore.stub import Stubber

from domain.db.users import (
    BotInfo,
    RecordNotFoundError,
    UserInfo,
    add_user,
    delete_user_or_team,
    dynamodb_client,
    get_user_info,
    set_user_timezone,
)


@pytest.fixture(autouse=True)
def dynamodb_stub():
    with Stubber(dynamodb_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


@pytest.fixture(autouse=True)
def mock_pendulum():
    with patch("domain.db.users.pendulum", Mock(wraps=pendulum)) as p:
        yield p


def test_get_user_info_errors_on_invalid_args():
    expected_msg = "Must supply at least one type of user info to be retrieved"

    with pytest.raises(TypeError, match=expected_msg):
        get_user_info("team123", "user456")


def test_get_bot_token(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "team123"},
                    "BotToken": {"S": "test-bot-token"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "team123"}}
                ],
                "ProjectionExpression": "TeamId, EntityId, BotToken",
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    bot_token = get_user_info("team123", "user456", BotInfo.BOT_TOKEN)

    assert bot_token == "test-bot-token"


def test_get_user_token(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "UserToken": {"S": "test-user-token"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": "TeamId, EntityId, UserToken",
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    user_token = get_user_info("team123", "user456", UserInfo.USER_TOKEN)

    assert user_token == "test-user-token"


def test_get_user_timezone(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "UserTimezone": {"S": "test-user-timezone"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}}
                ],
                "ProjectionExpression": "TeamId, EntityId, UserTimezone",
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    timezone = get_user_info("team123", "user456", UserInfo.USER_TIMEZONE)

    assert timezone == "test-user-timezone"


def test_get_user_num_schedules(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "ScheduleCount": {"N": "5"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}}
                ],
                "ProjectionExpression": "TeamId, EntityId, ScheduleCount",
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    num_schedules = get_user_info("team123", "user456", UserInfo.NUM_SCHEDULES)

    assert num_schedules == 5


def test_get_multiple_user_and_bot_info(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "team123"},
                    "BotToken": {"S": "test-bot-token"},
                },
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "UserToken": {"S": "test-user-token"},
                    "UserTimezone": {"S": "test-user-timezone"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "team123"}},
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": (
                    "TeamId, EntityId, UserTimezone, UserToken, BotToken"
                ),
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    timezone, user_token, bot_token = get_user_info(
        "team123",
        "user456",
        UserInfo.USER_TIMEZONE,
        UserInfo.USER_TOKEN,
        BotInfo.BOT_TOKEN,
    )

    assert timezone == "test-user-timezone"
    assert user_token == "test-user-token"
    assert bot_token == "test-bot-token"


def test_get_user_info_handles_missing_value(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "UserToken": {"S": "test-user-token"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": ("TeamId, EntityId, UserTimezone"),
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    timezone = get_user_info("team123", "user456", UserInfo.USER_TIMEZONE)

    assert timezone is None


def test_get_user_info_handles_multiple_missing_values(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "team123"},
                },
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "team123"}},
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": (
                    "TeamId, EntityId, UserToken, BotToken"
                ),
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    user_token, bot_token = get_user_info(
        "team123", "user456", UserInfo.USER_TOKEN, BotInfo.BOT_TOKEN
    )

    assert user_token is None
    assert bot_token is None


def test_get_user_info_handles_partial_missing_values(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "user456"},
                    "UserToken": {"S": "test-user-token"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": (
                    "TeamId, EntityId, UserToken, UserTimezone"
                ),
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    user_token, timezone = get_user_info(
        "team123", "user456", UserInfo.USER_TOKEN, UserInfo.USER_TIMEZONE
    )

    assert user_token == "test-user-token"
    assert timezone is None


def test_get_user_info_errors_on_missing_record(dynamodb_stub):
    service_response = {
        "Responses": {
            "test-user-table": [
                {
                    "TeamId": {"S": "team123"},
                    "EntityId": {"S": "team123"},
                },
            ]
        }
    }
    expected_params = {
        "RequestItems": {
            "test-user-table": {
                "Keys": [
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "team123"}},
                    {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
                ],
                "ProjectionExpression": (
                    "TeamId, EntityId, UserToken, BotToken"
                ),
            }
        }
    }
    dynamodb_stub.add_response(
        "batch_get_item",
        service_response=service_response,
        expected_params=expected_params,
    )

    expected_msg = "At least one requested record does not exist"

    with pytest.raises(RecordNotFoundError, match=expected_msg):
        get_user_info(
            "team123", "user456", UserInfo.USER_TOKEN, BotInfo.BOT_TOKEN
        )


def test_save_user(dynamodb_stub, mock_pendulum):
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 24, 12, tz="UTC"
    )
    user_update = {
        "TableName": "test-user-table",
        "Key": {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
        "UpdateExpression": (
            "SET #UserScope = :UserScope, #UserToken = :UserToken, "
            "#UpdatedTime = :UpdatedTime, "
            "#ScheduleCount = if_not_exists(#ScheduleCount, :Zero)"
        ),
        "ExpressionAttributeNames": {
            "#UserScope": "UserScope",
            "#UserToken": "UserToken",
            "#UpdatedTime": "UpdatedTime",
            "#ScheduleCount": "ScheduleCount",
        },
        "ExpressionAttributeValues": {
            ":UserScope": {"S": "user-scope"},
            ":UserToken": {"S": "user-token"},
            ":UpdatedTime": {"S": "1621857600"},
            ":Zero": {"N": "0"},
        },
    }

    team_update = {
        "TableName": "test-user-table",
        "Key": {"TeamId": {"S": "team123"}, "EntityId": {"S": "team123"}},
        "UpdateExpression": (
            "SET #TeamName = :TeamName, #BotId = :BotId, "
            "#BotScope = :BotScope, #BotToken = :BotToken, "
            "#UpdatedTime = :UpdatedTime"
        ),
        "ExpressionAttributeNames": {
            "#TeamName": "TeamName",
            "#BotId": "BotId",
            "#BotScope": "BotScope",
            "#BotToken": "BotToken",
            "#UpdatedTime": "UpdatedTime",
        },
        "ExpressionAttributeValues": {
            ":TeamName": {"S": "team-name"},
            ":BotId": {"S": "bot-id"},
            ":BotScope": {"S": "bot-scope"},
            ":BotToken": {"S": "bot-token"},
            ":UpdatedTime": {"S": "1621857600"},
        },
    }
    expected_params = {
        "TransactItems": [{"Update": team_update}, {"Update": user_update}],
    }
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params,
    )

    add_user(
        user_id="user456",
        user_scope="user-scope",
        user_token="user-token",
        team_id="team123",
        team_name="team-name",
        bot_id="bot-id",
        bot_scope="bot-scope",
        bot_token="bot-token",
    )


def test_set_user_timezone(dynamodb_stub):
    expected_params = {
        "TableName": "test-user-table",
        "Key": {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
        "UpdateExpression": "SET UserTimezone = :timezone",
        "ExpressionAttributeValues": {
            ":timezone": {"S": "Europe/London"},
        },
    }

    dynamodb_stub.add_response(
        "update_item",
        service_response={},
        expected_params=expected_params,
    )

    set_user_timezone(
        user_id="user456",
        team_id="team123",
        timezone="Europe/London",
    )


def test_delete_user_or_team(dynamodb_stub):
    expected_params = {
        "TableName": "test-user-table",
        "Key": {"TeamId": {"S": "team123"}, "EntityId": {"S": "user456"}},
    }

    dynamodb_stub.add_response(
        "delete_item",
        service_response={},
        expected_params=expected_params,
    )

    delete_user_or_team(team_id="team123", entity_id="user456")
