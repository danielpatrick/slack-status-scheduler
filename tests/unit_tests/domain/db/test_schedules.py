from unittest.mock import Mock, patch

import pendulum
import pytest
from botocore.stub import Stubber

from domain.db.schedules import (
    DatePastError,
    InvalidEndDateError,
    InvalidEndTimeError,
    TimePastError,
    create_daily_schedule,
    create_one_off_schedule,
    delete_schedule,
    dynamodb_client,
    get_user_schedule_ids,
    get_user_schedules,
)
from domain.exceptions import MaxSchedulesError
from domain.schedules import StatusScheduleRule
from unit_tests.factories import schedule_dynamodb_image


@pytest.fixture(autouse=True)
def dynamodb_stub():
    with Stubber(dynamodb_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


@pytest.fixture(autouse=True)
def mock_pendulum():
    with patch("domain.db.schedules.pendulum", Mock(wraps=pendulum)) as p:
        p.now.return_value = pendulum.datetime(
            2021, 5, 16, 12, tz="Europe/London"
        )
        p.today.return_value = pendulum.datetime(
            2021, 5, 16, tz="Europe/London"
        )

        yield p


@pytest.fixture(autouse=True)
def uuid4():
    with patch("domain.db.schedules.uuid4", autospec=True) as mock:
        mock.return_value = "some-uuid"
        yield mock


def expected_params(**kwargs):
    overwrite = kwargs.get("overwrite", True)
    next_event_begin_hour = kwargs.get(
        "next_event_begin_hour", "2021-05-16T15"
    )
    hour = kwargs.get("hour", 16)
    duration = kwargs.get("duration", 1)
    days = kwargs.get("days", ["0", "1", "2", "3", "4", "5", "6"])

    params = {
        "TransactItems": [
            {
                "Put": {
                    "TableName": "test-schedule-table",
                    "Item": {
                        "TeamId": {"S": "T1234"},
                        "UserIdScheduleId": {"S": "U1234#some-uuid"},
                        "CreatedBy": {"S": "USER"},
                        "Overwrite": {"BOOL": overwrite},
                        "NextEventBeginHour": {"S": next_event_begin_hour},
                        "StatusText": {"S": "status text"},
                        "StatusEmojis": {"SS": [":joy:", ":grimacing:"]},
                        "UserId": {"S": "U1234"},
                        "ScheduleId": {"S": "some-uuid"},
                        "Timezone": {"S": "Europe/London"},
                        "Hour": {"N": str(hour)},
                        "Duration": {"N": str(duration)},
                    },
                },
            },
            {
                "Update": {
                    "TableName": "test-user-table",
                    "Key": {
                        "TeamId": {"S": "T1234"},
                        "EntityId": {"S": "U1234"},
                    },
                    "UpdateExpression": (
                        "SET ScheduleCount = ScheduleCount + :One"
                    ),
                    "ConditionExpression": "ScheduleCount < :MaxSchedules",
                    "ExpressionAttributeValues": {
                        ":One": {"N": "1"},
                        ":MaxSchedules": {"N": "5"},
                    },
                },
            },
        ]
    }

    if days is not None:
        params["TransactItems"][0]["Put"]["Item"]["Days"] = {"NS": days}

    return params


def test_create_daily_schedule_calls_dynamodb(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(),
    )

    create_daily_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        hour=16,
        duration=1,
        overwrite=True,
        days=[0, 1, 2, 3, 4, 5, 6],
    )


def test_create_daily_schedule_with_overwrite_false(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(overwrite=False),
    )

    create_daily_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        hour=16,
        duration=1,
        overwrite=False,
        days=[0, 1, 2, 3, 4, 5, 6],
    )


def test_create_daily_schedule_uses_future_date_if_today_time_passed(
    dynamodb_stub, mock_pendulum
):
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 16, 20, tz="Europe/London"
    )
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(next_event_begin_hour="2021-05-17T15"),
    )

    create_daily_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        hour=16,
        duration=1,
        overwrite=True,
        days=[0, 1, 2, 3, 4, 5, 6],
    )


def test_create_daily_schedule_skips_unselected_days(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(
            next_event_begin_hour="2021-05-19T15",
            days=["2", "3", "4", "5"],
        ),
    )

    create_daily_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        hour=16,
        duration=1,
        overwrite=True,
        days=[2, 3, 4, 5],
    )


def test_create_daily_schedule_rejects_if_invalid_duration():
    expected_err_msg = (
        "Daily status duration must be between 1 and 24 hours inclusive"
    )

    with pytest.raises(Exception, match=expected_err_msg):
        create_daily_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            hour=8,
            duration=0,
            overwrite=True,
            days=[0],
        )

    with pytest.raises(Exception, match=expected_err_msg):
        create_daily_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            hour=8,
            duration=25,
            overwrite=True,
            days=[0],
        )


def test_create_daily_schedule_rejects_if_no_days():
    expected_err_msg = "One or more days required for daily scheduled status"

    with pytest.raises(Exception, match=expected_err_msg):
        create_daily_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            hour=8,
            duration=1,
            overwrite=True,
            days=[],
        )


def test_create_daily_schedule_rejects_if_max_schedules(dynamodb_stub):
    dynamodb_stub.add_client_error(
        "transact_write_items",
        service_error_code="TransactionCanceledException",
        service_message=(
            "Transaction cancelled, please refer cancellation reasons "
            "for specific reasons [None, ConditionalCheckFailed]"
        ),
        expected_params=expected_params(),
    )
    expected_err_msg = (
        "User is trying to create more than the allowed number of schedules"
    )

    with pytest.raises(MaxSchedulesError, match=expected_err_msg):
        create_daily_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            hour=16,
            duration=1,
            overwrite=True,
            days=[0, 1, 2, 3, 4, 5, 6],
        )


def test_create_daily_schedule_only_raises_max_schedules_if_condition_fails(
    dynamodb_stub,
):
    dynamodb_stub.add_client_error(
        "transact_write_items",
        service_error_code="TransactionCanceledException",
        service_message="Whoops",
        expected_params=expected_params(),
    )

    with pytest.raises(
        dynamodb_client.exceptions.TransactionCanceledException
    ):
        create_daily_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            hour=16,
            duration=1,
            overwrite=True,
            days=[0, 1, 2, 3, 4, 5, 6],
        )


def test_create_one_off_schedule_calls_dynamodb(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(days=None),
    )

    create_one_off_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        start_date_str="2021-05-16",
        start_hour=16,
        end_date_str="2021-05-16",
        end_hour=17,
        overwrite=True,
    )


def test_create_one_off_schedule_with_timezone_change(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(
            next_event_begin_hour="2021-10-30T11",
            hour=12,
            duration=24,
            days=None,
        ),
    )

    create_one_off_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        start_date_str="2021-10-30",
        start_hour=12,
        end_date_str="2021-10-31",
        end_hour=12,
        overwrite=True,
    )


def test_create_one_off_schedule_with_overwrite_false(dynamodb_stub):
    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params(overwrite=False, days=None),
    )

    create_one_off_schedule(
        team_id="T1234",
        user_id="U1234",
        status_text="status text",
        status_emojis=[":joy:", ":grimacing:"],
        timezone="Europe/London",
        start_date_str="2021-05-16",
        start_hour=16,
        end_date_str="2021-05-16",
        end_hour=17,
        overwrite=False,
    )


def test_create_one_off_schedule_raises_if_date_in_past(
    dynamodb_stub, mock_pendulum
):
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 16, tz="Europe/London"
    )
    expected_err_msg = "Scheduled status must be today or later"

    with pytest.raises(DatePastError, match=expected_err_msg):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-15",
            start_hour=23,
            end_date_str="2021-05-16",
            end_hour=0,
            overwrite=True,
        )


def test_create_one_off_schedule_raises_if_time_too_close(
    dynamodb_stub, mock_pendulum
):
    mock_pendulum.now.return_value = pendulum.datetime(
        2021, 5, 16, 15, 59, 1, tz="Europe/London"
    )
    expected_err_msg = (
        "Scheduled status must be at least one minute in the future"
    )

    with pytest.raises(TimePastError, match=expected_err_msg):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-16",
            start_hour=16,
            end_date_str="2021-05-16",
            end_hour=17,
            overwrite=True,
        )


def test_create_one_off_schedule_raises_if_end_date_before_start_date(
    dynamodb_stub, mock_pendulum
):
    expected_err_msg = "End date cannot be before start date"

    with pytest.raises(InvalidEndDateError, match=expected_err_msg):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-16",
            start_hour=16,
            end_date_str="2021-05-15",
            end_hour=17,
            overwrite=True,
        )


def test_create_one_off_schedule_raises_if_end_time_less_than_one_hour(
    dynamodb_stub, mock_pendulum
):
    expected_err_msg = (
        "End date/time must be at least one hour after start date/time"
    )

    with pytest.raises(InvalidEndTimeError, match=expected_err_msg):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-16",
            start_hour=16,
            end_date_str="2021-05-16",
            end_hour=16,
            overwrite=True,
        )


def test_create_one_off_schedule_rejects_if_max_schedules(dynamodb_stub):
    dynamodb_stub.add_client_error(
        "transact_write_items",
        service_error_code="TransactionCanceledException",
        service_message=(
            "Transaction cancelled, please refer cancellation reasons "
            "for specific reasons [None, ConditionalCheckFailed]"
        ),
        expected_params=expected_params(days=None),
    )
    expected_err_msg = (
        "User is trying to create more than the allowed number of schedules"
    )

    with pytest.raises(MaxSchedulesError, match=expected_err_msg):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-16",
            start_hour=16,
            end_date_str="2021-05-16",
            end_hour=17,
            overwrite=True,
        )


def test_create_one_off_schedule_only_raises_max_schedules_if_condition_fails(
    dynamodb_stub,
):
    dynamodb_stub.add_client_error(
        "transact_write_items",
        service_error_code="TransactionCanceledException",
        service_message="Whoops",
        expected_params=expected_params(days=None),
    )

    with pytest.raises(
        dynamodb_client.exceptions.TransactionCanceledException
    ):
        create_one_off_schedule(
            team_id="T1234",
            user_id="U1234",
            status_text="status text",
            status_emojis=[":joy:", ":grimacing:"],
            timezone="Europe/London",
            start_date_str="2021-05-16",
            start_hour=16,
            end_date_str="2021-05-16",
            end_hour=17,
            overwrite=True,
        )


def test_delete_schedule_with_user_update(dynamodb_stub):
    expected_params = {
        "TransactItems": [
            {
                "Delete": {
                    "TableName": "test-schedule-table",
                    "Key": {
                        "TeamId": {"S": "T1234"},
                        "UserIdScheduleId": {"S": "U1234#schedule1"},
                    },
                },
            },
            {
                "Update": {
                    "TableName": "test-user-table",
                    "Key": {
                        "TeamId": {"S": "T1234"},
                        "EntityId": {"S": "U1234"},
                    },
                    "UpdateExpression": (
                        "SET ScheduleCount = if_not_exists("
                        "ScheduleCount, :Zero) - :One"
                    ),
                    "ExpressionAttributeValues": {
                        ":Zero": {"N": "0"},
                        ":One": {"N": "1"},
                    },
                },
            },
        ]
    }

    dynamodb_stub.add_response(
        "transact_write_items",
        service_response={},
        expected_params=expected_params,
    )

    delete_schedule(team_id="T1234", user_id="U1234", schedule_id="schedule1")


def test_delete_schedule_without_user_update(dynamodb_stub):
    expected_params = {
        "TableName": "test-schedule-table",
        "Key": {
            "TeamId": {"S": "T1234"},
            "UserIdScheduleId": {"S": "U1234#schedule1"},
        },
    }

    dynamodb_stub.add_response(
        "delete_item",
        service_response={},
        expected_params=expected_params,
    )

    delete_schedule(
        team_id="T1234",
        user_id="U1234",
        schedule_id="schedule1",
        update_user=False,
    )


def test_get_user_schedule_ids(dynamodb_stub):
    service_response = {
        "Items": [
            {"ScheduleId": {"S": "schedule1"}},
            {"ScheduleId": {"S": "schedule2"}},
        ]
    }
    expected_params = {
        "TableName": "test-schedule-table",
        "Limit": 10,
        "KeyConditionExpression": (
            "TeamId = :TeamId AND begins_with(UserIdScheduleId, :UserId)"
        ),
        "ExpressionAttributeValues": {
            ":TeamId": {"S": "T1234"},
            ":UserId": {"S": "U1234"},
        },
        "ProjectionExpression": "ScheduleId",
    }

    dynamodb_stub.add_response(
        "query",
        service_response=service_response,
        expected_params=expected_params,
    )

    result = get_user_schedule_ids(team_id="T1234", user_id="U1234")

    assert result == ["schedule1", "schedule2"]


def test_get_status_schedules_gets_from_dynamodb(dynamodb_stub):
    service_response = {
        "Items": [
            schedule_dynamodb_image(
                team_id="T1234",
                user_id="U1234",
                schedule_id="schedule1",
                next_time="2021-05-16T10",
                hour=11,
                timezone="Europe/London",
                overwrite=True,
            ),
            schedule_dynamodb_image(
                team_id="T1234",
                user_id="U1234",
                schedule_id="schedule2",
                next_time="2021-05-16T11",
                hour=12,
                timezone="Europe/London",
                overwrite=False,
            ),
            schedule_dynamodb_image(
                team_id="T1234",
                user_id="U1234",
                schedule_id="schedule3",
                next_time="NO_NEXT_TIME",
                hour=11,
                days=[],
                timezone="Europe/London",
                overwrite=False,
            ),
            schedule_dynamodb_image(
                team_id="T1234",
                user_id="U1234",
                schedule_id="schedule4",
                next_time="2021-05-16T11",
                hour=11,
                duration=48,
                days=[],
                timezone="Europe/London",
                overwrite=False,
            ),
        ]
    }
    del service_response["Items"][0]["StatusEmojis"]["SS"]

    expected_params = {
        "TableName": "test-schedule-table",
        "Limit": 10,
        "KeyConditionExpression": (
            "TeamId = :TeamId AND begins_with(UserIdScheduleId, :UserId)"
        ),
        "ExpressionAttributeValues": {
            ":TeamId": {"S": "T1234"},
            ":UserId": {"S": "U1234"},
        },
    }

    dynamodb_stub.add_response(
        "query",
        service_response=service_response,
        expected_params=expected_params,
    )

    expected_result = {
        "schedule1": StatusScheduleRule(
            "2021-05-16T10",
            "Europe/London",
            1,
            [0, 1, 2, 3, 4, 5, 6],
            "Out for lunch",
            [],
            True,
        ),
        "schedule2": StatusScheduleRule(
            "2021-05-16T11",
            "Europe/London",
            1,
            [0, 1, 2, 3, 4, 5, 6],
            "Out for lunch",
            [":knife_fork_plate:", ":sandwich:"],
            False,
        ),
        "schedule4": StatusScheduleRule(
            "2021-05-16T11",
            "Europe/London",
            48,
            [],
            "Out for lunch",
            [":knife_fork_plate:", ":sandwich:"],
            False,
        ),
    }

    result = get_user_schedules(team_id="T1234", user_id="U1234")

    assert result == expected_result
