import os
from datetime import datetime
from uuid import uuid4

import pytest
from botocore.stub import Stubber

from infrastructure.secretsmanager import client as secretsmanager_client

SECRET = os.environ["SLACK_VALID_SIGNING_SECRET"]


@pytest.fixture(scope="module")
def secretsmanager_stub():
    with Stubber(secretsmanager_client) as stubber:
        secret_string = (
            "{"
            '"AppId": "APP1234", '
            '"ClientId": "test-slack-client-id", '
            '"ClientSecret": "test-slack-client-secret", '
            f'"SigningSecret": "{SECRET}"'
            "}"
        )

        service_response = {
            "ARN": (
                "arn:aws:secretsmanager:eu-west-1:123456789012:"
                "secret:SlackAppCredentials-abc123"
            ),
            "Name": "SlackAppCredentials",
            "VersionId": str(uuid4()),
            "SecretString": secret_string,
            "VersionStages": ["AWSCURRENT"],
            "CreatedDate": datetime(2021, 1, 1),
        }

        stubber.add_response(
            "get_secret_value",
            service_response=service_response,
            expected_params={"SecretId": "SlackAppCredentials"},
        )

        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()
