from datetime import datetime
from uuid import uuid4

import pytest
from botocore.stub import Stubber

from infrastructure.secretsmanager import client, get_secret_values


@pytest.fixture(autouse=True)
def secretsmanager_stub():
    with Stubber(client) as stubber:
        service_response = {
            "ARN": (
                "arn:aws:secretsmanager:eu-west-1:123456789012:"
                "secret:MySecret-abc123"
            ),
            "Name": "MySecret",
            "VersionId": str(uuid4()),
            "SecretString": '{"key1": "valueA", "key2": "valueB"}',
            "VersionStages": ["AWSCURRENT"],
            "CreatedDate": datetime(2021, 1, 1),
        }

        stubber.add_response(
            "get_secret_value",
            service_response=service_response,
            expected_params={"SecretId": "MySecret"},
        )

        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


def test_get_secret_values_one_key():
    assert get_secret_values("MySecret", "key1") == "valueA"


def test_get_secret_values_one_key_unknown():
    assert get_secret_values("MySecret", "key3") is None


def test_get_secret_values_two_keys():
    values = get_secret_values("MySecret", "key1", "key2")

    assert values == ["valueA", "valueB"]


def test_get_secret_values_two_keys_one_unknown():
    values = get_secret_values("MySecret", "key0", "key1")

    assert values == [None, "valueA"]
