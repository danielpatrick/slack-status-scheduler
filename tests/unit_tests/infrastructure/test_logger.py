import json
from unittest.mock import ANY, patch

import pytest

from infrastructure.logger import Logger


def assert_logs_expected(method, mock):
    expected_log = (
        '{"service": "test-service", "source": "test-source", '
        '"message": "This is a test"}'
    )

    method("This is a test")
    mock.assert_called_once_with(expected_log)


def assert_logs_expected_with_data(method, mock):
    expected_log = (
        '{"service": "test-service", "source": "test-source", "message": '
        '"This is also a test", "data": {"some-numbers": [1, 2, 3]}}'
    )

    method("This is also a test", {"some-numbers": [1, 2, 3]})
    mock.assert_called_once_with(expected_log)


def assert_stringifies_data_if_not_valid_json(method, mock):
    class NotJsonDumpable:
        def __str__(self):
            return "NotJsonDumpable object"

    expected_log = (
        '{"service": "test-service", '
        '"source": "test-source", '
        '"message": "This is a test", '
        '"data": {"object": "NotJsonDumpable object"}}'
    )

    method("This is a test", {"object": NotJsonDumpable()})
    mock.assert_called_once_with(expected_log)


def assert_redacts_specified_values(method, mock):
    class NotJsonDumpable:
        def __str__(self):
            return "NotJsonDumpable object"

    data = {
        "nested": {
            "sensitive": [3.14, NotJsonDumpable(), {"test": "hide me"}],
            "okay": 5,
        },
        "Sensitive": {"hello": "world", "num": 123},
        "sensitive": "nope",
    }

    expected_log = (
        '{"service": "test-service", '
        '"source": "test-source", '
        '"message": "This is a test", '
        '"data": {'
        '"nested": {'
        '"sensitive": ['
        '"Redacted float: N.NN", '
        '"Redacted NotJsonDumpable", '
        '{"test": "Redacted str: 7 chars"}], '
        '"okay": 5}, '
        '"Sensitive": {'
        '"hello": "Redacted str: 5 chars", '
        '"num": "Redacted int: NNN"}, '
        '"sensitive": "Redacted str: 4 chars"}}'
    )

    method("This is a test", data)
    mock.assert_called_once_with(expected_log)


def assert_logs_expected_with_data_and_traceback(method, mock):
    expected_json = {
        "service": "test-service",
        "source": "test-source",
        "message": "This is a test",
        "data": {"some-numbers": [1, 2, 3]},
        "traceback": ANY,
    }

    try:
        raise KeyError("Nope")
    except Exception:
        method("This is a test", {"some-numbers": [1, 2, 3]})

    assert mock.call_count == 1
    assert len(mock.call_args.args) == 1
    assert len(mock.call_args.kwargs) == 0

    json_log = json.loads(mock.call_args.args[0])

    assert json_log == expected_json
    assert isinstance(json_log["traceback"], str)
    assert json_log["traceback"].startswith(
        "Traceback (most recent call last):"
    )
    assert "KeyError: 'Nope'" in json_log["traceback"]


def assert_logs_expected_with_data_and_exception(method, mock):
    expected_json = {
        "service": "test-service",
        "source": "test-source",
        "message": "This is a test",
        "data": {"some-numbers": [1, 2, 3]},
        "traceback": ANY,
    }

    try:
        "".missing
    except Exception as e:
        exc = e

    # Create an additional exception to prove the exception param
    # takes precedent over the traceback
    try:
        raise KeyError("Nope")
    except Exception:
        method("This is a test", {"some-numbers": [1, 2, 3]}, exception=exc)

    assert mock.call_count == 1
    assert len(mock.call_args.args) == 1
    assert len(mock.call_args.kwargs) == 0

    json_log = json.loads(mock.call_args.args[0])

    assert json_log == expected_json
    assert isinstance(json_log["traceback"], str)
    assert json_log["traceback"].startswith(
        "Traceback (most recent call last):"
    )
    assert "KeyError: 'Nope'" not in json_log["traceback"]
    assert (
        "AttributeError: 'str' object has no attribute 'missing'"
        in json_log["traceback"]
    )


class TestLogger:
    @pytest.fixture
    def logger(self):
        yield Logger("test-service", "test-source", {"sensitive", "Sensitive"})

    @pytest.fixture
    def mock_logging(self):
        with patch("infrastructure.logger.logging", autospec=True) as mock:
            yield mock

    def test_debug_log(self, logger, mock_logging):
        assert_logs_expected(logger.debug, mock_logging.debug)

    def test_debug_log_with_data(self, logger, mock_logging):
        assert_logs_expected_with_data(logger.debug, mock_logging.debug)

    def test_debug_log_with_invalid_json(self, logger, mock_logging):
        assert_stringifies_data_if_not_valid_json(
            logger.debug, mock_logging.debug
        )

    def test_debug_log_redacted_data(self, logger, mock_logging):
        assert_redacts_specified_values(logger.debug, mock_logging.debug)

    def test_debug_log_inside_except_logs_traceback(
        self, logger, mock_logging
    ):
        assert_logs_expected_with_data_and_traceback(
            logger.debug, mock_logging.debug
        )

    def test_info_log(self, logger, mock_logging):
        assert_logs_expected(logger.info, mock_logging.info)

    def test_info_log_with_data(self, logger, mock_logging):
        assert_logs_expected_with_data(logger.info, mock_logging.info)

    def test_info_log_with_invalid_json(self, logger, mock_logging):
        assert_stringifies_data_if_not_valid_json(
            logger.info, mock_logging.info
        )

    def test_info_log_redacted_data(self, logger, mock_logging):
        assert_redacts_specified_values(logger.info, mock_logging.info)

    def test_info_log_inside_except_logs_traceback(self, logger, mock_logging):
        assert_logs_expected_with_data_and_traceback(
            logger.info, mock_logging.info
        )

    def test_warning_log(self, logger, mock_logging):
        assert_logs_expected(logger.warning, mock_logging.warning)

    def test_warning_log_with_data(self, logger, mock_logging):
        assert_logs_expected_with_data(logger.warning, mock_logging.warning)

    def test_warning_log_with_invalid_json(self, logger, mock_logging):
        assert_stringifies_data_if_not_valid_json(
            logger.warning, mock_logging.warning
        )

    def test_warning_log_redacted_data(self, logger, mock_logging):
        assert_redacts_specified_values(logger.warning, mock_logging.warning)

    def test_warning_log_inside_except_logs_traceback(
        self, logger, mock_logging
    ):
        assert_logs_expected_with_data_and_traceback(
            logger.warning, mock_logging.warning
        )

    def test_warning_log_exception_overrides_traceback(
        self, logger, mock_logging
    ):
        assert_logs_expected_with_data_and_exception(
            logger.warning, mock_logging.warning
        )

    def test_error_log(self, logger, mock_logging):
        assert_logs_expected(logger.error, mock_logging.error)

    def test_error_log_with_data(self, logger, mock_logging):
        assert_logs_expected_with_data(logger.error, mock_logging.error)

    def test_error_log_with_invalid_json(self, logger, mock_logging):
        assert_stringifies_data_if_not_valid_json(
            logger.error, mock_logging.error
        )

    def test_error_log_redacted_data(self, logger, mock_logging):
        assert_redacts_specified_values(logger.error, mock_logging.error)

    def test_error_log_inside_except_logs_traceback(
        self, logger, mock_logging
    ):
        assert_logs_expected_with_data_and_traceback(
            logger.error, mock_logging.error
        )

    def test_error_log_exception_overrides_traceback(
        self, logger, mock_logging
    ):
        assert_logs_expected_with_data_and_exception(
            logger.error, mock_logging.error
        )
