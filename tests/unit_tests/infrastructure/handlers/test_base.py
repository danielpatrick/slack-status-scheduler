import importlib
from unittest.mock import MagicMock

import pytest

from infrastructure.handlers.base import BaseHandler
from infrastructure.logger import Logger


class TestBaseHandler:
    @pytest.fixture
    def logger(self):
        yield MagicMock()

    @pytest.fixture
    def Handler(self, logger):
        class PatchedHandler(BaseHandler):
            def create_logger(self):
                return logger

        yield PatchedHandler

    @pytest.fixture
    def unset_service_source_env(self, monkeypatch):
        monkeypatch.delenv("SERVICE_NAME", raising=False)
        monkeypatch.delenv("SOURCE_NAME", raising=False)

    def test_logger_creation(self):
        handler = BaseHandler(None, None)

        assert handler.logger.service == "test-service"
        assert handler.logger.source == "test-source"
        assert handler.logger.redaction_keys == Logger.redaction_keys

    def test_logger_creation_with_custom_service_and_source(self):
        class Handler(BaseHandler):
            SERVICE = "service"
            SOURCE = "source"

        handler = Handler(None, None)

        assert handler.logger.service == "service"
        assert handler.logger.source == "source"

    def test_logger_service_and_source_default_to_unknown(
        self, unset_service_source_env
    ):
        import infrastructure.handlers.base

        importlib.reload(infrastructure.handlers.base)
        from infrastructure.handlers.base import BaseHandler

        handler = BaseHandler(None, None)

        assert handler.logger.service == "unknown"
        assert handler.logger.source == "unknown"

    def test_logger_creation_with_custom_redaction_keys(self):
        class Handler(BaseHandler):
            LOG_REDACTION_KEYS = {"signature", "Authorization"}

        expected_keys = Logger.redaction_keys | {"signature", "Authorization"}
        handler = Handler(None, None)

        assert handler.logger.redaction_keys == expected_keys

    def test_init_sets_event_and_context(self, Handler):
        h = Handler("event", "context")

        assert h.event == "event"
        assert h.context == "context"

    def test_sequence_of_methods_invoked_and_execute_result_returned(
        self, Handler
    ):
        class TestHandler(Handler):
            def parse_event(self):
                self.parse_event_attribute = (
                    f"e: {self.event} c: {self.context}"
                )

            def set_logger_attrs(self):
                self.set_logger_attrs_attribute = self.parse_event_attribute

            def execute(self):
                return self.set_logger_attrs_attribute

        assert TestHandler.handler("EVENT", "CONTEXT") == "e: EVENT c: CONTEXT"

    def test_error_handling_for_parse_event(self, Handler, logger):
        exception = KeyError("I lost my keys")

        class TestHandler(Handler):
            def parse_event(self):
                raise exception

        with pytest.raises(KeyError, match="I lost my keys"):
            TestHandler.handler("event", "context")

        expected_msg = "TestHandler.parse_event failed"
        assert logger.error.call_count == 2
        logger.error.assert_any_call(expected_msg)
        logger.error.assert_called_with(
            "Lambda invocation failure", exception=exception
        )

        logger.info.assert_called_once_with(
            "Lambda invocation started", {"event": "event"}
        )

    def test_error_handling_for_set_logger_attrs(self, Handler, logger):
        exception = KeyError("I lost my keys")

        class TestHandler(Handler):
            def set_logger_attrs(self):
                raise exception

        TestHandler.handler("event", "context")

        expected_msg = "TestHandler.set_logger_attrs failed"
        logger.warning.assert_called_once_with(
            expected_msg, exception=exception
        )

        logger.info.assert_called_once_with(
            "Lambda invocation started", {"event": "event"}
        )

    def test_error_handling_for_execute(self, Handler, logger):
        exception = KeyError("I lost my keys")

        class TestHandler(Handler):
            def execute(self):
                raise exception

        with pytest.raises(KeyError, match="I lost my keys"):
            TestHandler.handler("event", "context")

        expected_msg = "TestHandler.execute failed"
        assert logger.error.call_count == 2
        logger.error.assert_any_call(expected_msg)
        logger.error.assert_called_with(
            "Lambda invocation failure", exception=exception
        )
