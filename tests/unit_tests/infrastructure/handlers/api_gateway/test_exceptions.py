from infrastructure.handlers.api_gateway.exceptions import (
    HttpBadRequestError,
    HttpErrorBase,
    HttpForbiddenError,
    HttpInternalServerError,
)


class HttpTeapot(HttpErrorBase):
    status_code = 418
    default_body = "I'm a teapot"


class TestExceptions:
    def assert_default_body(self, ExceptionClass, status_code, body):
        e = ExceptionClass("Something went wrong")

        assert e.status_code == status_code
        assert e.body == body
        assert str(e) == "Something went wrong"

    def assert_custom_body(self, ExceptionClass, status_code):
        e = ExceptionClass("Something went wrong", "It broke")

        assert e.status_code == status_code
        assert e.body == "It broke"
        assert str(e) == "Something went wrong"

    def test_custom_error_class(self):
        self.assert_default_body(HttpTeapot, 418, "I'm a teapot")
        self.assert_custom_body(HttpTeapot, 418)

    def test_http_bad_request_error(self):
        self.assert_default_body(HttpBadRequestError, 400, "Bad request")
        self.assert_custom_body(HttpBadRequestError, 400)

    def test_forbidden_error(self):
        self.assert_default_body(HttpForbiddenError, 403, "Forbidden")
        self.assert_custom_body(HttpForbiddenError, 403)

    def test_http_internal_server_error(self):
        self.assert_default_body(
            HttpInternalServerError, 500, "Internal server error"
        )
        self.assert_custom_body(HttpInternalServerError, 500)
