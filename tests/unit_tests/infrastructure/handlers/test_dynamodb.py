import json
from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber

from infrastructure.handlers.dynamodb import DynamodbStreamToSnsHandler
from infrastructure.logger import Logger
from unit_tests.factories import schedule_stream_record


@pytest.fixture(autouse=True)
def logger():
    yield MagicMock(wraps=Logger("SERVICE", "SOURCE"))


@pytest.fixture(autouse=True)
def sns_stub():
    with Stubber(DynamodbStreamToSnsHandler.sns_client) as stubber:
        yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()


@pytest.fixture
def BasicHandler(logger):
    class Handler(DynamodbStreamToSnsHandler):
        TOPIC_ARN = "some-topic-arn"

        def create_logger(self):
            return logger

    yield Handler


def test_handler_sends_insert_record_to_sns(BasicHandler, sns_stub, logger):
    record = schedule_stream_record(event_name="INSERT")

    expected_params = {
        "TopicArn": "some-topic-arn",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "INSERT", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "123"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert BasicHandler.handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_modify_record_to_sns(BasicHandler, sns_stub, logger):
    record = schedule_stream_record(event_name="MODIFY")

    expected_params = {
        "TopicArn": "some-topic-arn",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "MODIFY", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "234"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert BasicHandler.handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_handler_sends_remove_record_to_sns(BasicHandler, sns_stub, logger):
    record = schedule_stream_record(event_name="REMOVE")

    expected_params = {
        "TopicArn": "some-topic-arn",
        "Message": json.dumps(record["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "REMOVE", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "345"},
        expected_params=expected_params,
    )

    event = {"Records": [record]}

    assert BasicHandler.handler(event, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)


def test_filter_prevents_sending_messages(BasicHandler, sns_stub, logger):
    class InsertHandler(BasicHandler):
        def filter(self, record):
            return record["eventName"] == "MODIFY"

    record1 = schedule_stream_record(event_name="INSERT")
    record2 = schedule_stream_record(event_name="MODIFY")
    record3 = schedule_stream_record(event_name="REMOVE")

    expected_params = {
        "TopicArn": "some-topic-arn",
        "Message": json.dumps(record2["dynamodb"]),
        "MessageAttributes": {
            "eventName": {"StringValue": "MODIFY", "DataType": "String"},
        },
    }

    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "234"},
        expected_params=expected_params,
    )

    event1 = {"Records": [record1]}
    event2 = {"Records": [record2]}
    event3 = {"Records": [record3]}

    assert InsertHandler.handler(event1, None) is None
    assert InsertHandler.handler(event2, None) is None
    assert InsertHandler.handler(event3, None) is None

    logger.info.assert_any_call(
        "Skipping filtered record",
        {"record": record1},
    )
    logger.info.assert_any_call("Sending to SNS", expected_params)
    logger.info.assert_any_call(
        "Skipping filtered record",
        {"record": record3},
    )


def test_customise_message_and_attributes(BasicHandler, sns_stub, logger):
    class CustomMessageHandler(BasicHandler):
        def create_message(self, record):
            return f"Event type: {record['eventName']}"

        def create_message_attributes(self, record):
            return {
                "EVENT_NAME": {
                    "StringValue": record["eventName"],
                    "DataType": "String",
                }
            }

    record = schedule_stream_record(event_name="MODIFY")

    expected_params = {
        "TopicArn": "some-topic-arn",
        "Message": "Event type: MODIFY",
        "MessageAttributes": {
            "EVENT_NAME": {"StringValue": "MODIFY", "DataType": "String"},
        },
    }
    sns_stub.add_response(
        "publish",
        service_response={"MessageId": "234"},
        expected_params=expected_params,
    )

    assert CustomMessageHandler.handler({"Records": [record]}, None) is None

    logger.info.assert_any_call("Sending to SNS", expected_params)
