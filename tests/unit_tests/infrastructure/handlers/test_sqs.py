from unittest.mock import MagicMock

import pytest

from infrastructure.handlers.sqs import SqsMessageHandler
from infrastructure.logger import Logger


@pytest.fixture(autouse=True)
def logger():
    yield MagicMock(wraps=Logger("SERVICE", "SOURCE"))


@pytest.fixture
def handler(logger):
    class Handler(SqsMessageHandler):
        def create_logger(self):
            return logger

    yield Handler.handler


@pytest.fixture
def sqs_logger_handler(logger):
    class Handler(SqsMessageHandler):
        def create_logger(self):
            return logger

        def execute(self):
            self.logger.info("self.record is", {"record": self.record})

    yield Handler.handler


def test_handler_errors_if_more_than_one_record(handler):
    event = {"Records": [{}, {}]}

    expected_err_msg = (
        "Event must contain zero or one records; found 2 records"
    )

    with pytest.raises(ValueError, match=expected_err_msg):
        handler(event, None)


def test_handler_sets_record_attribute(sqs_logger_handler, logger):
    event = {"Records": [{"body": '{"hello": "world"}'}]}

    sqs_logger_handler(event, None)

    logger.info.assert_any_call(
        "self.record is",
        {"record": {"body": '{"hello": "world"}'}},
    )


def test_handler_sets_record_as_none_if_no_records(sqs_logger_handler, logger):
    sqs_logger_handler({"Records": []}, None)

    logger.info.assert_any_call(
        "self.record is",
        {"record": None},
    )


def test_loggable_event_is_json_parsed(handler, logger):
    event = {"Records": [{"body": '{"hello": "world"}'}]}

    handler(event, None)

    logger.info.assert_any_call(
        "Lambda invocation started",
        {"event": {"Records": [{"body": {"hello": "world"}}]}},
    )
