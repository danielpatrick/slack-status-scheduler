+++
title = "Frequently Asked Questions"
in_search_index = true
weight = 99

[extra]
lead = "Or things someone would ask if anyone was asking anything"
toc = true
+++

## So how do I use this thing?

Schedule your daily lunch break using the `/daily-status` slash command:

`/daily-status 🍕🍔🥪`

and simply complete the form that pops up.

## Is it possible to set a one-off schedule?

Yes! Use the `/schedule-status` slash command to schedule a one-off status like
a doctor's appointment:

`/schedule-status 🩺`

and complete the form that pops up.

## Why does the app not respect my skin tone preferences?

Slack doesn't provide a way for apps to get this information about users
right now. If this changes, the app will provide the functionality as soon
as possible.

## Why can't I use custom emojis?

Keeping an up to date record of a user's custom emojis is a fair amount of
work but this may be implemented at a later date.
