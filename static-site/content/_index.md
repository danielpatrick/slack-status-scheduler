+++
title = "Automate your Slack status"

[extra]
lead = "If you're manually setting yourself away at lunch time, it's time to stop"
url = "install"
url_button = "Get started"

[[extra.list]]
title = "Future statuses ⏰"
content = "Set your holiday or doctor's appointment using <code>/schedule-status 🌴</code>"

[[extra.list]]
title = "Recurring statuses 🗓️"
content = "Set your daily lunch break or afternoon nap using <code>/daily-status 🍕 🌯</code>"

[[extra.list]]
title = "Randomised emojis 🎉"
content = "Provide more than one emoji and <strong>Status Cron</strong> will pick one at random each time it sets your status."
+++
