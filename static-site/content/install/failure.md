+++
title = "Authentication failed"
in_search_index = false

[extra]
toc = true
seo_noindex = true
+++

Something went wrong.

## Try again?

Click [here](@/install/_index.md) to try again.
