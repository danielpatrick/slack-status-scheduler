+++
title = "Installation"
template = "section.html"
page_template = "page.html"
sort_by = "weight"
weight = 1

[extra]
lead = "Add Status Cron to your Slack workspace"
toc = true
+++

## Authorize with Slack

Click on the button below to authorize the app.

{{ slack_link() }}
