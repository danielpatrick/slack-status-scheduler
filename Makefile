.PHONY: install local lint cfn-lint flake8 black black-fix isort isort-fix bandit safety test build emojis FORCE

EMOJIS_SOURCE_URL := "https://raw.githubusercontent.com/iamcal/emoji-data/master/emoji.json"

FORCE:

install:
	poetry install

update:
	poetry update

local: install
	poetry run pre-commit install

lint: flake8 black cfn-lint isort

flake8:
	poetry run flake8

black:
	poetry run black --diff --check .

black-fix:
	poetry run black .

isort:
	poetry run isort --check --diff .

isort-fix:
	poetry run isort .

cfn-lint:
	poetry run cfn-lint template.yaml

bandit:
	poetry run bandit -r slack_status_scheduler -q -n 3

safety:
	poetry export -f requirements.txt | poetry run safety check --stdin

utest:
	poetry run pytest \
		--cov-report term:skip-covered \
		--cov-report html:reports \
		--junitxml=reports/unit_test_coverage.xml \
		--cov=slack_status_scheduler \
		tests/unit_tests -ra -s

test: utest

.DELETE_ON_ERROR:
requirements.txt:
	poetry export -f requirements.txt -o requirements.txt
	sam build -m requirements.txt -t template.yaml --debug --use-container
	rm requirements.txt

build: requirements.txt

slack_status_scheduler/domain/slack/emojis.py: FORCE
	printf '"""Set of valid Slack emojis (excluding user-defined emojis)."""\n\nEMOJI_SET = {\n' > $@
	curl -sSL $(EMOJIS_SOURCE_URL) | jq -r 'map("    \":\(.short_name):\",") | .[]' >> $@
	printf '}\n' >> $@

emojis: slack_status_scheduler/domain/slack/emojis.py

zola-dev:
	API_DOMAIN=api.currentlyunder.dev SLACK_CLIENT_ID=123.456 SLACK_APP_ID=A1234 zola --root ./static-site serve
