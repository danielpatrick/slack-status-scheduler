# Slack Status Scheduler

Scheduling Slack statuses since [insert year project is finished]

## Deployment to new env

1. Push TF config for domain name, certificate, S3 bucket and Cloudfront distribution
2. Create new Slack app using manifest yml file for the correct env (/slack-config/\*.yml)
3. Upload the icon from /slack-config to the Slack app's 'Display Information' section (under 'Basic Information')
4. Upload Slack credentials to AWS Secrets Manager as `SlackAppCredentials` in the format below
5. Deploy everything else via GitLab CI, using a job that extends `.deploy`
6. Click the Retry button in Slack's Event Subscriptions page for the app

### SlackAppCredentials

```json
{
  "AppId": "string",
  "ClientId": "string",
  "ClientSecret": "string",
  "SigningSecret": "string",
}
```

## Architecture

![Architecture](architecture.png "Application Architecture")
